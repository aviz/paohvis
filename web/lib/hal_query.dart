library hal;

//import 'dart:core';
import 'dart:html';
import 'dart:convert';

String LAST_QUERY_ID = "";
String LAST_QUERY_RES = "";

String halQuery(String docid){

  if(docid != LAST_QUERY_ID) {
    String query = "https://api.archives-ouvertes.fr/search/?q=docid:" + docid +
        "&wt=json";

    HttpRequest.getString(query).then((String fileContents) {
      var response = json.decode(fileContents);
      LAST_QUERY_RES = response['response']['docs'][0];
      return LAST_QUERY_RES;

    }).catchError((Error error) {
      print("error");
    });

    LAST_QUERY_ID = docid;
  }
  return LAST_QUERY_RES;

}