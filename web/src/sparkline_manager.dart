part of paohvis;


// sparkline manager
class SparkLineManager{

  CanvasElement _canvas;

  double maxValue = 0.0;
  //int _nodeHeight;
  Map<String, Map<String, Sparkline> > sparkLines;
  bool isHighlighted = false;
  bool isSelected = false;

  List<String> backColors = Theme.ALT_COLORS;

  Map<String, double> _minVal;
  Map<String, double> _maxVal;

  String defaultVal = 'value';
  String _firstTs = '';

  TimeManager tm = new TimeManager();


  SparkLineManager (CanvasElement canvas){

    _canvas = canvas;

    sparkLines = new Map<String, Map<String, Sparkline> > ();
    sparkLines[defaultVal] = new Map<String, Sparkline>();
    _minVal = new Map<String, double>();
    _maxVal = new Map<String, double>();
    _minVal[defaultVal] = 0.0;
    _maxVal[defaultVal] = 0.0;
    setup();
  }

  setup(){
    HslColor hueCol = new HexColor(Theme.ALT_COLORS[0]).toHslColor();
    backColors[0] =  '#' + new HslColor(hueCol.h, hueCol.s, BACKGROUND_INTENSITY*100).toHexColor().toString();
  }

  double getMaxVal([String value=null]){
    if(value == null) value = defaultVal;
    return _maxVal[value];
  }

  double getMinVal([String value=null]){
    if(value == null) value = defaultVal;
    return _minVal[value];
  }

  String firstTs(){
    return _firstTs;
  }

  void computeFirstTs(String value){
    String fTs = sparkLines[value].keys.reduce((v, e){
      return v.compareTo(e) < 0 ? v : e;
    } );
    _firstTs = fTs;
  }

  /**Indicates min and max values of data inside node sparklines */
  void addSparkline(Sparkline sl, [String value=null]){
    if(value == null) value = defaultVal;

    if(!sparkLines.containsKey(value))
      sparkLines[value] = new Map<String, Sparkline>();
    if(!_minVal.containsKey(value))
      _minVal[value] = 0.0;
    if(!_maxVal.containsKey(value))
      _maxVal[value] = 0.0;

    if(!sparkLines[value].containsKey(sl.timeFrame)) {
      if (sparkLines[value].length == 0) {
        _minVal[value] = sl.minValue;
        _maxVal[value] = sl.maxValue;
      } else {
        if (sl.minValue < _minVal[value])
          _minVal[value] = sl.minValue;
        if (sl.maxValue > _maxVal[value]) {
          _maxVal[value] = sl.maxValue;
          maxValue = _maxVal[value]; //TODO: remove this
        }
      }
      sparkLines[value][sl.timeFrame] = sl;
      // updates global values
      updateMinMaxSparklines(_minVal[value], _maxVal[value], value);
      computeFirstTs(value);
    }

  }

  double computeAggregatedValue([String value = null]){
    if(value == null) value = defaultVal;
    double aggregatedValue = 0.0;
    sparkLines[value].forEach((ts, sl){
      aggregatedValue += sl.computeAggregatedValue();
    });
    aggregatedValue /= tm.numTimes;
    return aggregatedValue;
  }

  void updateMinMaxSparklines(var minVal, var maxVal, [String value = null]){
    if(value == null) value = defaultVal;
    //some small optimization can be done here, if needed
    //everytime a new sparkline is added here is the
    //loop through all sparklines to update the min and
    //max values.
    sparkLines[value].forEach((tf, sls){
      sls.globalMinValue = minVal;
      sls.globalMaxValue = maxVal;
    });
  }

  /** displacement: space of the canvas used to show nodes frame */
  void renderAllSparkLines( num height,  Node n){
    num nts = sparkLines.length;
    int i =0;
    sparkLines.keys.forEach((s){
      renderSparkLines(height/nts, n, 0,i*height/nts, s);
      i++;
    });
  }

  /** displacement: space of the canvas used to show nodes frame */
  void renderSparkLines(num height,  Node n, [num left = 0, num top = 0, String value = null]){

    if(value == null) value = defaultVal;
    double sparkLineHeight = height;
    int alter_color = 1;
    // draws time frames
    tm.forEach((var s){
      if(tm.isVisible(s)) {
        if(!tm.isMinimized(s)) {
//        int t = tm.getIndex(s);
//        print(sparkLines[value][s]);
          num posX = tm.getPosX1(s) +
              left; //when left is set it gives a left margin to the sparkline
          num posY = n.y - n.margin +
              top; //TODO: margin was introduced later, check Node.height/2 and replace with margin
          num tsWidth = tm.getWidth(s);


          Sparkline sparkLine = new Sparkline([]);
          if ((sparkLines.containsKey(value)) &&
              ((sparkLines[value].containsKey(s)))) {
//          if(n.hasCommunity && n._community != "theComId") {
//            double com = double.parse(n._community);
////            print(n._community);
//            sparkLine = new Sparkline([com]);
//          }
            if (s == "2" && n.name == "45") {
              sparkLines[value][s].timepoints[0] = 3.0;
            }
            if (s == "2" && n.name == "46") {
              sparkLines[value][s].timepoints[0] = 3.0;
            }
            if (s == "2" && n.name == "5") {
              sparkLines[value][s].timepoints[0] = 1.0;
            }
            if (s == "2" && n.name == "7") {
              sparkLines[value][s].timepoints[0] = 5.0;
            }
            if (s == "2" && n.name == "58") {
              sparkLines[value][s].timepoints[0] = 0.0;
            }
            sparkLine = sparkLines[value][s];
//        }

//        if (sparkLines.containsKey(value)) {
//          if (sparkLines[value].containsKey(s)) {
//            sparkLine = sparkLines[value][s];
//          }
          }

          if (NODELINK) {
            alter_color =
            (tm.getIndex(s) % 2); //vertical zebra stripes in node-link only
          } else {
            alter_color = (n.position / N_ALTERNATE_ROWS).floor() % 2;
          }

          sparkLine.renderSparkline(
              _canvas.getContext('2d'),
              posX,
              posY,
              tsWidth,
              sparkLineHeight,
              sparkLineHeight,
              isHighlighted,
              isSelected,
              backColors[alter_color]);
        }
      }
    });
  }
}
