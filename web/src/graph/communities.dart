part of paohvis;


class Communities extends IterableBase {
  Map<String, int> communities = null; // int could be replaced with a class Community with more info about the community
  Map<int, String> comIds = null;

  Communities(){
    communities = new Map<String, int>();
    comIds = new Map<int, String>();
  }

  get iterator {
    return communities.keys.iterator;
  }

  bool addCommunity(String newCom){
    if(communities.containsKey(newCom)) return false;
    int comId = communities.length;
    comIds[comId] = newCom;
    communities[newCom] = comId;
    return true;
  }

  int getComId(String com){
    if(communities.containsKey(com)) {
      return communities[com];
    }
    return null;
  }

  String getComName(int comId){
    if(comIds.containsKey(comId)) {
      return comIds[comId];
    }
    return null;
  }

  getCommunitiesNames(){
    return communities.keys.toList();
  }
}