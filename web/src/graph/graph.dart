part of paohvis;


/* contains data of the graph */
class Graph {
  // metadata here...

  String _hyperedge_meta = 'hyperedge';
  String _node_meta = 'node';
  String _group_meta = 'group';
  String _ts_meta = 'time slot';

  double maxNodeValue = 0.0;
  double minNodeValue = 0.0;

  Nodes nodes = new Nodes();
  Map <String, List <Edge>> _edges = new Map<String, List <Edge>>();
  Map <String, List <Edge>> _simple = new Map<String, List <Edge>>();
  Communities communities = new Communities();
  Communities roles = new Communities();



  Map<String, Map<String, List<int>> > _mapEdgesTS = new Map<String, Map<String, List<int> > >();

  List<List<double>> _adj;
  List<int> _adjId;
  bool isAdjComputed = false;

  Map <String, List <Edge>> get edges{
    return _edges;
  }

  Map <String, List <Edge>> get simple{
    computeSimple();
    return _simple;
  }

  Map<String, Map<String, List<int>> > get mapEdgesTS{
    return _mapEdgesTS;
  }


  String get hyperedge_meta => _hyperedge_meta;

  set hyperedge_meta(String value) {
    _hyperedge_meta = value;
  }
  String get node_meta => _node_meta;
  set node_meta(String value) {
    _node_meta = value;
  }

  String get group_meta => _group_meta;
  set group_meta(String value) {
    _group_meta = value;
  }

  String get ts_meta => _ts_meta;
  set ts_meta(String value) {
    _ts_meta = value;
  }



  Graph() {

    // setup stuff here...
  }

  num getAverageNumberOfNodesbyEdgeinTs(String ts){
    if(_edges.containsKey(ts)) {
      num eits = getNumberOfEdgesInTs(ts);
      if(eits != 0)
        return getNumberOfNodesInTs(ts)/eits;
  }
    return 0;
  }

  num getAverageNumberOfHighlightedNodesbyEdgeinTs(String ts){
    if(_edges.containsKey(ts)) {
      num eits = getNumberOfHighlightedEdgesInTs(ts);
      if(eits != 0)
        return getNumberOfHighlightedNodesInTs(ts)/eits;
    }
    return 0;
  }

  num getAverageNumberOfSelectedNodesbyEdgeinTs(String ts) {
    if (_edges.containsKey(ts)){
      num eits = getNumberOfSelectedEdgesInTs(ts);
      if (eits != 0)
        return getNumberOfSelectedNodesInTs(ts) /
            eits;
    }
    return 0;
  }



  num getNumberOfEdgesInTs(String ts){
    if(_edges.containsKey(ts))
      return edges[ts].fold(0, (num prev, Edge e) => prev + ((e.isValid) ? 1 : 0));
    return 0;
  }
  num getNumberOfHighlightedEdgesInTs(String ts){

    if(_edges.containsKey(ts)) {
      return _edges[ts].fold(0, (num prev, Edge e) => prev + ((e.isValid && (e.isHighlighted||e.isSideHighlighted)) ? 1 : 0));
    };
    return 0;
  }
  num getNumberOfSelectedEdgesInTs(String ts){

    if(_edges.containsKey(ts)) {
      return _edges[ts].fold(0, (num prev, Edge e) => prev + ((e.isValid && (e.isSelected || e.isSideSelected)) ? 1 : 0));
    };
    return 0;
  }

  num getNumberOfNodesInTs(String ts){
    int nNodes = 0;
    if(_edges.containsKey(ts)) {
      _edges[ts].forEach((Edge e) {
        if(e.isValid)
          nNodes += e.edgeUnfiltered.length;
      });
    }
    return nNodes;
  }
  num getNumberOfHighlightedNodesInTs(String ts){
    int nNodes = 0;
    if(_edges.containsKey(ts)) {
      _edges[ts].forEach((Edge e) {
        if(e.isValid)
            if(e.isHighlighted || e.isSideHighlighted)
//          if(n.isHighlighted || n.isAdjacentToHighlighted)
              nNodes += e.edgeUnfiltered.length;
      });
    }
    return nNodes;
  }
  num getNumberOfSelectedNodesInTs(String ts){
    int nNodes = 0;
    if(_edges.containsKey(ts)) {
      _edges[ts].forEach((Edge e) {
        if(e.isValid)
          if(e.isSelected || e.isSideSelected)
            nNodes += e.edgeUnfiltered.length;
      });
    }
    return nNodes;
  }

  num getNumberOfUniqueNodesInTs(String ts){
    Set nodesSet = new Set();
    if(_edges.containsKey(ts)) {
      _edges[ts].forEach((Edge e) {
        if(e.isValid)
          nodesSet.addAll(e.edge.map((Node n) => n.id));
      });
    }
    return nodesSet.length;
  }

  num getNumberOfUniqueHighlightedNodesInTs(String ts){
    Set nodesSet = new Set();
    if(_edges.containsKey(ts)) {
      _edges[ts].forEach((Edge e) {
        if(e.isValid)
          if(e.isHighlighted || e.isSideHighlighted)
          e.edge.forEach((Node n){
//            if(n.isHighlighted || n.isAdjacentToHighlighted)
              nodesSet.add(n.id);
        });
//        nodesSet.addAll(e.edge.map((Node n) => n.id));
      });
    }
    return nodesSet.length;
  }

  num getNumberOfUniqueSelectedNodesInTs(String ts){
    Set nodesSet = new Set();
    if(_edges.containsKey(ts)) {
      _edges[ts].forEach((Edge e) {
        if(e.isValid)
          if(e.isSelected || e.isSideSelected)
          e.edge.forEach((Node n){
            if(n.isSelected || n.isAdjacentToSelected) nodesSet.add(n.id);
        });
//        nodesSet.addAll(e.edge.map((Node n) => n.id));
      });
    }
    return nodesSet.length;
  }

  //naive function that adds edges. It believes that there are no dupes in data
  void addEdge(Edge e, String ts) {

    if (!edges.containsKey(ts)) edges.putIfAbsent(ts, () => new List<Edge> ());

    e.edge.forEach((Node n){
      n.updateFirsTs(ts);
    });

//    addEdgeToMap(e, ts);

//    print(ts);
//    print(edgeString);
    edges[ts].add(e);
  }

  void printUpset(){
//    List<List<double>>  adj = getAdjacency();


    String table = "";

    String nIdRow = 'Row;';
    nodes.valid.forEach((n) {
      nIdRow += n.name + "; ";
    });

    table += nIdRow + "\n";

      edges.forEach((ts, tsEdges){

        int eId = 1;

        tsEdges.forEach((e){

          if(e.isValid) {
            String eS = ts + "_" + eId.toString() + '; ';
            nodes.valid.forEach((n) {
              bool flag = false;

            e.edge.forEach((n1) {
                if (n1.id == n.id) {
                  flag = true;
                }
              });
              eS += flag? "1; " : "0; ";
            });
            table += eS  + "\n";
          }

          eId++;
      });
    });

      print(table);

  }

  void addEdgeToMap(Edge e, String ts){
    String edgeString = e.getString();
    if(!_mapEdgesTS.containsKey(edgeString)) {
      _mapEdgesTS[edgeString] = new Map<String, List<int>>();
    }
    if(!_mapEdgesTS[edgeString].containsKey(ts)){
      _mapEdgesTS[edgeString][ts] = new List<int>();
    }
    _mapEdgesTS[edgeString][ts].add(edges[ts].length);
  }


  /** First checks that all nodes of edge already are declared,
   * then creates a new TS not existsing
   * then adds the edge if not present */

  void _addIfNotExisting(Edge newEdge, String ts){
    bool insert = true;
//                  newEdge.updateValid();
    if(edges.containsKey(ts)) {
      insert = !edgeInTs(newEdge, edges[ts]);
    }
    if (insert) {
      addEdge(newEdge, ts);
    }
  }

  bool edgeInTs (Edge newEdge, List<Edge> edgeTs){
    if (edgeTs.length > 0) {
      for (Edge existingEdge in edgeTs) {
        if (existingEdge.equal(newEdge)) {
          return true;
        }
      }
    }
    return false;
  }

  void addEdgeIfAbsent(Edge e, String ts) {
    if (!nodesExistInEdge(e)) {
//      if (!edges.containsKey(ts)) {
//        edges.putIfAbsent(ts, () => new List<Edge> ());
//      }

      _addIfNotExisting(e, ts);

//      bool insert = true;
//      if (edges[ts].length > 0) {
//        edges[ts].forEach((existingEdge) {
//          if (existingEdge.equal(e)) {
//            insert = false;
//          }
//        });
//      }
//      if (insert) {
//        edges[ts].add(e);
//      }
    }
  }

  /**
   * returns true only if every nodes of the edge exist in nodes
   */
  bool nodesExistInEdge(Edge e) {
    bool exist = false;
      e.edge.forEach((n) {
        if (!nodes.any((ex_n) => ex_n.id == n.id))
          exist = false;
      });
    return exist;
  }

  //service function that prints the content of an endge in the console
  void printEdge(Edge e){
    if(DEBUG){
      print ("edge (");
      e.edge.forEach((n){
        print(n.id);
      });
      print (")");
    }
  }

  void printCounts(){
    int dotsTotal = 0;
    int dotsTotalValid = 0;

    edges.forEach((ts, tsEdges){

      tsEdges.forEach((tsEdge) {

          tsEdge.edgeUnfiltered.forEach((n) {
            dotsTotal++;

            if(tsEdge.isValid && n.isValid){
              dotsTotalValid++;

            }
          });
      });
    });

    print("total dots " + dotsTotal.toString());
    print("total dots valid " + dotsTotalValid.toString());

  }


    // clear all data in the graph
  void reset(){
    communities.communities.clear();
    nodes.clear();
    edges.clear();
    _simple.clear();
    maxNodeValue = 0.0;
    isAdjComputed = false;
  }

  void orderByNodeValue(){
    //nodes.sort();
  }

  int getNumNodes() => nodes.length;

  List<List<double>> getAdjacency(){
//    if(!isAdjComputed)
    computeAdjacency();
    isAdjComputed = true;
    return _adj;
  }

  void computeSimple(){
    if(_simple.length == 0){
      edges.forEach((ts, tsEdges){
        _simple[ts] = new List <Edge>();
//        print(ts);

        for(Edge e in tsEdges){
          if(e.edgeUnfiltered.length == 2){
            Edge newEdge = new Edge([e.edgeUnfiltered[0], e.edgeUnfiltered[1]], e.weight);
            if(!edgeInTs(newEdge, _simple[ts])){
              _simple[ts].add(newEdge);
            }


//            _simple[ts].add(e);
          }else if(e.edgeUnfiltered.length > 2){
            e.edgeUnfiltered.forEach((n1){
              e.edgeUnfiltered.forEach((n2) {
                if (n1.id < n2.id) {
                  Edge newEdge = new Edge([n1, n2], e.weight);

                  if(!edgeInTs(newEdge, _simple[ts])){
                    _simple[ts].add(newEdge);
                  }
                }
              });
            });
          }
        }

      });
    }
  }

  void computeDegree([bool notValids = false]){
    Map<int, int> nodeMap = new Map<int, int>(); //consider having one map
    Map<int, int> nodeMapUnique = new Map<int, int>(); //consider having one map

    edges.forEach((ts, tsEdges){
      Set<int> nodeSet =  new Set<int> ();

      tsEdges.forEach((tsEdge) {
        if(notValids || tsEdge.isValid){
          tsEdge.edge.forEach((n){

            if(nodeMap.containsKey(n.id)) {
              nodeMap[n.id] = nodeMap[n.id] + 1;
            }else{
              nodeMap[n.id] = 1;
            }

            if(nodeSet.add(n.id)) {
              if (nodeMapUnique.containsKey(n.id)) {
                nodeMapUnique[n.id] = nodeMapUnique[n.id] + 1;
              } else {
                nodeMapUnique[n.id] = 1;
              }
            }

          });
        }
      });
    });

    nodes.forEach((var node){
      node.degree = nodeMap[node.id];
      if(node.degree == null) node.degree = 0;

      node.degreeUnique = nodeMapUnique[node.id];
      if(node.degreeUnique == null) node.degreeUnique = 0;

    });

  }

  void computeHighDegree(){
    Map<int, int> nodeMap = new Map<int, int>(); //consider having one map
    Map<int, int> nodeMapUnique = new Map<int, int>(); //consider having one map

    edges.forEach((ts, tsEdges){
      Set<int> nodeSet =  new Set<int> ();

      tsEdges.forEach((tsEdge) {
        if(tsEdge.isHighlighted || tsEdge.isSideHighlighted) {
          if( tsEdge.isValid){
          tsEdge.edge.forEach((n) {

              if (n.isHighlighted || n.isAdjacentToHighlighted) {
              if (nodeMap.containsKey(n.id)) {
                nodeMap[n.id] = nodeMap[n.id] + 1;
              } else {
                nodeMap[n.id] = 1;
              }

              if (nodeSet.add(n.id)) {
                if (nodeMapUnique.containsKey(n.id)) {
                  nodeMapUnique[n.id] = nodeMapUnique[n.id] + 1;
                } else {
                  nodeMapUnique[n.id] = 1;
                }
              }
            }

          });
        }
        }
      });
    });

    nodes.forEach((var node){
      node.highDegree = nodeMap[node.id];
      if(node.highDegree == null) node.highDegree = 0;

      node.highDegreeUnique = nodeMapUnique[node.id];
      if(node.highDegreeUnique == null) node.highDegreeUnique = 0;

    });

  }
  void computeSelectedDegree(){
    Map<int, int> nodeMap = new Map<int, int>(); //consider having one map
    Map<int, int> nodeMapUnique = new Map<int, int>(); //consider having one map

    edges.forEach((ts, tsEdges){
      Set<int> nodeSet =  new Set<int> ();

      tsEdges.forEach((tsEdge) {
        if(tsEdge.isSelected  || tsEdge.isSideSelected) {
          if( tsEdge.isValid){

            tsEdge.edge.forEach((n) {
            if (n.isSelected || n.isAdjacentToSelected) {
              if (nodeMap.containsKey(n.id)) {
                nodeMap[n.id] = nodeMap[n.id] + 1;
              } else {
                nodeMap[n.id] = 1;
              }

              if (nodeSet.add(n.id)) {
                if (nodeMapUnique.containsKey(n.id)) {
                  nodeMapUnique[n.id] = nodeMapUnique[n.id] + 1;
                } else {
                  nodeMapUnique[n.id] = 1;
                }
              }
            }
          });
        }
        }
      });
    });

    nodes.forEach((var node){
      node.selectedDegree = nodeMap[node.id];
      if(node.selectedDegree == null) node.selectedDegree = 0;

      node.selectedDegreeUnique = nodeMapUnique[node.id];
      if(node.selectedDegreeUnique == null) node.selectedDegreeUnique = 0;

    });

  }

  orderElements(List <int> adjOrder){

//    resetNodesOrder();
    List <int> order;
    if(_adjId.length > 0){
      order = new List<int>();
      adjOrder.forEach((o){
        order.add(_adjId[o]);
      });
    }
    else
      order = adjOrder;

    nodes.orderElements(order);
  }

  /// sorts according to the overall degree of node connections
  sortNodes(SortingNodes sn, [bool desc]){
    resetNodesOrder();
    if(SortingNodes.alpha.index == sn.index) {
      desc = desc == null? true: desc;
      nodes.sortAlpha(desc);
    }
    if(SortingNodes.community.index == sn.index) {
      desc = desc == null? true: desc;
      nodes.sortCommunity(desc);
    }
    if(SortingNodes.degree.index == sn.index) {
      desc = desc == null? true: desc;
      nodes.sortDegree(desc);
    }
    if(SortingNodes.timeslot.index == sn.index) {
      desc = desc == null? true: desc;
      nodes.sortTimeSlot(desc);
    }
  }

  resetNodesOrder() {
    nodes.resetOrder();
  }

  computeAdjacency(){
    Map<int, int> nodeMap = new Map<int, int>();
    _adj = new List<List<double>> ();
    _adjId = new List<int>();

    int ni = 0;
    nodes.valid.forEach((n1){
      nodeMap.putIfAbsent(n1.id, () => ni);
      _adjId.add(n1.id);
      _adj.add(new List<double> ());
      nodes.valid.forEach((_) {
        _adj.last.add(0.0);
      });
      ni++;
    });

    edges.forEach((ts, tsEdges){
      tsEdges.forEach((e){
        if(e.isValid){
          e.edge.forEach((n1) {
            if (n1.isValid) {
              e.edge.forEach((n2) {
                if (n2.isValid) {
                  if (n1.id != n2.id) {
                    int source = nodeMap[n1.id];
                    int target = nodeMap[n2.id];
                    _adj[source][target] = _adj[source][target] + 1.0;
                    _adj[target][source] = _adj[target][source] + 1.0;
                  }
                }
              });
            }
          });
        }

      });
    });
  }


}
