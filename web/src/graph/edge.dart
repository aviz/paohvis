part of paohvis;


/**
 * Manages edge of the graph
 */
class Edge extends IterableBase{
  List<Node> _edge;
  num _weight = 1.0;
  Map _meta = new Map();

  /*
  _repeat indicates that the edge is the representative of many edges sharing the
  same vertices in the same TS
  * */
  int _repeat = 1;

  FastBitSet nodeSet = new FastBitSet(new List<int>());

  List<Node> _validEdge;
  double _xPos = 0.0;
  int _lenght = 0;

  List<Node> get edge => _validEdge;
  List<Node> get edgeUnfiltered => _edge;

  num get weight => _weight;
  Map get meta => _meta;


  int get lenght => _lenght;
  int get minPos => first.position;
  int get maxPos => last.position;
  Node get first => edge[0];
  Node get last => edge[edge.length-1];
  int get numVertex => edge.length;
  double get xPos => _xPos;
  String get label => label;

  set xPos(double x) => _xPos = x;


  bool _isValid = true;
  bool _isVisible = true;

  bool _isSelected = false;
  bool _isHighlighted = false;
  bool _isSideHighlighted = false;
  bool _isSideSelected = false;

  bool _inIntersection = false;

  Edge (List<Node> ln, num w, [Map meta = const{}]){
    _edge = ln;
    _weight = w;
    _meta = meta;

    ln.forEach((n){
      nodeSet.add(n.id);
    });

    updateValid();
    computeMinMax();
  }

  bool get isValid => _isValid;

  bool get isSelected => _isSelected;
//  set isSelected(bool s) => _isSelected = s;
  void select() {_isSelected = true;}
  void unselect(){_isSelected = false;}

  bool get isHighlighted => _isHighlighted;
//  set isHighlighted(bool s) => _isHighlighted = s;
  void highlight() {_isHighlighted = true;}
  void unhighlight() {_isHighlighted = false;}

  bool get isSideHighlighted => _isSideHighlighted;
  void sideHighlight() {_isSideHighlighted = true;}
  void unsideHighlight() {_isSideHighlighted = false;}

  bool get isSideSelected => _isSideSelected;
  void sideSelect() {_isSideSelected = true;}
  void unsideSelect() {_isSideSelected = false;}

  void set inIntersection(bool st){ _inIntersection = st;}
  bool get inIntersection => _inIntersection;

  int get repeat => _repeat;
  void set repeat(int n) => _repeat = n;
  void addRepeat() => _repeat++;

  resetValid(){
    _isValid = true;
  }

  void updateValid([bool toBeValid=false]){

    bool validContext = true;
    if( !SHOW_CONTEXT )
      validContext = (EDGE_SELECTED || NODE_SELECTED || TS_SELECTED ) && (isSelected || isSideSelected);

    if(validContext) {
      _isValid = false; //_edge.any((n) => n.isValid); // TODO: updateValid is called every render, but, this doesn't need to be updated every render
      _validEdge = [];
      _edge.forEach((n) {
        if (n.isValid || (toBeValid &&n.toBeValid)) {
          _isValid = true;
          _validEdge.add(n);
        }
      });

    }else
      _isValid = false;
    computeMinMax();
  }

  void sort(){
    if(_validEdge != null && _validEdge.length > 1)
      _validEdge.sort((a, b) => a.position.compareTo(b.position));
  }

  bool equal(Edge b){
    Edge a = this;

    Function eq = const ListEquality().equals;
    List<num> anIds = new List<num>.from(a.edgeUnfiltered.map((Node n) => n.id));
    List<num> bnIds = new List<num>.from(b.edgeUnfiltered.map((Node n) => n.id));
    return eq(anIds, bnIds);

//    bool equal = true;
//    if(a.length == b.length) {
//      bool localEquality = true;
//      a.edge.forEach((nodeNew) {
//        bool existingFound = b.edge.any((el) => nodeNew.id == el.id);
//        if (!existingFound) equal = false;
//      });
//    } else equal = false;
//    return equal;
  }

  void computeMinMax(){
    sort();
    _lenght = _isValid ? last.position - first.position: 0;
  }

  get iterator {
    return _validEdge.iterator;
  }

  bool get isVisible{
    return _isVisible;// _edge.any((n) => n.isVisible);
  }

  String getString(){
    List<int> nodesIds = _edge.map((n) => n.id).toList();
    nodesIds.sort();
    String nodesString = nodesIds.fold("", (v, n) => v + "_" + n.toString());
    return nodesString;
  }

}