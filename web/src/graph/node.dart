part of paohvis;

// visual node, to be mapped to the data
class Node{
  // node data
  double _value=0.0;
  String name = "";
  num _id = 0;
  var maxValue;
  SparkLineManager _slm;
  String _community = "";

  num coordX = null; //Coordinate x of the node
  num coordY = null; //Coordinate y of the node
  num x = 0; // x position
  num y = 0; // y position
  num width = 0; // width
  num height = 0; // height
  num _margin = 0; // margin
  double size = 1.0; // dimension
  String foreColor = "#000000"; // color
  String backColor = "#FFFFFF";
  double orientation = 0.0; // orientation

  bool _isHighlighted = false;
  bool _isSelected = false;


  bool isAdjacentToHighlighted = false;
  bool isAdjacentToSelected = false;
  num position = 0; // can be useful

  num degree = 0; // can store the degree of the node
  num degreeUnique = 0; // can store the degree of the node

  num highDegree = 0; // can store the degree of the node
  num highDegreeUnique = 0; // can store the degree of the node

  num selectedDegree = 0; // can store the degree of the node
  num selectedDegreeUnique = 0; // can store the degree of the node

  double aggregatedValue = 0.0;

  bool isValid = true;
  bool isVisible = true;
  bool visited = false;
  bool toBeValid = true;

  String _firstTs = '';

  String get firstTs => _firstTs;

  set firstTs(String value) {
    _firstTs = value;
  }

  //Node constructor
  Node (num v){
    _id = v;
    _value = v.toDouble(); //TODO: use a better method to set the value (e.g. check the type before assigning)
    //slm = new SparkLineManager();
  }

  bool get isSelected => _isSelected;
//  set isSelected(bool s) => _isSelected = s;
  void select() {_isSelected = true;}
  void unselect() {_isSelected = false;}

  bool get isHighlighted => _isHighlighted;
//  set isHighlighted(bool s) => _isHighlighted = s;
  void highlight() {_isHighlighted = true;}
  void unhighlight() {_isHighlighted = false;}

  num get id => _id;

  set value(double value) => _value = value;
  double get value => _value;

  String get community => _community;
  set community(String value) => _community = value; // visual information

  bool get hasCommunity => community.length>0;

  double get maxSlValue => slm.maxValue;

  num get margin => _margin;
  set margin(num margin) => _margin = margin; // margin

  SparkLineManager get slm => _slm;

  set slm(SparkLineManager value) =>  _slm = value;

  updateFirsTs(String ts){
    if(_firstTs == '') {
      _firstTs = ts;
    }else{
      _firstTs = _firstTs.compareTo(ts) < 0? _firstTs : ts;
    }
  }

  num getTsCommunity(String ts, [String value = "community"]){

    if(_slm.sparkLines.containsKey(value)) {
      if (_slm.sparkLines[value].containsKey(ts)) {
        return _slm.sparkLines[value][ts].timepoints[0];
      }
    }
    return null;
  }

  computeAggregatedValue(){
    aggregatedValue = _slm.computeAggregatedValue();
  }

  bool isSimilar(String bName){
    if(bName.length < 2) return false;

//    if (libLev.distance(name, bName) <= 1){
//      return true;
//    }

    String nameLower = name.toLowerCase();
    String bnameLower = bName.toLowerCase();

    if(nameLower.length >= bnameLower.length) {
      if (nameLower.substring(0, bnameLower.length) == bName)
        return true;
    }else{
      if (bnameLower.substring(0, nameLower.length) == nameLower)
        return true;
    }


    for(String sn in nameLower.split(' ')) {
//      if (libLev.distance(sn, bName) < 1)
//        return true;
      if(sn.length > 1 ) {
        if (sn.length >= bnameLower.length) {
          if (sn.substring(0, bnameLower.length) == bnameLower)
            return true;
        } else {
          if (bnameLower.substring(0, sn.length) == sn)
            return true;
        }
      }
    }
    return false;
  }

}