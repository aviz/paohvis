part of paohvis;

class TabMinimize {

  CanvasElement _canvas;
  CanvasRenderingContext2D _context;

  TimeManager tm = new TimeManager();
  num _topMargin = 0;
  num _bottomMargin = 5;
  num _rightMargin = 0;
  num _fontSize = MAX_FONT;
  num _height = 0;


  TabMinimize (CanvasElement canvasElement, [num pos = 0, num tm = 0, num rm = 0]) {
    _canvas = canvasElement;
    _context = _canvas.getContext('2d');
    _topMargin = tm;
    _rightMargin = pos + rm;
    _height = _fontSize + _bottomMargin;
  }

  // renders minimize
  render(){
      _context
        ..beginPath()
        ..moveTo(_rightMargin-(_height / 2), _topMargin + (3 * (_height / 4)) - _bottomMargin)
        ..lineTo(_rightMargin, _topMargin + (3 * (_height / 4)) - _bottomMargin)
        ..strokeStyle = "#999999"
        ..closePath()
        ..stroke();
  }
}
