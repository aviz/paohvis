part of paohvis;

class VisManager{
  CanvasElement _canvas;
  CanvasElement _canvasOverlay;
  CanvasRenderingContext2D _context;
  CanvasRenderingContext2D _contextOverlay;
  Graph graph;
  TimeManager tm = new TimeManager();
  SelectStatisticManagerTop selectStatisticsTs;
  SelectStatisticManagerLeft selectStatisticsNodes;

  DivElement _ttip = document.querySelector("#tltip");
  DivElement _bib=document.querySelector("#bibfile");
  DivElement divcontainer= document.querySelector("#divcontainer");

  double baseUnit = 0.0; // if needed
  static int rightMargin = 10;
  num _visWidth, _visHeight = 0;

  num _tabsHeight = 0;

  num _statsWidth = 120.0;
  num _statsHeight= 50;

  int _currentEdgeHighlight = -1;
  int _currentNodeHighlight = -1;



  NodeManager nodeRenderer;
  EdgeManager edgeRenderer;
  TimeLabels tsLabelsRenderer;
  TsTabs tsTabs;
  Tooltip tooltip;
  StatsTop barChartTs = null;
  StatsLeft barChartNodes = null;

  num get visWidth => _visWidth;
  num get visHeight => _visHeight;
  CanvasRenderingContext2D get canvasContext => _context;
  CanvasElement get canvas => _canvas;


  TimeSliderManager tsm = new TimeSliderManager(_timeSlider, _timeSliderLables, _tsLabelMin, _tsLabelMax);

  CommunityLegendManager clm = new CommunityLegendManager(_divLegend);

  Map<String, Function> filters = new Map<String, Function> ();

  bool _showingHyper = true;

  List<Node> nodeHighlights =  new List<Node> ();
  List<Edge> edgeHighlights =  new List<Edge> ();

  List<Node> nodeSelects =  new List<Node> ();
  List<Edge> edgeSelects =  new List<Edge> ();
  List<String> tsSelects =  new List<String> ();

  FastBitSet selectedNodeSet = new FastBitSet(new List<int>());


  // canvas events
  var onMouseMove,onMouseUp,onMouseLeave,onMouseDown,onClick,onDblClick,onMouseWheel,
  onKeyDown;
//      onWindowResizeEvent, onDivResizeEvent;

  // custom events
  var NodeFilterChangedEvent,
      NodeSliderChangedEvent, NodeScrolledEvent,
      TimeSliderChangedEvent, TimeSlotsScrolledEvent, TimeSlotsMovedEvent,
      TimeSlotsStatisticsChangedEvent,  NodeStatisticsChangedEvent;


  VisManager(Graph g, CanvasElement canvas, CanvasElement canvasOverlay){

    if(DEMO_VIS4DH){
      _statsWidth = 20;
      _statsHeight = 2;
    }

    graph = g;

    _canvas = canvas;
    _canvasOverlay = canvasOverlay;

    _context = _canvas.getContext('2d');
    _contextOverlay = _canvasOverlay.getContext('2d');

    setupEvents();


    tsTabs = new TsTabs(g, canvas, 0, _visWidth, _statsHeight);
    nodeRenderer = new NodeManager(g, canvas);
    edgeRenderer = new EdgeManager(g, canvas, tsTabs.minWidth);
    //if SHOW_TABS is not enabled it don't give the space for render the tab
    if(SHOW_TABS){
      _tabsHeight = tsTabs.height;
    }

    tsLabelsRenderer = new TimeLabels(canvas, _statsHeight +  _tabsHeight, _visWidth);
    tooltip = new Tooltip(canvas, _ttip);

    selectStatisticsTs = new SelectStatisticManagerTop(_stElement, _selectStElement);//, 140+statsHeight, statsWidth, nl.frameWidth);
    selectStatisticsNodes = new SelectStatisticManagerLeft(_stElementLeft, _selectStElementLeft);//, 163+statsHeight, 0, statsWidth);

    computeDimensions();

    nodeRenderer.setContextConstraints(_statsWidth, _tabsHeight + tsLabelsRenderer.height + _statsHeight);
    //if SHOW_TABS is not enabled it don't give the space at left of sparklines
    //this margin align sparklines to the tabs
    if(SHOW_TABS) {
      nodeRenderer.setLeftMargin(tsTabs.tabMargin);
    }
    graph.computeDegree();

    graph.nodes.computeMinMaxDataset('value');
    graph.resetNodesOrder();
//    nl.computeAggregateValues();
//    nl.nodes.forEach((n) {
//      n.computeAggregatedValue();
//    });

    barChartTs = new StatsTop(_statsHeight, _visWidth, _canvas, _tabsHeight);
    barChartNodes = new StatsLeft(_visHeight, _statsWidth, _canvas);

    setup();

  }


  computeDimensions(){
    num dpr = window.devicePixelRatio;

    _visWidth = _visDiv.clientWidth; //window.screen.available.width;
    _visHeight = _visDiv.clientHeight; //window.screen.available.height;

    _canvas.width = (_visWidth * dpr).toInt();
    _canvas.height = (_visHeight * dpr).toInt();

    _canvasOverlay.width = _canvas.width;
    _canvasOverlay.height = _canvas.height;

    _canvas.style.width = _visWidth.toString() + "px";
    _canvas.style.height = _visHeight.toString() + "px";

    _canvasOverlay.style.width = _visWidth.toString() + "px";
    _canvasOverlay.style.height = _visHeight.toString() + "px";

    _context.scale(dpr, dpr);

    CANVAS_HEIGHT = _canvas.height;

    nodeRenderer._frameHeigth = _canvas.height - nodeRenderer._topLeftY;

    tsLabelsRenderer._width = _visWidth;

    tsTabs._width = _visWidth;

    updateStatsMenuDimensions();

  }

  updateStatsMenuDimensions(){
    num _canvasTop = _canvas.getClientRects()[0].top;

    selectStatisticsTs.updateDims(_canvasTop-20+_statsHeight+ _tabsHeight, _statsWidth, nodeRenderer.frameWidth);
    selectStatisticsNodes.updateDims( _canvasTop+_statsHeight+ _tabsHeight, 0, _statsWidth);
  }

  void saveCanvas(AnchorElement element, String fileName){
    var canvasData = _canvas.toDataUrl("image/png", 1);
    element.download = fileName;
    element.href = canvasData;
  }



  void reset() {
    onMouseMove.cancel();
    onMouseUp.cancel();
    onMouseLeave.cancel();
    onMouseDown.cancel();
    onKeyDown.cancel();
    onClick.cancel();
    onDblClick.cancel();
    onMouseWheel.cancel();

//    onDivResizeEvent.cancel();
//    onWindowResizeEvent.cancel();

    NodeFilterChangedEvent.cancel();
    NodeSliderChangedEvent.cancel();
    NodeScrolledEvent.cancel();

    TimeSliderChangedEvent.cancel();
    TimeSlotsScrolledEvent.cancel();
    TimeSlotsMovedEvent.cancel();

    TimeSlotsStatisticsChangedEvent.cancel();
    NodeStatisticsChangedEvent.cancel();

  }

  void clearCanvas() {
    _context.clearRect(0, 0, _visWidth, _visHeight);
    _context
      ..fillStyle = Theme.canvasBackground
      ..strokeStyle = Theme.canvasBorderBackground
      ..globalAlpha = 1;
    _context
      ..beginPath()
      ..rect(0, 0, _visWidth, _visHeight)
      ..closePath()
      ..fill()
      ..stroke();
  }

  void setup(){
  //print("all setup");
  //tm.setOffsetAll(tm.getPosX1(ts));
//    else{
//      el.computeDimensions();
//    }
    if(SHOW_HYPERGRAPH) {
      if(!_showingHyper) {
        edgeRenderer.switchToHyper();
        edgeRenderer.updateValid();
      }
      _showingHyper = true;
    }
    else{
      if(_showingHyper) {
        edgeRenderer.switchToSimple();
        edgeRenderer.updateValid();
      }
      _showingHyper = false;
    }

    nodeRenderer.setup();
    edgeRenderer.setup();
    tooltip.setup();

    tm.margin = nodeRenderer.frameWidth + nodeRenderer._topLeftX;
    tm.width = _visWidth - nodeRenderer.frameWidth;
    tm.setup();

    tsm.setup();

    updateNodesVisibility();
    updateTSVisibility();

    updateVisSpace();
    updateNodeSpace();

  }

  void searchByName (String name){
    resetSelectStatusAll();

    if(name.length < 2) return;

    List<Node> nn = graph.nodes.searchbyName(name);

//    if(nn.length == 1) _searchName.value = nn[0].name;

    if (DEBUG) print("searching " + name);
    if(nn.length >=  1) {
      nn.forEach((Node n) {
//        print('is a match ' + n.name);
        markNodeSelected(n);

//        TS_SELECTED = true;
        selectEdgesWithNode(n);
        NODE_SELECTED = true;
      });
    }
//    if(ENABLED_FILTER){
      filterSelectedNodes(NODE_SELECTED);
      filterSelectedNodes(NODE_SELECTED);
//    }

    updateEdgeSelects(getEdgeSelections());

  }

  void setupEvents(){

    //print("setup events");

    NodeFilterChangedEvent = eventBus.on<NodeFilterChanged>().listen((NodeFilterChanged event) {
      updateFilteredNodes();

    });


    NodeSliderChangedEvent = eventBus.on<NodeSliderChanged>().listen((NodeSliderChanged event){
      if(event.delta != 0) {
        slideNodes(event.delta);
        updateNodeSpace();

        updateNodeHighlights(getNodeHighlights());
//        updateEdgeHighlights(edgeHighlights);

//        updateEdgeSelects(edgeSelects);
      }

    });

    NodeScrolledEvent = eventBus.on<NodesScrolled>().listen((NodesScrolled event) {
      if(event.delta != 0) {
        slideNodes(event.delta);
        updateNodeSpace();
      }

    });


    TimeSliderChangedEvent = eventBus.on<TimeSliderChanged>().listen((TimeSliderChanged event) {
      tsm.update(event.text);
      int tsIdx =  int.parse(event.text);
      setMinTs(tsIdx);
//      eventBus.fire(new TimeSlotsChanged());

      updateTsSpace();

    });

    TimeSlotsScrolledEvent = eventBus.on<TimeSlotsScrolled>().listen((TimeSlotsScrolled event) {
      int ts =  event.delta;
      shiftTs(ts);
      tsm.update(tm.firstVisibleIdx().toString());

//      eventBus.fire(new TimeSlotsChanged());

      updateTsSpace();
      //      updateNodeSpace();

    });

    TimeSlotsMovedEvent = eventBus.on<TimeSlotsMoved>().listen((TimeSlotsMoved event) {
      num posX =  event.posX;

      slideTsTo(posX);
      render();

    });

    TimeSlotsStatisticsChangedEvent = eventBus.on<TimeSlotsStatisticsChanged>().listen((TimeSlotsStatisticsChanged event) {

     updateStatisticsTop(event.stat);
     updateStatisticsSelectedTop(event.stat);
     barChartTs.render();

     _canvas.focus();

    });

    NodeStatisticsChangedEvent = eventBus.on<NodeStatisticsChanged>().listen((NodeStatisticsChanged event) {

      updateStatisticsLeft(event.stat);
      updateStatisticsSelectedLeft(event.stat);
      barChartNodes.render();

      _canvas.focus();

    });


  }

  void slideTsTo(num posX){
    tm.setTranslateAll(posX+tm.margin);

    updateTSVisibility();

    tsm.update(tm.firstVisibleIdx().toString());

//      eventBus.fire(new TimeSlotsChanged());

    updateTsSpace();
  }
  void updateEdgeHighlights(List<Edge> highlights){

    List<num> highlightsPos = new List<num>();
    highlights.forEach((Edge e) => highlightsPos.add(e.xPos));

    eventBus.fire(new UpdateEdgeHighlights(highlightsPos));

  }

  void updateNodeHighlights(List<Node> highlights){

    List<num> highlightsPos = new List<num>();
    highlights.forEach((Node n) => highlightsPos.add(n.y));

    eventBus.fire(new UpdateNodeHighlights(highlightsPos));
  }
  void updateEdgeSelects(List<Edge> selects){

    if(selects.length==0){
      _bib.style.visibility="hidden";
    }
    List<num> selectsPos = new List<num>();
    selects.forEach((Edge e) => selectsPos.add(e.xPos));

    eventBus.fire(new UpdateEdgeSelects(selectsPos));
  }

  void updateNodeSelects(List<Node> selects){
    if(selects.length==0){
      _bib.style.visibility="hidden";
    }

    List<num> selectsPos = new List<num>();
    selects.forEach((Node n) => selectsPos.add(n.y));

    eventBus.fire(new UpdateNodeSelects(selectsPos));
  }

  void updateNodeSpace(){

    nodeRenderer.updateMinMax();

    num n0 = nodeRenderer._minY;
    num n1 = nodeRenderer._maxY;

    showVerticalScroll();

    eventBus.fire(new VertScrollNodeSpaceChanged(n0, n1));

    updateNodeSelects(getNodeSelections());

    updateStatisticsLeft(barChartNodes.statsName);
    updateStatisticsTop(barChartTs.statsName, true);

    updateStatisticsSelectedLeft(barChartNodes.statsName);
    updateStatisticsSelectedTop(barChartTs.statsName);

    barChartTs.render();
    barChartNodes.render();

  }


  void updateTsSpace(){

    num n0 =  tm.getPosX1(tm.timeList.first);
    num n1 =  tm.getPosX2(tm.timeList.last);

    showHorizontalScroll();

    eventBus.fire(new TimeScrollTsSpaceChanged(n0, n1));
    updateEdgeSelects(getEdgeSelections());

    updateStatisticsTop(barChartTs.statsName, true);

  }
  bool showVerticalScroll(){
    bool show = ((nodeRenderer._maxY) > _visHeight || (nodeRenderer._minY) < getVerticalMin());

    vertScrollDiv.style.display = show? 'block': 'none';

    //adapt the render position of the scrollbar based on the presence of the
    //bibfile element
    if(_bib.clientWidth != 0) {
      //render the vertical scrollbar of a distance from left that is equal to the
      // sum of the #visCanvas width and the width of the vertical scrollbar
      vertScrollDiv.style.setProperty("left",
          (_visDiv.scrollWidth + vertScrollDiv.scrollWidth).toString() + "px");
    }else{
      vertScrollDiv.style.removeProperty("left");
    }
    return show;
  }

  bool showHorizontalScroll(){

    bool show =  (tm.getPosX2(tm.timeList.last)) > _visWidth || (tm.getPosX1(tm.timeList.first)) < tm.margin;
    horScrollDiv.style.display = show? 'block': 'none';

    //adapt the width of the scrollbar based on the presence of the bibfile element
    if(_bib.clientWidth != 0) {
      //set the width of the horizontal scrollbar as the sum of the #visCanvas
      // width and the width of the vertical scrollbar
      horScrollDiv.style.setProperty("width",
          (_visDiv.scrollWidth + vertScrollDiv.scrollWidth).toString() + "px");
    }else{
      horScrollDiv.style.removeProperty("width");
    }
    return show;
  }
  num getVerticalMax(){
    return _visHeight - (showHorizontalScroll()? horScrollDiv.client.height: 0);
  }

  num getVerticalMin(){
    return tsLabelsRenderer.height + barChartTs.height + _tabsHeight;
  }

  void updateVisSpace(){
    num vy0 = getVerticalMin();
    num vy1 = getVerticalMax();

    barChartNodes._topCorner = nodeRenderer._topLeftY;
    barChartTs._leftCorner =  tm.margin;

    barChartNodes.updateStatsHeight(_visHeight);
    barChartTs.updateStatsWidth(_visWidth);

    eventBus.fire(new VertScrollVisLimitsChanged(vy0, vy1, 0.0));

    num x0 = nodeRenderer.frameWidth + nodeRenderer._topLeftX;

    num vx0 = tm.margin; //tm.getPosX1(firstTs);
    num vx1 = tm.width + tm.margin;
    eventBus.fire(new TimeScrollVisLimitsChanged(vx0, vx1, x0));// x0


//    scTimeSlots.setupDims();
//    scTimeSlots.update();
  }

  void setMinTs(tsIdx){
    double shift = tm.getShift(tsIdx) - tm.margin;
//      double shift = tm.getPosX1(ts);

    shiftTs(shift);
  }

  void shiftTs(shift){
    tm.translateAll(-shift);

    updateTSVisibility();

    render();
  }

  interactionHighlights(MouseEvent e){
    bool needsRender = false;

    resetHighlightStatusAll();

    if (highlightEdge(e)) {
      needsRender = true;
    }
    else if (highlightTimeSlot(e)) {
      needsRender = true;
    }
    else if (highlightNodes(e)){
      needsRender = true;
    }

    if(needsRender){
      updateStatisticsHighlightedTop(barChartTs.statsName);
      updateStatisticsHighlightedLeft(barChartNodes.statsName);
//          stl.updateStatsHigh(numberOfHighlightedNodesbyTs());

    }else{
      updateStatisticsHighlightedTop(null);
      updateStatisticsHighlightedLeft(null);
//          stl._statsHigh = null;
    }

    needsRender = true; // TODO: this should be false, need to add method that check if highlighted status changes

    edgeHighlights =  getEdgeHighlights();
    nodeHighlights =  getNodeHighlights();

    updateNodeHighlights(nodeHighlights);
    updateEdgeHighlights(edgeHighlights);

    return needsRender;
  }

  bool interactionSelects(MouseEvent e){

    bool somethingSelected = true;
    //        bool selectAdding = e.shiftKey;
    bool selectAdding = e.ctrlKey || e.metaKey || e.shiftKey;

    if (!selectAdding) {
      nodeSelects.clear();
      edgeSelects.clear();
      if(!SHOW_TABS){
      tsSelects.clear();
      }
    }

    if(SHOW_TABS && selectAdding){
      tsSelects.clear();
    }

    //try to select Edge
    Point p = e.offset;
    Edge edge = edgeUnderCursor(p);

    if (edge != null) {
      int iedge = edgeSelects.indexOf(edge);
      if (iedge != -1) {
        edgeSelects.removeAt(iedge);
      } else {
        edgeSelects.add(edge);
      }
    } else {
      String ts = timeSlotUnderCursor(p);
      num selectTop = tsLabelsRenderer._topMargin;
      num selectBottom = tsLabelsRenderer.height + tsLabelsRenderer._topMargin;
      TimeManager tm = new TimeManager();
      num maxWidth = tm.getWidth(ts);
      num halfWidth = tm.getPosX1(ts) + tsTabs._tabMargin;
      num checkSpace = halfWidth + maxWidth; //checkspace is equal TS width
      num minimizeSpace = checkSpace;
      if (SHOW_TABS) {
        if (maxWidth > (tsTabs.height + tsTabs._tabMargin)) {
          checkSpace = (maxWidth / 2) + halfWidth; //checkspace is equal half TS width
        }else{
          minimizeSpace = 0;
        }
        selectTop = tsTabs._topMargin;
        selectBottom = tsTabs.height + tsTabs._topMargin;
      }
      if (p.y > selectTop && //pointer under top limit for selection
          p.y < selectBottom && //pointer up to bottom limit for selection
          p.x < checkSpace &&  //pointer on the check space
          ts != null) {
        int its = tsSelects.indexOf(ts);
        if (its != -1) {
          tsSelects.removeAt(its);
        } else {
          tsSelects.add(ts);
        }
      } else if(
          p.y > selectTop && //pointer under top limit for selection
          p.y < selectBottom && //pointer up to bottom limit for selection
          p.x > checkSpace &&   //pointer on the check space
          p.x < minimizeSpace &&
          ts != null){
            tm.setMinimized(ts, true);
            updateVisDimensions();
      } else {
//        print(halfWidth + edgeRenderer.minWidth);
        if(
            tm.isMinimized(ts) &&
            p.y > selectBottom && //pointer up to bottom limit for selection
            p.x < checkSpace &&   //pointer on the check space
            ts != null){
          tm.setMinimized(ts, false);
          updateVisDimensions();
        }
        Node n = nodeUnderCursor(p, ts);

        if (n != null) {
          int inode = nodeSelects.indexOf(n);
          if (inode != -1) {
            nodeSelects.removeAt(inode);
          } else {
            nodeSelects.add(n);
          }
        } else {
          nodeSelects.clear();
          edgeSelects.clear();
          tsSelects.clear();

          updateStatisticsSelectedTop(null);
          updateStatisticsSelectedLeft(null);

          somethingSelected = false;
        }
      }
    }

    updateSelections();

    markIntersections();


    if (somethingSelected) {
      updateStatisticsSelectedTop(barChartTs.statsName);
      updateStatisticsSelectedLeft(barChartNodes.statsName);
    }

    updateNodeSelects(getNodeSelections());
    updateEdgeSelects(getEdgeSelections());
//      if(ENABLED_FILTER){
//        filterSelectedNodes(NODE_SELECTED);
//      }
    return somethingSelected;
  }

  void setupInteraction(){
    onMouseMove = _canvasOverlay.onMouseMove.listen((MouseEvent e) {

      nodeHighlights.clear();
      edgeHighlights.clear();
      bool needsRender = true;

      if (ENABLED_HIGHLIGHT) {
        needsRender = interactionHighlights(e);
      }
      if (needsRender) render();
    });

    onMouseUp = _canvasOverlay.onMouseUp.listen((MouseEvent e){
//      _canvasOverlay.style.cursor = 'default';
//      _mouseDown = false;
    });
    onMouseLeave = _canvasOverlay.onMouseLeave.listen((MouseEvent e){
//      _canvasOverlay.style.cursor = 'default';
//      _mouseDown = false;
      updateStatisticsHighlightedTop(null);
      updateStatisticsHighlightedLeft(null);
      resetHighlightStatusAll();
      render();
    });
    onMouseDown = _canvasOverlay.onMouseDown.listen((MouseEvent e){
//      _mouseDown = true;
    });
    onClick = _canvasOverlay.onClick.listen((MouseEvent e) {

      resetTimeSlotSelectedStatus();
      resetEdgeSelectedStatus();
      resetNodeSelectedStatus();

//      bool somethingSelected = true;

      if (ENABLED_SELECTION) {
          interactionSelects(e);
      }
      render();
    });
    onDblClick = _canvasOverlay.onDoubleClick.listen((Event e){
//      if (ENABLED_SELECTION) { //ENABLED_SELECTION
//
////        bool selectAdding = e.shiftKey;
//        bool selectAdding = false;//e.ctrlKey || e.metaKey;
//        resetTimeSlotSelectedStatus(selectAdding);
//        resetEdgeSelectedStatus(selectAdding);
//        resetNodeSelectedStatus(selectAdding);

//        if(!selectEdge(e)) {
//          if (!selectTimeSlot(e))
//            selectNode(e);
//        }

      SHOW_CONTEXT = true;

//      }
//      if(ENABLED_FILTER){
        filterSelectedNodes(NODE_SELECTED);
//      }


//        updateStatisticsLeft(barChartNodes.statsName, true);
//        updateStatisticsTop(barChartTs.statsName, true);
      filterSelectedNodes(NODE_SELECTED);


      markIntersections();

      render();
    });
    onMouseWheel = _canvasOverlay.onMouseWheel.listen((WheelEvent e){
      eventBus.fire(new NodeSliderChanged(e.deltaY));
      e.preventDefault();
      return false;
    });

    onKeyDown = document.onKeyDown.listen( (KeyboardEvent ke) {

      bool changeEdgeHighlights = false;
      bool changeNodeHighlights = false;

      if(ke.ctrlKey && ke.shiftKey && ke.keyCode == KeyCode.S){
//        print('hh');

        _saveCanvas.dispatchEvent(new MouseEvent('click'));
      }
      else if(ke.ctrlKey && ke.shiftKey && ke.keyCode == KeyCode.P){

//        graph.printUpset();
          graph.printCounts();
      }



      if (ke.keyCode == KeyCode.UP) {
        if(_currentNodeHighlight == -1)
          _currentNodeHighlight = graph.nodes.valid.length-1;
        else
          _currentNodeHighlight = (_currentNodeHighlight-1)%graph.nodes.valid.length;
        changeNodeHighlights = true;
      }else if (ke.keyCode == KeyCode.DOWN) {
        if(_currentNodeHighlight == -1)
          _currentNodeHighlight = 0;
        else
          _currentNodeHighlight = (_currentNodeHighlight+1)%graph.nodes.valid.length;
        changeNodeHighlights = true;
      }else if (ke.keyCode == KeyCode.RIGHT) {
           if(NODE_HIGHLIGHTED && !TS_HIGHLIGHTED) {

            if(_currentEdgeHighlight == -1)
                _currentEdgeHighlight = 0;
              else
                _currentEdgeHighlight = (_currentEdgeHighlight+1)%edgeHighlights.length;
              EDGE_HIGHLIGHTED = true;
              changeEdgeHighlights = true;
          }else if(EDGE_HIGHLIGHTED) {
             if (ke.keyCode == KeyCode.RIGHT) {
               if (_currentEdgeHighlight == -1)
                 _currentEdgeHighlight = 0;
               else
                 _currentEdgeHighlight =
                     (_currentEdgeHighlight + 1) % edgeHighlights.length;
               changeEdgeHighlights = true;
             }
           }
      } else if (ke.keyCode == KeyCode.LEFT) {
          if(NODE_HIGHLIGHTED && !TS_HIGHLIGHTED) {

            if(_currentEdgeHighlight == -1)
              _currentEdgeHighlight = edgeHighlights.length-1;
            else
              _currentEdgeHighlight = (_currentEdgeHighlight - 1) % edgeHighlights.length;

            EDGE_HIGHLIGHTED = true;
            changeEdgeHighlights = true;

          }else if(EDGE_HIGHLIGHTED){
            if(_currentEdgeHighlight == -1)
              _currentEdgeHighlight = edgeHighlights.length-1;
            else
              _currentEdgeHighlight = (_currentEdgeHighlight-1)%edgeHighlights.length;
            changeEdgeHighlights = true;
          }
      }

      if(changeEdgeHighlights){
        edgeHighlights.forEach((Edge e){
          e.unhighlight();
          e.sideHighlight();
        });

        if(_currentEdgeHighlight >=0){
          edgeHighlights[_currentEdgeHighlight].highlight();
          scrollIfEdgeOutsideVisibleSpace();
          showEdgeTooltip(edgeHighlights[_currentEdgeHighlight]);
        }


//        updateStatisticsHighlightedTop(barChartTs.statsName);
//        updateStatisticsHighlightedLeft(barChartNodes.statsName);
        render();

        updateNodeHighlights(nodeHighlights);
        updateEdgeHighlights(edgeHighlights);

      }

      if(changeNodeHighlights){
        _currentEdgeHighlight = -1;

        resetHighlightStatusAll();

        if(_currentNodeHighlight >=0){


          graph.nodes.valid.elementAt(_currentNodeHighlight).highlight();

          highlightEdgesWithNode(graph.nodes.valid.elementAt(_currentNodeHighlight));

          scrollIfNodeOutsideVisibleSpace();

          NODE_HIGHLIGHTED = true;
          DRAW_BAR = true;
        }

        nodeHighlights = getNodeHighlights();

        render();

        edgeHighlights = getEdgeHighlights();

        updateStatisticsHighlightedTop(barChartTs.statsName);
        updateStatisticsHighlightedLeft(barChartNodes.statsName);

        updateNodeHighlights(nodeHighlights);
        updateEdgeHighlights(edgeHighlights);
        }


    });
  }

  scrollIfEdgeOutsideVisibleSpace(){
    num edgePos = edgeHighlights[_currentEdgeHighlight].xPos;
    num visYMax = _visWidth - (showVerticalScroll()? 20: 0);
    num visYMin = tm.margin;

    if(edgePos > visYMax || edgePos < visYMin){
      slideTsTo(-(edgePos-tm.margin - EDGE_SEP*SCALE*2));
    }
  }

  scrollIfNodeOutsideVisibleSpace(){
    num nodePos = graph.nodes.valid.elementAt(_currentNodeHighlight).y;
    num visXMax = getVerticalMax();
    num visXMin = getVerticalMin();

    num nodeHeight = SCALE*NODE_HEIGHT_DIM;

    if(nodePos+nodeHeight > visXMax){
      slideNodes(nodePos-visXMax + nodeHeight*2);
      updateNodeSpace();
    }else if(nodePos-nodeHeight < visXMin){
      slideNodes(-(visXMin - nodePos + nodeHeight*2));
      updateNodeSpace();
    }
  }

  updateVisDimensions(){
    computeDimensions();
    setup();
    render();
  }

  num verticalMargins(){
    return barChartTs.height + tsLabelsRenderer.height + _tabsHeight;
  }

  num getFitNodesScale(){
//    print('lengths');
//    print(graph.nodes.valid.length);
//    print(graph.nodes.length);
    return (_visHeight-verticalMargins())/((graph.nodes.valid.length*SCALE)*2);
//    return (_visHeight-topMargin())/(graph.nodes.valid.length*NODE_HEIGHT_DIM*2);
  }

  num getFitTsScale(){
    return (_visWidth-tm.margin)/(tm.getWidthAllN());
  }

  void scaleTimeSlot(num scale){
    tm.forEach((ts){
      tm.scaleWidth(scale);
    });
  }

  bool setTooltip(Point p, String message){
    // take info for tooltip and send to tt.setTooltip
    //...
    tooltip.setTooltip(p, message);
    return true; // for future needs
  }

  bool setTooltipPos(Point p){
    // take info for tooltip and send to tt.setTooltip
    //...
    tooltip.setTooltipPos(p);
    return true; // for future needs
  }

  bool setTooltipAlignRight(bool alignRight){
    // take info for tooltip and send to tt.setTooltip
    //...
    tooltip.setTooltipAlignRight(alignRight);
    return true; // for future needs
  }

  bool setTooltipMessage(String message){
    // take info for tooltip and send to tt.setTooltip
    //...
    tooltip.setTooltipMessage(message);
    return true; // for future needs
  }

  void setTooltipVisible(bool vis){
    tooltip.setTooltipVisible(vis);
    if (!ENABLED_TOOLTIP) {
      _visDiv.style.width = "100%";
      _bib.style.display = "none";
    } else {
      _visDiv.style.width = "75%";
      _bib.style.display = "block";
    }
  }


  /// renders everything
  void render(){
    // clear the canvas
    clearCanvas();

//    el.updateStatus();
    // redraw

    nodeRenderer.renderSparklines();
    if(PAOVIS || CURVES || SPLAT || NODELINK) {
      edgeRenderer.render();
    }
    if(SHOW_TABS){
      tsTabs.renderTabs();
    }
    tsLabelsRenderer.renderLabels();
    nodeRenderer.renderNodes();
    tooltip.render();
    barChartTs.render();
    barChartNodes.render();

    _context
      ..fillStyle = Theme.canvasBackground
      ..strokeStyle = Theme.canvasBorderBackground
      ..globalAlpha = 1;

    _context
      ..beginPath()
      ..rect(0, 0, tm.margin, nodeRenderer._topLeftY)
      ..closePath()
      ..fill()
      ..stroke();

    num w = tm.getWidthAll() + nodeRenderer.frameWidth;

    _context
      ..strokeStyle = "#333333";

//    print(nodeHighlights);

    nodeHighlights.forEach((Node n){
      if (n.isHighlighted && DRAW_BAR) {
        if(n.y > verticalMargins()){
          _context
            ..beginPath()
            ..moveTo(nodeRenderer._topLeftX, n.y - n.height / 2)
            ..lineTo(nodeRenderer._topLeftX+w,  n.y - n.height / 2 )
            ..moveTo(nodeRenderer._topLeftX, n.y + n.height + 3)
            ..lineTo(nodeRenderer._topLeftX+w,  n.y + n.height +3)
  //          ..(nl._topLeftX, n.y - n.height / 2, w, n.height * 2)
            ..closePath()
            ..stroke();
        }
      }

    });

    _context
      ..strokeStyle = "#ffffff"
      ..fillStyle = "#ffffff"
    ;


    if(showVerticalScroll() || showHorizontalScroll()){
      _context
        ..beginPath()
        ..rect(_visWidth-20, _visHeight-20, 20, 20)
        ..closePath()
        ..fill()
        ..stroke();

    }

  }


  void resetHighlightStatusAll(){
    nodeHighlights.clear();
    edgeHighlights.clear();

    _currentEdgeHighlight = -1;

    updateNodeHighlights(nodeHighlights);
    updateEdgeHighlights(edgeHighlights);

    if(ENABLED_HIGHLIGHT) {
      resetTimeSlotHighlightedStatus();
      resetEdgeHighlightedStatus();
      resetNodeHighlightedStatus();
    }
  }

  void resetSelectStatusAll(){

    SHOW_CONTEXT = true;

    nodeSelects.clear();
    edgeSelects.clear();
    tsSelects.clear();


    if(ENABLED_SELECTION || ENABLED_FILTER) {
      resetNodeFilterStatus();
      resetTimeSlotSelectedStatus();
      resetEdgeSelectedStatus();
      resetNodeSelectedStatus();
    }

    updateNodeSelects(getNodeSelections());
    updateEdgeSelects(getEdgeSelections());


  }
  /** ----------------------------- **/
  /** NODE highlight/select **/

  bool isNodeUnderCursor(Point p, Node n, [String ts=null]){
    if(p.y < verticalMargins()) return false; // select only in left frame

    if(p.x > tm.getPosX2(tm.timeList.last)) return false; // select only in left frame

    if(p.x < nodeRenderer._topLeftX) return false; // highlight only in left frame

    if(NODELINK && ts != null){
      num nX = edgeRenderer.nlPosX(n, ts);
      if(p.x < (nX - n.height) || p.x > (nX + n.height)) return false;

      num nY = edgeRenderer.nlPosY(n);
      if(p.y < (nY - n.height) || p.y > (nY + n.height)) return false;
      return true;
    }
    return p.y > n.y - n.height/2  && p.y < n.y + n.height*2;
  }

  Node nodeUnderCursor(Point p, [String ts = null]){
    if(p.x > tm.getPosX2(tm.timeList.last)) return null; // select only in not white space
    for(Node n in graph.nodes){
      if(n.isValid) {
        if (isNodeUnderCursor(p, n, ts)) {
//        if (inRect(p, n.x, n.y, n.x + n.width, n.y + n.height)) {
          return n;
        }
      }
    }
    return null;
  }

  void resetNodeHighlightedStatus(){
    NODE_HIGHLIGHTED = false;
    DRAW_BAR = false;

    setTooltipVisible(false);

    graph.nodes.forEach((n){
      n.isAdjacentToHighlighted = false;
      n.unhighlight();
      n.slm.isHighlighted = false;
    });
  }

  void resetNodeSelectedStatus([bool selectAdding = false]){
    if(selectAdding) return;

    NODE_SELECTED = false;
    graph.nodes.forEach((var n){
      n.isAdjacentToSelected = false;
      n.unselect();
      n.slm.isSelected = false;
    });
  }

  void resetNodeFilterStatus(){
    filterSelectedNodes();
  }

  void highlightNodesWithEdge(Edge e){

    e.edge.forEach((Node n){
      markNodeHighlighted(n);
    });

  }

  void selectNodesWithEdge(Edge e){
    List<Node> eF = e.edge;
    if(!AND_SELECTIONS_FILTERS) {
      eF = e.edgeUnfiltered;
    }

    eF.forEach((Node n) {
      n.isAdjacentToSelected = true;
//      markNodeSelected(n);
    });

    NODE_SELECTED = true;
  }

  /** returns true if a Node was highlighted */
  bool highlightNodes(MouseEvent e){
    Point p = e.offset;
//    if(p.x > NODES_FRAME_WIDTH) return false; // select only in left frame
    if(p.x < nodeRenderer._topLeftX) return false; // highlight only in left frame


    String ts = timeSlotUnderCursor(p);

    Node n = nodeUnderCursor(p, ts);
    if(n == null) return false;

    markNodeHighlighted(n);
    n.slm.isHighlighted = true;

    DRAW_BAR = true;

//    if(el.edges[ts].length == 0) return false;

    if(ENABLED_TOOLTIP){
//      if(NODELINK){
      String tooltip_node = "<span>" +  n.name.toString() + "</span>" ; //
      String tooltipCommunity = null;
      if(ts!=null && n.getTsCommunity(ts) !=null){
        int idCom = n.getTsCommunity(ts).toInt();
        tooltipCommunity = graph.communities.getComName(idCom);
      }

//        else{
//          if(n.hasCommunity) tooltipCommunity = n.community;
//        }

      if( tooltipCommunity!= null){
        tooltip_node += "<br><span> Group: " + tooltipCommunity +"</span>";
      }

      setTooltipMessage(tooltip_node);

      setTooltipVisible(true);
      setTooltipPos(new Point(e.client.x+4, e.client.y-20));
      setTooltipAlignRight(false);
//      }
//      else
//        setTooltipPos(new Point(0,0));
    }

    if(ts!=null) {
      bool anyEdgeInTs = false;
      for (Edge e in edgeRenderer.edges[ts]) {
        if (e.nodeSet.has(n.id)) {
          anyEdgeInTs = true;
          break;
        }
      }
      if (!anyEdgeInTs) return false;
    }

    if(ts != null) {
      TS_HIGHLIGHTED = true;
      tm.highlight(ts);
    }


    highlightEdgesWithNode(n);
    NODE_HIGHLIGHTED = true;

//    highlightTimeSlotsWithNode(n); // this is currently inside highlightEdgesWithNode TODO: should it be outside?


    return true;
  } //must return always true since it has to ping every node

  /** returns true if a Node was selected */
  bool selectNode(Node n, String ts) {
    if (n == null) {
      _bib.style.visibility="hidden";
      return false;
    }

    if(ts!=null) {
      bool anyEdgeInTs = false;
      for (Edge e in edgeRenderer.edges[ts]) {
        if (e.nodeSet.has(n.id)) {
          anyEdgeInTs = true;
          break;
        }
      }
      if (!anyEdgeInTs) return false;
    }

    if(ts != null) {
      TS_SELECTED = true;
      tm.select(ts);
    }

//    if(n.isAdjacentToSelected) {
//      //this means there's an intersection
//
//    }
    markNodeSelected(n);

    selectEdgesWithNode(n);

      if (ENABLED_TOOLTIP) {
        divcontainer.children.clear();
        //  _bib.setInnerHtml("<h3>You have selected:</h3>");
        _bib.children.add(divcontainer);
        edgeRenderer._edges.forEach((String ts, List<Edge> edge){
          edge.forEach((Edge e){
            var buf= new StringBuffer();
            String authors, description, year, booktitle, entrytype, biburl;
            List<Node> listnode=e.edge;
            if(listnode.contains(n)){
              listnode.forEach((no){
                buf.write(no.name.toString());
                buf.write(", ");
              });

            authors = buf.toString().substring(0, buf.toString().length - 2);
            description = e.meta["name"].toString();
            booktitle = e.meta["booktitle"].toString();
            entrytype = e.meta["entryType"].toString();
            biburl = e.meta["biburl"].toString() + ".bib";
            year = ts;

            setBibList(authors, description, booktitle, year, biburl);
          }
        });
      });
      // setTooltip(e.offset, n.name.toString() + "   com: " + n.community);
    }
    NODE_SELECTED = true;

    return true;
  }

  markNodeSelected(Node n){
    n.select();
//    nodeSelects.add(n);
    n.slm.isSelected = true;


    selectedNodeSet.add(n.id);

    NODE_SELECTED = true;
  }

  markEdgeSelected(Edge edge){
    edge.select();
//    edgeSelects.add(edge);

    NODE_SELECTED = true;
    EDGE_SELECTED = true;
  }

  markTsSelected(String ts){
    tm.select(ts);


    TS_SELECTED = true;
  }

  markNodeHighlighted(Node n){
    n.highlight();
//    selectedNodeSet.add(n.id);
  }




  /** ----------------------------- **/
  /** TS highlight/select **/

  bool isTimeSlotUnderCursor(ts, p){
    num tsx1 = tm.getPosX1(ts);
    num tsx2 = tm.getPosX2(ts);
    return (p.x > tsx1) && (p.x < tsx2);
  }
  String timeSlotUnderCursor(Point p){
    if(p.x < tm.margin) return null;
    for(final ts in tm.timeList){
      if(isTimeSlotUnderCursor(ts, p)){
        return ts;
      }
    }
    return null;
  }

  void resetTimeSlotHighlightedStatus() {
    TS_HIGHLIGHTED = false;

    tm.anyTimeSlotHighlighted = false;
    tm.forEach((ts){
      tm.unhighlight(ts);
    });
  }

  void resetTimeSlotSelectedStatus([bool selectAdding = false]){
    if (selectAdding) return;

    TS_SELECTED = false;

    tm.anyTimeSlotSelected = false;
    tm.forEach((ts){
      tm.unselect(ts);
      tm.unsideselect(ts);
    });

  }

  bool selectTimeSlot(String ts){

    if(ts != null) {
      tm.select(ts);
      selectEdgesWithinTs(ts);

      TS_SELECTED = true;

      return true;
    }
    return false;
  }
  bool highlightTimeSlot(MouseEvent e){
    Point p = e.offset;
    if(p.y < tsLabelsRenderer._topMargin || p.y > (tsLabelsRenderer.height + tsLabelsRenderer._topMargin)) return false; // select only in left frame

    String ts = timeSlotUnderCursor(p);
    if(ts != null) {
      tm.highlight(ts);
      highlightEdgesWithinTs(ts);

      TS_HIGHLIGHTED = true;

      return true;
    }
    return false;
  }
  /** ----------------------------- **/


  /** ----------------------------- **/
  /** EDGE highlight/select **/

  void resetEdgeSelectedStatus([bool selectAdding = false]){

    edgeRenderer.edges.forEach((ts, tsEdges){
      tsEdges.forEach((Edge e){
        e.inIntersection = false;
      });
    });

    if(selectAdding) return;
    selectedNodeSet.clear();


    EDGE_SELECTED = false;

    edgeRenderer.edges.forEach((ts, tsEdges){
      tsEdges.forEach((Edge e){
        if(!selectAdding) {
          e.unselect();
          e.unsideSelect();
        }
      });
    });


  }

  void resetEdgeHighlightedStatus([bool selectAdding = false]){
    EDGE_HIGHLIGHTED = false;

    edgeRenderer.edges.forEach((ts, tsEdges){
      tsEdges.forEach((e){
          e.unhighlight();
          e.unsideHighlight();
      });
    });
  }


  bool isEdgeUnderCursor(Edge e, Point p){
    if(PAOVIS) {
      num margin = min(4, SCALE*EDGE_SEP);
      num fy = e.first.y + e.first.height / 2;
      num ly = e.last.y + e.last.height / 2 + (e.edgeUnfiltered.length - e.edge.length)*e.last.height*.7 ;
      return (fy - margin < p.y && p.y < ly + margin) && (e.xPos - margin < p.x && p.x < e.xPos + margin);
    }
    return false;
  }

  Edge edgeUnderCursor(Point p){
    if(PAOVIS) {
      if (p.x <= tm.margin) return null;

      // search which timeslot is under cursor
      String selTs = timeSlotUnderCursor(p);
      if (selTs == null) return null;

      for (List<Edge> bin in edgeRenderer.edgesBins(selTs)) {
        for (Edge e in bin) {
          if (isEdgeUnderCursor(e, p))
            return e;
        }
      }
    }
    return null;
  }


  void updateSelections(){

    for(Edge e in edgeSelects){
      markEdgeSelected(e);
      selectSimilarEdges(e);
      if (ENABLED_TOOLTIP) {
        divcontainer.children.clear();

        //  _bib.setInnerHtml("<h3>You have selected:</h3>");
        _bib.children.add(divcontainer);
        String authors, description, year, booktitle, entrytype, biburl;
        StringBuffer buf = new StringBuffer();
        List<Node> listnode = e.edge;
        listnode.forEach((no) {
          buf.write(no.name.toString());
          buf.write(", ");
        });

        //authors = buf.toString().substring(0, buf.toString().length - 2);
        description = e.meta["name"].toString();
        authors=e.meta["author"].toString();
        booktitle = e.meta["booktitle"].toString();
        entrytype = e.meta["entryType"].toString();
        biburl = e.meta["biburl"].toString() + ".bib";
        edgeRenderer._edges.forEach((String ts, ed) {
          ed.forEach((edges) {
            if (edges == e) year = ts;
          });
        });

        setBibList(authors, description, booktitle, year, biburl);

      }
    }

    for(String ts in tsSelects){
      markTsSelected(ts);
      selectEdgesWithinTs(ts);
    }

    for(Node n in nodeSelects){
      markNodeSelected(n);
      selectEdgesWithNode(n);

      if (ENABLED_TOOLTIP) {
        divcontainer.children.clear();
        //  _bib.setInnerHtml("<h3>You have selected:</h3>");
        _bib.children.add(divcontainer);
            edgeRenderer._edges.forEach((String ts, List<Edge> edge) {
              edge.forEach((Edge e) {
                var buf = new StringBuffer();
                String authors, description, year, booktitle, entrytype,
                    biburl;
                List<Node> listnode = e.edge;
                if (listnode.contains(n)) {
                  listnode.forEach((no) {
                    buf.write(no.name.toString());
                    buf.write(", ");
                  });

                /*  authors = buf.toString().substring(0, buf
                      .toString()
                      .length - 2);*/
                  authors=e.meta["author"].toString();
                  description = e.meta["name"].toString();
                  booktitle = e.meta["booktitle"].toString();
                  entrytype = e.meta["entryType"].toString();
                  biburl = e.meta["biburl"].toString() + ".bib";
                  year = ts;

                  setBibList(authors, description, booktitle, year, biburl);
                }
              });
            });

        // setTooltip(e.offset, n.name.toString() + "   com: " + n.community);
      }
    }
  }

  List<Node> getNodeSelections(){
    return graph.nodes.valid.where((Node n) => n.isSelected || n.isAdjacentToSelected ).toList();
  }

  List<Node> getNodeHighlights(){

    _currentNodeHighlight = -1;

    int ixn = 0;

    List<Node> highlightNodes =  List<Node>();

    graph.nodes.valid.forEach((Node n){
      if(n.isHighlighted){
        highlightNodes.add(n);
        _currentNodeHighlight = ixn;
      }else if(n.isAdjacentToHighlighted) {
        highlightNodes.add(n);
      }
      ixn++;
    });

    return highlightNodes;
  }

  List<Edge> getEdgeSelections(){

    List<Edge> selectedEdges =  List<Edge>();
     graph.edges.forEach((ts, edgesTs){
       edgesTs.forEach((Edge e){
         if(e.isSelected || e.isSideSelected)
           selectedEdges.add(e);
       });
     });
     return selectedEdges;

  }
  List<Edge> getEdgeHighlights(){
    _currentEdgeHighlight = -1;

    int ie = 0;

    List<Edge> highlightEdges =  List<Edge>();
//    graph.edges.forEach((ts, edgesTs){

      tm.timeList.forEach((String ts) {
        var edgesTs = graph.edges[ts];

        edgesTs.forEach((Edge e){
         if(e.isHighlighted) {
           highlightEdges.add(e);
           _currentEdgeHighlight = ie;
           ie++;
         }else if(e.isSideHighlighted){
           highlightEdges.add(e);
           ie++;
         }
       });
     });
     return highlightEdges;
  }

  void markIntersections(){

    if(selectedNodeSet.size() <=1) return;


    edgeRenderer.edges.forEach((ts, tsEdges){
//      if(!TS_SELECTED || tm.isSelected(ts)) {
        tsEdges.forEach((Edge e) {
//          FastBitSet visibleNodeSet = new FastBitSet(new List<int>());
//
//          e.forEach((Node n){
//            visibleNodeSet.add(n.id);
//          });

          if(selectedNodeSet.intersection_size(e.nodeSet) == selectedNodeSet.size())
            e.inIntersection = true;

        });
//      }
    });

  }

  showEdgeTooltip(Edge edge, [MouseEvent e = null]){
    if(ENABLED_TOOLTIP){
    //      if(NODELINK){
      String ttipMessage = "";
  //    if(edge.meta.containsKey('hal_docid')) {
  //      ttipMessage = halQuery(edge.meta['hal_docid']);
  //    }else
      if(edge.meta.containsKey('name')) {
        ttipMessage = edge.meta['name'];
      }
      setTooltipMessage(ttipMessage); // + "   com: " + n.community);

      if(e!=null){
        setTooltipPos(new Point(e.client.x, e.client.y - 20));
      }else{
        setTooltipPos(new Point(edge.xPos, _visDiv.offsetTop + edge.edge.first.y));
      }

      setTooltipAlignRight(true);

      setTooltipVisible(true);
    }

  }

  /** returns true if a Node was highlighted */
  bool highlightEdge(MouseEvent e){

    Point p = e.offset;

    Edge edge = edgeUnderCursor(p);
    if(edge == null) return false;

    FastBitSet visibleNodeSet = new FastBitSet(new List<int>());
    edge.forEach((var n){
      visibleNodeSet.add(n.id);
    });

    graph.edges.forEach((ts, edgesTs){
      edgesTs.forEach((Edge e){
        if(visibleNodeSet.intersection_size(e.nodeSet) == visibleNodeSet.size()){
          e.sideHighlight();
//          edgeHighlights.add(e);
          e.forEach((var ns){
            nodeHighlights.add(ns);
          });
        }
      });
    });

    edge.highlight();
//    edgeHighlights.add(edge);


    highlightNodesWithEdge(edge);

    EDGE_HIGHLIGHTED = true;

   showEdgeTooltip(edge, e);


    return true; //must return always true since it has to ping every node
  }

  /** returns true if the Point p falls into a node of the list */
  bool selectEdge(Edge edge){

    if(edge == null) return false;

    edge.select();
    edgeSelects.add(edge);

    selectNodesWithEdge(edge);

    EDGE_SELECTED = true;
    if (edge.isSelected) {
      if (ENABLED_TOOLTIP) {
        divcontainer.children.clear();

        //  _bib.setInnerHtml("<h3>You have selected:</h3>");
        _bib.children.add(divcontainer);
        String authors, description, year, booktitle, entrytype, biburl;
        StringBuffer buf = new StringBuffer();
        List<Node> listnode = edge.edge;
        listnode.forEach((no) {
          buf.write(no.name.toString());
          buf.write(", ");
        });

        authors = buf.toString().substring(0, buf.toString().length - 2);
        description = edge.meta["name"].toString();
        booktitle = edge.meta["booktitle"].toString();
        entrytype = edge.meta["entryType"].toString();
        biburl = edge.meta["biburl"].toString() + ".bib";
        edgeRenderer._edges.forEach((String ts, ed) {
          ed.forEach((edges) {
            if (edges == edge) year = ts;
          });
        });

        setBibList(authors, description, booktitle, year, biburl);
      }
    }
    return true;
  }

  void selectSimilarEdges(Edge edge){

    FastBitSet visibleNodeSet = new FastBitSet(new List<int>());
    edge.forEach((var n){
      visibleNodeSet.add(n.id);
    });

    graph.edges.forEach((ts, edgesTs){
      edgesTs.forEach((Edge e){
        if(visibleNodeSet.intersection_size(e.nodeSet) == visibleNodeSet.size()){

          tm.sideSelect(ts);

          e.sideSelect();
//          edgeSelects.add(e);

          e.edge.forEach((Node ns){
//            nodeSelects.add(ns);
              ns.isAdjacentToSelected = true;
          });

        }
      });
    });


  }



  void highlightEdgesWithinTs(String ts){
//    if(!tm.anyTimeSlotHighlighted) return;
    if (tm.isVisible(ts)) {
      if(!tm.isMinimized(ts)) {
//      bool tsStatus =  tm.isHighlighted(ts);
        edgeRenderer.edges[ts].forEach((e) {
          e.highlight();
//        edgeHighlights.add(e);

          highlightNodesWithEdge(e);
        });
      }
    }
  }

  void selectEdgesWithinTs(String ts){
//    if(!tm.anyTimeSlotHighlighted) return;
    if (tm.isVisible(ts)) {
      if (!tm.isMinimized(ts)) {
//      bool tsStatus =  tm.isHighlighted(ts);
        edgeRenderer.edges[ts].forEach((e) {
          e.select();
//        edgeSelects.add(e);

          selectNodesWithEdge(e);
        });
      }
    }
  }


  void highlightEdgesWithNode(Node n){

    edgeRenderer.edges.forEach((ts, tsEdges){
      if(!TS_HIGHLIGHTED || tm.isHighlighted(ts)){
        tsEdges.forEach((Edge e) {
          if (e.any((var n) => n.isHighlighted)) {
            e.highlight();
//            edgeHighlights.add(e);

            tm.highlight(ts);

            e.edge.forEach((Node na) {
              if (!na.isHighlighted) {
//                nodeHighlights.add(na);
                na.isAdjacentToHighlighted = true;
              }
            });
          }
        });
      }
    });

  }

  void selectEdgesWithNode(Node n){
    edgeRenderer.edges.forEach((ts, tsEdges){
//      if(!TS_SELECTED || tm.isSelected(ts)) {
        tsEdges.forEach((Edge e) {
          if (e.edgeUnfiltered.any((Node ni) => n.id == ni.id)) {
            e.select();

            tm.sideSelect(ts);

            List<Node> eF = e.edge;
            if(!AND_SELECTIONS_FILTERS)
              eF = e.edgeUnfiltered;

            eF.forEach((Node na) {
              if (!na.isSelected) {
                na.isAdjacentToSelected = true;
              }
            });
          }
        });
//      }
    });
  }

/** ----------------------------- **/

/**
 *  stats
 * */


  List<num> getTsPos(){
    List<num> pos = tm.timeList.map((ts) => tm.getPosX1Min(ts)).toList();
    return pos;
  }
  List<num> getNodesPos(){
    List<num> pos = graph.nodes.valid.map((n) => n.y+nodeRenderer.nodeHeight/2).toList();
    return pos;
  }


  List<num> numberOfEdgesbyTs(){
    return tm.timeList.map((ts) => graph.getNumberOfEdgesInTs(ts)).toList();
  }

  List<num> numberOfNodesbyTs(){
//    return tm.timeList.map((ts) => graph.getNumberOfNodesInTs(ts)).toList();
    return tm.timeList.map((ts) => graph.getNumberOfUniqueNodesInTs(ts)).toList();
  }


  List<num> averageNumberOfNodesbyEdge(){
    return tm.timeList.map((ts) => graph.getAverageNumberOfNodesbyEdgeinTs(ts)).toList();
  }
  List<num> averageNumberOfHighlightedNodesbyEdge(){
    return tm.timeList.map((ts) => graph.getAverageNumberOfHighlightedNodesbyEdgeinTs(ts)).toList();
  }
  List<num> averageNumberOfSelectedNodesbyEdge(){
    return tm.timeList.map((ts) => graph.getAverageNumberOfSelectedNodesbyEdgeinTs(ts)).toList();
  }

  List<num> numberOfHighlightedEdgesbyTs(){
    return tm.timeList.map((ts) => graph.getNumberOfHighlightedEdgesInTs(ts)).toList();
  }

  List<num> numberOfSelectedEdgesbyTs(){
    return tm.timeList.map((ts) => graph.getNumberOfSelectedEdgesInTs(ts)).toList();
  }

  List<num> numberOfHighlightedNodesbyTs(){
//    return tm.timeList.map((ts) => graph.getNumberOfNodesInTs(ts)).toList();
    return tm.timeList.map((ts) => graph.getNumberOfUniqueHighlightedNodesInTs(ts)).toList();
  }

  List<num> numberOfSelectedNodesbyTs(){
//    return tm.timeList.map((ts) => graph.getNumberOfNodesInTs(ts)).toList();
    return tm.timeList.map((ts) => graph.getNumberOfUniqueSelectedNodesInTs(ts)).toList();
  }


  List<num> numberOfEdgesbyNode(){
    return graph.nodes.valid.map((n) => n.degree).toList();
  }

  List<num> numberOfTsbyNode(){
    return graph.nodes.valid.map((n) => n.degreeUnique).toList();
  }

  List<num> numberOfHighlightedEdgesbyNode(){
    return graph.nodes.valid.map((n) => n.highDegree).toList();
  }

  List<num> numberOfHighlightedTsbyNode(){
    return graph.nodes.valid.map((n) => n.highDegreeUnique).toList();
  }

  List<num> numberOfSelectedEdgesbyNode(){
    return graph.nodes.valid.map((n) => n.selectedDegree).toList();
  }

  List<num> numberOfSelectedTsbyNode(){
    return graph.nodes.valid.map((n) => n.selectedDegreeUnique).toList();
  }


  /** ----------------------------- **/

/**
 *  vertices vertical movement
 * */

  void slideNodes(num delta){
//    int diffY = (-nl.nodeHeight*2*delta.abs()/delta).round();

    nodeRenderer.setOffset(-delta);
    updateNodesVisibility();

    render();
  }

  void sortNodesBy(SortingNodes ord, [bool desc]){
//    if(SortingNodes.community.index != ord.index){
      graph.resetNodesOrder();
//    }
    if(SortingNodes.barycentric.index == ord.index){
      List<List< double >> adj = graph.getAdjacency();

      var jsAdj =  new JsObject.jsify(adj);

      List<int> order = context.callMethod('barycentricOrder', [jsAdj]).cast<int>();

      graph.orderElements(order);

    } else if(SortingNodes.leafOrder.index == ord.index){
      List<List< double >> adj = graph.getAdjacency();
      var jsAdj =  new JsObject.jsify(adj );
      var order = context.callMethod('leafOrder', [jsAdj]).cast<int>();
      graph.orderElements(order);
    }else if(SortingNodes.rcm.index == ord.index){
      List<List< double >> adj = graph.getAdjacency();
      var jsAdj =  new JsObject.jsify(adj );
      var order = context.callMethod('reverseCuthillMckee', [jsAdj]).cast<int>();
      graph.orderElements(order);
    }
    else if(SortingNodes.spectralOrder.index == ord.index){
      List<List< double >> adj = graph.getAdjacency();
      var jsAdj =  new JsObject.jsify(adj );
      var order = context.callMethod('spectralOrder', [jsAdj]).cast<int>();
      graph.orderElements(order);
    }else if(SortingNodes.timeslot.index == ord.index){
      graph.sortNodes(ord, desc);
    }else if(SortingNodes.community.index == ord.index){
      graph.sortNodes(ord, desc);
    }
    else if(desc != null) {
      graph.sortNodes(ord, desc);
    }
    else {
      graph.sortNodes(ord, desc);
    }
    nodesVisChanged();

  }

  void updateNodesVisibility(){

    nodeRenderer.nodes.forEach((n){
      n.isVisible = n.isValid &&
          objInRect(
              new Rectangle(0, tsLabelsRenderer.height, _visWidth, _visHeight),
              new Rectangle(n.x, n.y, n.width * 2, n.height*2));
//      inVisibleCanvasArea(
//          canvas, n.width * 2, n.height * 2, new Point(n.x, n.y));


    });
  }

  void updateTSVisibility(){
//    print("widths");
//    print(canvas.width);
//    print(_winWidth);

    tm.forEach((ts) {
      bool isVisible = objInRect(
          new Rectangle(nodeRenderer.frameWidth, 0, _visWidth-nodeRenderer.frameWidth, _visHeight),
          new Rectangle(tm.getPosX1(ts), 0, tm.getWidth(ts), tsLabelsRenderer.height));
//      bool isVisible = inVisibleArea(
//          new Rectangle(nl.frameWidth, 0, _winWidth-nl.frameWidth, _winHeight), new Point(tm.getPosX1(ts), 0));
      tm.setVisible(ts, isVisible);
    });
  }

  void nodesVisChanged(){
    edgeRenderer.updateValid();  // should be called when nodes pos changes
    nodeRenderer.setup();
    updateNodesVisibility();
    edgeRenderer.setup();

    updateNodeSelects(getNodeSelections());

    updateStatisticsLeft(barChartNodes.statsName, true);
    updateStatisticsSelectedLeft(barChartNodes.statsName);

    render();
  }

  void updateStatisticsTop(String statName, [bool force=false]) {
    List<num> posStTop = getTsPos();

    if(barChartTs!=null) {

      if (posStTop.length != barChartTs.pos.length || barChartTs.statsName != statName || force) {
        List<num> statsTop = null;

        if (statName == "number of nodes") {
          statsTop = numberOfNodesbyTs();
        } else if (statName == "number of edges") {
          statsTop = numberOfEdgesbyTs();
        } else if (statName == "average nodes") {
          statsTop = averageNumberOfNodesbyEdge();
        }

        if (statsTop != null) barChartTs.updateStatsPos(statName, statsTop, posStTop);
      } else {
      barChartTs.updatePos(posStTop);
      }
    }
  }

  void updateStatisticsHighlightedTop(String statName) {
    if(barChartTs!=null) {
      List<num> statsTop = null;

      if (statName == "number of nodes") {
        statsTop = numberOfHighlightedNodesbyTs();
      } else if (statName == "number of edges") {
        statsTop = numberOfHighlightedEdgesbyTs();
      } else if (statName == "average nodes") {
        statsTop = averageNumberOfHighlightedNodesbyEdge();
      }
      barChartTs.statsHigh = statsTop;
    }
  }

  void updateStatisticsSelectedTop(String statName) {
    if(barChartTs!=null) {
      List<num> statsTop = null;
      if(NODE_SELECTED || EDGE_SELECTED || TS_SELECTED ){
        if (statName == "number of nodes") {
          statsTop = numberOfSelectedNodesbyTs();
        } else if (statName == "number of edges") {
          statsTop = numberOfSelectedEdgesbyTs();
        } else if (statName == "average nodes") {
          statsTop = averageNumberOfSelectedNodesbyEdge();
        }
      }
      barChartTs.statsSelect = statsTop;
    }

  }

  void updateStatisticsHighlightedLeft(String statName) {

    graph.computeHighDegree();

    if(barChartNodes!=null) {
      List<num> statsLeft = null;

      if (statName == "number of edges") {
        statsLeft = numberOfHighlightedEdgesbyNode();
      }else if (statName == "number of appereances") {
        statsLeft = numberOfHighlightedTsbyNode();
      }
      barChartNodes.statsHigh = statsLeft;
    }

  }

  void updateStatisticsSelectedLeft(String statName) {

    graph.computeSelectedDegree();

    if(barChartNodes!=null) {
      List<num> statsLeft = null;
      if(NODE_SELECTED || EDGE_SELECTED || TS_SELECTED ){

        if (statName == "number of edges") {
          statsLeft = numberOfSelectedEdgesbyNode();
        }else if (statName == "number of appereances") {
          statsLeft = numberOfSelectedTsbyNode();
        }
      }
      barChartNodes.statsSelect = statsLeft;
    }

  }


  void updateStatisticsLeft(String statName, [bool force=false]){
    if(barChartNodes!=null) {
      List<num> posStLeft = getNodesPos();
      if (posStLeft.length != barChartNodes.pos.length || barChartNodes.statsName != statName || force) {
        List<num> statsLeft = null;

        if (statName == "number of edges") {
          statsLeft = numberOfEdgesbyNode();
        }else if (statName == "number of appereances") {
          statsLeft = numberOfTsbyNode();
        }

        if (statsLeft != null) barChartNodes.updateStatsPos(statName, statsLeft, posStLeft);
      }
      else {
        barChartNodes.updatePos(posStLeft);
      }
    }
  }

  void updateFilteredNodes(){

    FILTERING_TRANSITION = true;

    nodeRenderer.nodes.forEach((n){
      n.toBeValid = true;
    });
    edgeRenderer.updateValid(true);  // should be called when nodes pos changes
    graph.computeDegree(true);


//    List<Nodes> previousValid = nl.nodes.nodes.where((Node n) => n.isValid).toList();
    int beforeUpdate = nodeRenderer.nodes.nodes.where((Node n) => n.isValid).length;

    filters.forEach((s, f){
      nodeRenderer.nodes.preFilterValid(f);
    });

    int afterUpdate = nodeRenderer.nodes.where((var n) => n.toBeValid).length;

    if(afterUpdate > beforeUpdate) {
      nodeRenderer.nodes.resetPositions();
      nodeRenderer.updatePositions();
    }
    edgeRenderer.updateValid();  // should be called when nodes pos changes

    render();

    updateStatisticsLeft(barChartNodes.statsName);
    updateStatisticsTop(barChartTs.statsName);

    nodeRenderer.nodes.resetValid();

    filters.forEach((s, f){
//      print("filtering " + s);
//      print(f);
      nodeRenderer.nodes.filterValid(f);
    });

    graph.computeDegree();

    new Future.delayed(const Duration(milliseconds: 1500), () {
      nodesVisChanged();
      FILTERING_TRANSITION = false;
      updateNodeSpace();

      edgeRenderer.updateValid();  // should be called when nodes pos changes

      document.querySelector('#filters-total').innerHtml =
              nodeRenderer.nodes.valid.length.toString() + ' ' +graph.node_meta + 's visible out of '+  nodeRenderer.nodes.length.toString();

      render();
    });

  }

  void filterSelectedNodes([bool nodeSelected=false]){
    if(nodeSelected)
      filters["select"] = (Node n) => n.isSelected || n.isAdjacentToSelected;
    else
      filters["select"] = (Node n) => true;

    eventBus.fire(new NodeFilterChanged("select"));
  }

  void filterDegree(int minDegree){
      filters["degree"] = (Node n) => n.degree>=minDegree;
      eventBus.fire(new NodeFilterChanged("degree"));
  }

  void setBibList(String authors, String description, String booktitle, String year, String biburl) {
    //setto un div per inserire una linea separatrice e un div per inserire le informazioni relative ad un'opera.

    DivElement divbox= new DivElement();
    divbox.title="Click for more info";
    divbox.id="containerbib";
    DivElement line = new DivElement();
    DivElement nodeinfo = new DivElement();
    line.id = "horizontalline";
    nodeinfo.id = "info";

    //manages clicks over each iperedge to get more info.
    nodeinfo.onClick.listen((MouseEvent e) {
      if (nodeinfo.style.height == "auto") {
        nodeinfo.style.height = "70px";
        // nodeinfo.style.overflow="hidden";
        // nodeinfo.style.whiteSpace="nowrap";
      } else {
        nodeinfo.style.height = "auto";
        // nodeinfo.style.overflow = "visible";
        // nodeinfo.style.whiteSpace="unset";
      }
    });

    //informazioni da mostrare nel div
    String content = "";

    if (authors != "null") {
      authors = authors + ".";
      content = content + "<h6 class='oneline'> $authors</h6>";
    }
    if (description != "null") {
      description = description + ".";
      content = content + "<strong class='oneline'> <br> $description</strong>";
    }
    if (booktitle != "null") {
      booktitle = booktitle + ".";
      content = content + "<h6 class='oneline' style='font-size: 12px'> $booktitle</h6>";
    }
    if (year != "null") {
      year = year + ".";
      content = content + "<h6 class='oneline'> $year</h6>";
    }

    //inserisco le info nel tag di nodeinfo
    nodeinfo.setInnerHtml(content);
    divbox.children.add(nodeinfo);

    //permetto il download del bibtex ove disponibile
    if (biburl != "null") {
      AnchorElement link = new AnchorElement()
        ..title = "Download .bib"
        ..id = "linkdownload"
        ..href = '$biburl';
      ImageElement image = new ImageElement(src: "https://dblp.uni-trier.de/img/download.dark.hollow.16x16.png");

      link.children.add(image);
      divbox.children.add(link);
    }
    divcontainer.children.add(divbox);
    divcontainer.children.add(line);
    _bib.style.visibility = "visible";
  }
}
