
part of paohvis;


/** Manages a single sparkline, that is a set of values in a time frame*/
class Sparkline{
  List timePointsOrig;
  List <double> timepoints;
  String timeFrame;
  ColorCodings _colorMap = ColorCodings.grayscale;
  ImageElement _img;

  ColorCodings get colorMap => _colorMap;
  set colorMap(ColorCodings value){
    _colorMap = value;
  } //metadata

  // golbalMinValue and golbalMaxValue refer to the SparkLines of the node
  double _globalMaxValue = 0.0;
  double _globalMinValue = 0.0;

  /** Max and Min value are two measure that can be:
   * 1) displayed on the screen
   * 2) serve to normalize the visualizaiton
   * but the scope is local to the single sparkline*/

  double _maxValue = 0.0;
  double _minValue = 0.0;

  double get maxValue => _maxValue;
  double get minValue => _minValue;
  void set minValue(double newVal){_minValue = newVal;}
  void set maxValue(double newVal){_maxValue = newVal;}



  double get globalMaxValue => _globalMaxValue;
  set globalMaxValue(double value) => _globalMaxValue = value;

  double get globalMinValue => _globalMinValue;
  set globalMinValue(double value) => _globalMinValue = value;

  bool isMaxValueVisible = MAXVALUEVISIBLE;//if true should display min / max for the series
  num suggestedHeatmapBandHeigth = 6; // remember to set this before visualize the heatmap


  bool recreateHeatmap;

  // TODO: get data from external sources
  Sparkline (List<double> nTimePoints){
    timepoints = nTimePoints;
    recreateHeatmap = true;
  }

  createHeatMapImage(){

    num currentMin = globalMinValue;
    num currentMax = globalMaxValue;
    if(globalMinValue==globalMaxValue){
      currentMin = minValue;
      currentMax = maxValue;
    }

    bool lightColor = true; //to improve foreground readability

    _img = createImage(timepoints, currentMin, currentMax, colorMap, lightColor);

  }
  // renders the sparkline according to a canvas, and given dimensions
  renderSparkline (CanvasRenderingContext2D context, num posX, num posY, num width, num height,
      num nodeHeight, bool highlighted, bool selected, [String backColor=null]){

    if(HEATMAP_IMG && recreateHeatmap && timepoints.length>0){;
      createHeatMapImage();
      recreateHeatmap = false;
    }

    if(backColor == null)
      backColor = Theme.sparklineBackground;

    suggestedHeatmapBandHeigth = nodeHeight; //frame

    /* sparkline border and background */ //TODO dont draw when highlight
    context
      ..strokeStyle = backColor
      ..fillStyle = backColor;

//      if (selected) context.fillStyle = Theme.sparklineSelect;

    String prevStrokeStyle = context.strokeStyle;

    context
      ..beginPath()
      ..rect(posX, posY, width, height)
      ..closePath()
      ..fill()
      ..stroke();
    context.strokeStyle = prevStrokeStyle;

    if(timepoints.length > 0) {

      context
        ..beginPath()
        ..rect(posX, posY, width, height)
        ..closePath()
        ..fill();
      context.strokeStyle = prevStrokeStyle;


      // prepare for rendering time series
      // halfStep: 1/2 distance between a time point and the next one
      // TODO: if there is only one time point this can generate an error
      int nTimePoints = timepoints.length;
      double step = width.toDouble();
//      double halfStep = width.toDouble();
      if (nTimePoints > 1) {
        step = width / (nTimePoints - 1);
      }


      if (HEATMAP && timepoints.length>0){// && timepoints[0]!=0) {// heatmap-like visualization
//        String colorCode="#C0FFEE";
//        context
//          ..beginPath()
//          ..fillStyle = Theme.backgroundTimeSeries
//          ..strokeStyle = Theme.strokeNodes
//          ..lineWidth = 1
//          ..globalAlpha = 1;
//        double lowerbound = ((height / 2) - (suggestedHeatmapBandHeigth / 2));
//        bool lightColor = false; //to improve foreground readability

//        if (BIOFABRIC || CURVES || SPLAT || NODELINK) lightColor = true;
        num margin = height/10;
        context.drawImageScaled(_img, posX, posY+margin, width, height-margin*2);
        context.imageSmoothingEnabled = false;


//        for (int i = 0; i < nTimePoints; i++) {
//          //color
//
//            colorCode = getColorCode(
//              globalMinValue, globalMaxValue,
//              timepoints[i],
//                colorMap,
//              lightColor,
//                120,
//                new HexColor("#eeeeee"),
//                i
//            );
//          context
//            ..fillStyle = colorCode
//            ..strokeStyle = colorCode;
//
//          //draw
//          if(i==0) {
//            context
//              ..fillRect(posX,
//                  posY + lowerbound,
//                  halfStep,
//                  suggestedHeatmapBandHeigth);
////          if (isMaxValueVisible && timepoints[i] == maxValue) {
////            // insert instructions to draw maxvalue here...
////          }
//          }else{
//            if (i==nTimePoints-1){
//              context
//                ..fillRect(posX + (i * step) - halfStep,
//                    posY + lowerbound,
//                    halfStep,
//                    suggestedHeatmapBandHeigth);
//            } else {
//              context
//                ..fillRect(posX + (i * step) - halfStep,
//                    posY + lowerbound,
//                    step,
//                    suggestedHeatmapBandHeigth);
//            }
//          }
//        }
//        context.stroke();
      }

      if (LINEGRAPH) {// line graph visualization
        double weight = (height - 2) / (globalMaxValue - globalMinValue); // weight: the normalized unit to vertically render sparklines.
        Point startingPoint = new Point(
            posX + 0 * step,
            posY + height - (timepoints[0] + globalMinValue.abs()) * weight);
        context
          ..beginPath()
          //..fillStyle = Theme.backgroundTimeSeries
          ..strokeStyle = Theme.histogramBorderLinegraph
          ..lineWidth = 1
          ..globalAlpha = 1
          ..moveTo(startingPoint.x, startingPoint.y)
          ..stroke();


        if (timepoints.length == 1) { //when just one point draws an horizontal line/box

          if(HISTOGRAM_LINEGRAPH) {
          context
            ..fillStyle = Theme.histogramBackgroundLinegraph
            ..fillRect(posX, startingPoint.y, width, (timepoints[0] + globalMinValue.abs()) * weight)
            ..rect(posX, startingPoint.y, width, (timepoints[0] + globalMinValue.abs()) * weight);;
          } else {
            context.lineTo(
                posX + 1 * step,
                posY + (height - (timepoints[0] + globalMinValue.abs()) * weight));
          }

        } else {
          for (int i = 1; i < timepoints.length; i++) {
            double normalizedPosY = height - (timepoints[i] + globalMinValue.abs()) * weight;
            context.lineTo(posX + i * step, posY + normalizedPosY);
            if (isMaxValueVisible && timepoints[i] == maxValue) {
              context.stroke();
              String prevStrokeStyle = context.strokeStyle;
              String prevFillStyle = context.fillStyle;
              context
                ..beginPath()
                ..strokeStyle = Theme.sparklineBackground
                ..fillStyle = Theme.sparklineBackground
                ..arc(posX + i * step, posY + normalizedPosY, 1, 0, PI2, true)
                ..stroke();
              context
                ..beginPath()
                ..strokeStyle = prevStrokeStyle
                ..fillStyle = prevFillStyle
                ..moveTo(posX + i * step, posY + normalizedPosY);
            }
          }
        }
        context.stroke();
      }
    }

//    if (highlighted && !NODELINK) {
//      String prevStrokeStyle = context.strokeStyle;
//
//      context..
//        strokeStyle = "#000000";
//
//      context
//        ..beginPath()
//        ..rect(posX+1, posY, width-1, height-1)
//        ..closePath()
//        ..stroke();
//      context.strokeStyle = prevStrokeStyle;
//    }
  }

  double computeAggregatedValue(){
    double aggregatedValue = 0.0;
    timepoints.forEach((tp){
      aggregatedValue += tp;
    });
    return aggregatedValue/timepoints.length;
  }


}