part of paohvis;

class TabCheckbox {

  CanvasElement _canvas;
  CanvasRenderingContext2D _context;

  TimeManager tm = new TimeManager();
  num _topMargin = 0;
  num _bottomMargin = 5;
  num _leftMargin = 0;
  num _fontSize = MAX_FONT;
  num _height = 0;


  TabCheckbox (CanvasElement canvasElement, [num pos = 0, num tm = 0, num lm = 0]) {
    _canvas = canvasElement;
    _context = _canvas.getContext('2d');
    _topMargin = tm;
    _leftMargin = pos + lm;
    _height = _fontSize + _bottomMargin;
  }

  // renders checkbox
  render([bool checked = false]){
    //checked layout
    if(checked) {
      _context
        ..beginPath()
        ..rect(_leftMargin, _topMargin + (_height / 2) - _bottomMargin, (_height / 2), (_height / 2))
        ..fillStyle = "#999999"
        ..strokeStyle = "#999999"
        ..closePath()
        ..fill()
        ..stroke();
      //show checkbox icon inside
      _context
        ..font = "bold 12px " + CANVAS_FONT
        ..setFillColorRgb(256, 256, 256)
        ..fillText("\u2713", _leftMargin + 3, _topMargin - 2 + _fontSize - _bottomMargin,
            _leftMargin);
    }else{
      //not checked layout
      _context
        ..beginPath()
        ..rect(_leftMargin, _topMargin + (_height / 2) - _bottomMargin,
            (_height / 2), (_height / 2))
        ..lineWidth = 1.5
        ..fillStyle = "#ffffff"
        ..strokeStyle = "#999999"
        ..closePath()
        ..fill()
        ..stroke();
    }
  }
}
