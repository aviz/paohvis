part of paohvis;

//draft class that should map the marie boucher dataset schema, it could be removed
class EdgeMB {
  int index;
  String nom1;
  String lieu;
  String qualification;
  String nom2;
  String lieux;
  String autresLieux;
  String eduteNotaire;
  String coteADLA;
  String natureActe;
  String date;
  bool check;
  int hyper;
}

TimeSlotScroll scTimeSlots;
VertScroll scVertex;

// main class of the project
class Paohvis {
  Graph mainGraph = null;
  VisManager vm = null;
  TimeManager tm = null;
  Writer writer = null;
  Reader reader = null;

  num shiftX = 0;
  num shiftY = 0;

  bool _toolbarVisible = true;

  var onWindowResizeEvent, onToogleVisible;

  Paohvis() {
    /// connecting the list of sort buttons to code
//    for (int i = 0; i < SortingNodes.values.length; i++){
//      orderingList.add(new Button(document.querySelector(reordering[i]), false));
//    }
    /// connecting the list of style buttons to code
    for (int i = 0; i < LineStyle.values.length; i++) {
      lineStylingList
          .add(new Button(document.querySelector(linestyle[i]), false));
    }

    setupEvents();

    mainGraph = new Graph();
    tm = new TimeManager();
    writer = new Writer(mainGraph);
    reader = new Reader(mainGraph, tm);

    scTimeSlots = new TimeSlotScroll(horScrollDiv, horScrollCanvas);
    scVertex = new VertScroll(vertScrollDiv, vertScrollCanvas);
  }

  /**
   * Specifies the path where the server is located
   * */
  void setBasePath(String bp) {
    _serverBasePath = bp;
  }

  String get basePath => _serverBasePath;

  void setDataPath(String bp) {
    _dataBasePath = bp;
  }

  /**
   * Loads data in main memory
   */
  loadDBList(String jsonString) async {
    try {
      var data = await HttpRequest.getString(jsonString);
      _getDataFileList(data);
    } catch (e, s) {
      handleError(e, s);
    }
  }

  /**
   * Retrieves the list of available DB (in json format)
   */
  void _getDataFileList(String jsonString) {
    Map fileData = json.decode(jsonString); // gets the JSON file
    listDBFiles = fileData['files'];

    for (num i = 0; i < listDBFiles.length; i++) {
      if (i >= _selectDBFile.options.length)
        _selectDBFile.append(new OptionElement());
      _selectDBFile.options[i].innerHtml = listDBFiles[i]['name'];
      _selectDBFile.options[i].title = listDBFiles[i]['description'];
    }

    _opDBList = _selectDBFile.options;

    /// onChange management
    _selectDBFile.onChange.listen((e) {
      selectedDB = listDBFiles[_selectDBFile.selectedIndex]['name'];
      _divDsDescription.innerHtml =
          listDBFiles[_selectDBFile.selectedIndex]['description'];
      changeDB(_dataBasePath + "/" + selectedDB);
      _selectDBFile.blur();
    });

//    currentDB = "eeg.json"; // not needed anymore

    // sets current DB file as "selected" inside the related list
    int lengthOptions = _selectDBFile.options.length;
    for (num i = 0; i < lengthOptions; i++) {
      if (_selectDBFile.options[i].text.contains(currentDB.toString()))
        _selectDBFile.options[i].selected = true;
    }
    _selectDBFile.selectedIndex = 0;
    selectedDB = listDBFiles[_selectDBFile.selectedIndex]['name'];

    _divDsDescription.innerHtml =
        listDBFiles[_selectDBFile.selectedIndex]['description'];

    changeDB(_dataBasePath + "/" + selectedDB);
//    loadFileDB(_serverBasePath + "/" + selectedDB);
  }

  changeDB(selectedDB) async {
    try {
      var data = await HttpRequest.getString(selectedDB);
      loadFileDB(data, selectedDB);
      currentDB = selectedDB;
      savePreferences();
    } catch (e, s) {
      handleError(e, s);
    }
  }

  setupEvents() {
    _visDiv.onDrop.listen(dropHandler);
    _visDiv.onDragOver.listen(dragOverHandler);

    onWindowResizeEvent = window.onResize.listen((ev) {
      if (vm != null) vm.updateVisDimensions();
    });

    onToogleVisible = _btnToolbarVisible.onClick.listen((MouseEvent event) {
      if (_toolbarVisible) {
        _secondaryRows.style.display = "none";
        _iconToogleVisibility.classes.add('toolbar-hidden');
        _iconToogleVisibility.classes.remove('toolbar-visible');
        _sideNavContent.classes.add('sideNav-toolbar-hidden');
        _sideNavContent.classes.remove('sideNav-toolbar-visible');

        _visDiv.style.top = "55px";
        vertScrollDiv.style.top = "55px";
        if (vm != null) vm.updateVisDimensions();

        _toolbarVisible = false;
      } else {
        _visDiv.style.top = "160px";
        vertScrollDiv.style.top = "160px";
        _secondaryRows.style.display = "flex";

        _iconToogleVisibility.classes.add('toolbar-visible');
        _iconToogleVisibility.classes.remove('toolbar-hidden');
        _sideNavContent.classes.remove('sideNav-toolbar-hidden');
        _sideNavContent.classes.add('sideNav-toolbar-visible');

        if (vm != null) vm.updateVisDimensions();

        _toolbarVisible = true;
      }
    });

//    _visDiv.onDragStart.listen(_onDragStart);
//    _visDiv.onDragEnd.listen(_onDragEnd);
//    _visDiv.onDragEnter.listen(_onDragEnter);
//    _visDiv.onDragOver.listen(_onDragOver);
//    _visDiv.onDragLeave.listen(_onDragLeave);
//    _visDiv.onDrop.listen(dropHandler);
  }

  dropHandler(ev) {
    ev.stopPropagation();

    // Prevent default behavior (Prevent file from being opened)
    ev.preventDefault();

    var files = ev.dataTransfer.files;
    if (files.length > 0) {
      var file = files[0];

      FileReader fr = new FileReader();
      fr.onLoad.listen((ProgressEvent fileEvent) {
        String fileContent = fr.result;

        loadFileDB(fileContent, file.name);
        _divDsDescription.innerHtml = file.name;
//          _selectDBFile.options.add(new OptionElement(_inputFileOpen.files[0].name, _inputFileOpen.files[0].name, true));
      });

      fr.onError.listen((itWentWrongEvent) {});

      fr.readAsText(file);
    }
    // Pass event to removeDragData for cleanup
    removeDragData(ev);
  }

  dragOverHandler(ev) {
    // Prevent default behavior (Prevent file from being opened)
    ev.preventDefault();
  }

  removeDragData(ev) {
    if (ev.dataTransfer.items) {
      // Use DataTransferItemList interface to remove the drag data
      ev.dataTransfer.items.clear();
    } else {
      // Use DataTransfer interface to remove the drag data
      ev.dataTransfer.clearData();
    }
  }

  ///
  void setSelectDBFile(SelectElement sdb) {
    _selectDBFile = sdb;
  }

  SelectElement get selectDBFile => _selectDBFile;

  /**
   * Loads data in main memory
   */
  loadFileDB(String data, String fileName) {
    // TODO: reset key event

    if (vm != null) vm.reset();
    //vm = null;

    String extension = fileName.split(".").last;
    switch (extension) {
      case "pao":
      case "json":
        reader._getDataFromJsonFile(
            data); // calls _getDataFromFile if the request was successful
        break;
      case "bib":
        {
//          print("ho preso bib");
          //  reader._getfrombib(data);
          reader._getDataFromBibFile(
              data); //calls _getDataFromBibFile if the request was successful
          break;
        }
      case "csv":
        reader._getDataFromCsvFile(
            data); // calls _getDataFromFile if the request was successful
        break;
    }

    vm = new VisManager(mainGraph, canvas, canvasOverlay);

    resetSelectStatusAll();

    // reset state with previous interactions, but not all of them! check this!!!
    vm.setupInteraction();
    vm.filterDegree(FILTER_DEGREE);
    sortBy(getSelectedOrder());

    resetZoom();

    eventBus.fire(new TimeSliderChanged('0'));
    _timeSlider.value = "0";
    eventBus.fire(new TimeSlotsChanged());

    replaceMeta();
  }

  void resetHighlightStatusAll() {
    if (ENABLED_HIGHLIGHT) {
      vm.resetHighlightStatusAll();
      vm.render();
    }
  }

  void resetSelectStatusAll() {
    SHOW_CONTEXT = true;

    _searchName.value = "";

    if (ENABLED_SELECTION || ENABLED_FILTER) {
      vm.resetNodeFilterStatus();
      vm.resetTimeSlotSelectedStatus();

      vm.resetEdgeSelectedStatus();
      vm.resetNodeSelectedStatus();
      vm.resetNodeFilterStatus();

//      vm.resetEdgeHighlightedStatus();
//      vm.resetNodeHighlightedStatus();
      vm.updateStatisticsLeft(vm.barChartNodes.statsName, true);
      vm.updateStatisticsTop(vm.barChartTs.statsName, true);

      vm.render();
    }
  }

  void createFile(_anchorSave, fileName) {
    writer.createFile(_anchorSave, fileName);
  }

  void updateVis() {
    checkUIIntegrity();

    vm.setup();
    vm.render();
    vm.updateTsSpace();
    vm.updateNodeSpace();
    vm.updateStatsMenuDimensions();
    replaceMeta();
  }

  void scaleTimeSlotWidth(num scale) {
    checkUIIntegrity();
    vm.scaleTimeSlot(scale);

    vm.edgeRenderer.computeDimensions();
    vm.updateTSVisibility();
    vm.updateTsSpace();

    vm.render();
  }

  void resetMenuVis() {
    PAOVIS = false;
    SPLAT = false;
    CURVES = false;
    NODELINK = false;
    SLIDER_NODELINK_MOVED = false;
  }

  /** adds interactivity */
  void setupVis() {
    //TODO: use a single function instead of repeating boring instructions

    /** button click management */
    _btnCurves.onClick.listen((MouseEvent e) {
      resetMenuVis();
      CURVES = true;
      //CURVES = !CURVES;
      updateVis();
    });

    _btnEdgeSplat.onClick.listen((MouseEvent e) {
      resetMenuVis();
      SPLAT = true;
      //SPLAT = !SPLAT;
      updateVis();
    });

    _btnBioFabric.onClick.listen((MouseEvent e) {
      resetMenuVis();
      PAOVIS = true;
      //BIOFABRIC = !BIOFABRIC;
      updateVis();
    });

    _btnNodeLink.onClick.listen((MouseEvent e) {
      resetMenuVis();
      NODELINK = true;
      //NODELINK = !NODELINK;
      updateVis();
    });

    _btnShowHyper.onClick.listen((MouseEvent e) {
      SHOW_HYPERGRAPH = !SHOW_HYPERGRAPH;
      updateVis();
    });

    _btnHeatMap.onClick.listen((MouseEvent e) {
      HEATMAP = !HEATMAP;
      updateVis();
      vm.clm.render(vm.graph.communities);
      replaceMeta();
    });

    _btnLineGraph.onClick.listen((MouseEvent e) {
      LINEGRAPH = !LINEGRAPH;
      updateVis();
    });

    _btnInterleaving.onClick.listen((MouseEvent e) {
      SPLAT_INTERLEAVING = !SPLAT_INTERLEAVING;
      updateVis();
    });

    _btnNodeLabels.onClick.listen((MouseEvent e) {
      NODE_LABELS = !NODE_LABELS;
      updateVis();
    });

    /// interaction buttons
    _btnHighlight.onClick.listen((MouseEvent e) {
      resetHighlightStatusAll();
      ENABLED_HIGHLIGHT = !ENABLED_HIGHLIGHT;
      checkUIIntegrity();
    });

    _btnSelection.onClick.listen((MouseEvent e) {
      resetSelectStatusAll();
      ENABLED_SELECTION = !ENABLED_SELECTION;
      if (!ENABLED_SELECTION) ENABLED_FILTER = false;
      checkUIIntegrity();
    });

    _btnFilter.onClick.listen((MouseEvent e) {
//      resetSelectStatusAll();
//      ENABLED_FILTER = !ENABLED_FILTER;
//      if(ENABLED_FILTER)
//        ENABLED_SELECTION = true;

      vm.filterSelectedNodes(NODE_SELECTED);
      vm.render();
      checkUIIntegrity();
    });

    _btnFilterRemove.onClick.listen((MouseEvent e) {
      SHOW_CONTEXT = !SHOW_CONTEXT;

      vm.edgeRenderer.updateValid();
      vm.graph.computeDegree();
      updateVis();

      vm.render();

//      vm.render();
      checkUIIntegrity();
    });

    _btnReset.onClick.listen((MouseEvent e) {
      resetSelectStatusAll();

      checkUIIntegrity();
    });

    _filterDegree.onChange.listen((Event e) {
      FILTER_DEGREE = int.parse(_filterDegree.value);
      vm.filterDegree(FILTER_DEGREE);
      vm.filterDegree(FILTER_DEGREE);
      vm.render();
    });

    _searchName.onInput.listen((Event e) {
      String text = _searchName.value;
//      vm.resetHighlightStatusAll();

      SHOW_CONTEXT = true;

      if (ENABLED_SELECTION || ENABLED_FILTER) {
        vm.resetTimeSlotSelectedStatus();
        vm.resetEdgeSelectedStatus();
        vm.resetNodeSelectedStatus();
        vm.resetNodeFilterStatus();
      }

      vm.searchByName(text);
      vm.render();
      checkUIIntegrity();
    });

    _btnSearchName.onClick.listen((Event e) {
      String text = _searchName.value;
      vm.resetHighlightStatusAll();
      vm.searchByName(text);
      vm.render();
    });

    _btnBindNL.onClick.listen((MouseEvent e) {
      ENABLED_BIND_NODELINK = !ENABLED_BIND_NODELINK;
      checkUIIntegrity();
      updateVis();
    });

    _btnTooltip.onClick.listen((MouseEvent e) {
      ENABLED_TOOLTIP = !ENABLED_TOOLTIP;
//      GHOST_TOOLTIPS = ENABLED_TOOLTIP;
      checkUIIntegrity();
      if (!ENABLED_TOOLTIP) {
        vm.setTooltipVisible(false);
        //_ttip.className = "width_zero";
      } else {
        vm.setTooltipVisible(true);
        //_ttip.className = "width_twenty";
      }
      vm.updateVisDimensions();
      //updateVis();
    });

    _btnRole.onClick.listen((MouseEvent e) {
      ENABLED_NODEROLE = !ENABLED_NODEROLE;
      checkUIIntegrity();

      updateVis();
    });

    _btnColorGroup.onClick.listen((MouseEvent e) {
      ENABLED_COLORGROUP = !ENABLED_COLORGROUP;
      vm.clm.render(vm.graph.communities);

      updateVis();
    });

    _btnEdgePacking.onClick.listen((MouseEvent e) {
      HYPENET_PACK_EDGES = !HYPENET_PACK_EDGES;
      updateVis();
    });

    _btnRepeatArch.onClick.listen((MouseEvent e) {
      REPEAT = !REPEAT;
      updateVis();
    });

    _btnAlternateColors.onClick.listen((MouseEvent e) {
      ALTERNATE_COLORS = !ALTERNATE_COLORS;
      updateVis();
    });

//    _btnSortEdges.onClick.listen((MouseEvent e) {
//      HYPENET_SORT_LENGTH = !HYPENET_SORT_LENGTH;
//      updateVis();
//    });

    _selectEdgesOrder.onChange.listen((Event e) {
      String edgeOrder = getEdgeOrder();
      if (edgeOrder == "length") {
        HYPENET_SORT_LENGTH = true;
      } else if (edgeOrder == "appearanceEdge") {
        HYPENET_SORT_LENGTH = false;
      }

      checkUIIntegrity();

      vm.nodesVisChanged();
      vm.render();

      _selectEdgesOrder.blur();

//      replaceMeta();
    });

    _saveCanvas.onClick.listen((MouseEvent e) {
      String fileName = 'canvas.png';
      vm.saveCanvas(_saveCanvas, fileName);
    });

    _anchorSave.onClick.listen((MouseEvent e) {
      String fileName = 'test.json';
      //TODO: allow to change file name
      createFile(_anchorSave, fileName);
    });

    _inputFileOpen.onChange.listen((Event e) {
      if (_inputFileOpen.files.length > 0) {
        FileReader fr = new FileReader();
        fr.onLoad.listen((ProgressEvent fileEvent) {
          String fileContent = fr.result;

          loadFileDB(fileContent, _inputFileOpen.files[0].name);
          _divDsDescription.innerHtml = _inputFileOpen.files[0].name;
//          _selectDBFile.options.add(new OptionElement(_inputFileOpen.files[0].name, _inputFileOpen.files[0].name, true));
        });

        fr.onError.listen((itWentWrongEvent) {});

        fr.readAsText(_inputFileOpen.files[0]);
      }
    });

    _rngIntensity.onMouseMove.listen((MouseEvent e) {
      if (INTENSITY_CHANGING) {
        updateIntensity();
      }
    });
    _rngIntensity.onMouseUp.listen((MouseEvent e) {
      INTENSITY_CHANGING = false;
      updateIntensity();
    });
    _rngIntensity.onMouseLeave.listen((MouseEvent e) {
      INTENSITY_CHANGING = false;
    });
    _rngIntensity.onMouseDown.listen((MouseEvent e) {
      INTENSITY_CHANGING = true;
    });

    _tsWidthSlider.onMouseMove.listen((MouseEvent e) {
      if (RANGE_NODELINK_CHANGING) {
        SLIDER_NODELINK_MOVED = true;
        updateTsWidth();
      }
    });
    _tsWidthSlider.onMouseUp.listen((MouseEvent e) {
      RANGE_NODELINK_CHANGING = false;
      updateTsWidth();
    });
    _tsWidthSlider.onMouseLeave.listen((MouseEvent e) {
      RANGE_NODELINK_CHANGING = false;
    });
    _tsWidthSlider.onMouseDown.listen((MouseEvent e) {
      RANGE_NODELINK_CHANGING = true;
    });

    _decreaseTsWidth.onClick.listen((MouseEvent e) {
      decreaseTsWidth();
    });

    _increaseTsWidth.onClick.listen((MouseEvent e) {
      increaseTsWidth();
    });

//    _sliderZoom.onMouseMove.listen((MouseEvent e) {
//      updateZoom();
//    });
    _sliderZoomNode.onInput.listen((Event e) {
      updateZoomNode();
    });

    _decreaseZoomNode.onClick.listen((MouseEvent e) {
      decreaseZoomNode();
    });

    _increaseZoomNode.onClick.listen((MouseEvent e) {
      increaseZoomNode();
    });

//   _sliderZoom.onInput.listen((Event e) {
//      updateZoom();
//    });
//
//    _decreaseZoom.onClick.listen((MouseEvent e) {
//      decreaseZoom();
//    });
//
//    _increaseZoom.onClick.listen((MouseEvent e) {
//      increaseZoom();
//    });
//
    _fitZoomTs.onClick.listen((MouseEvent e) {
      fitZoomTsWidth();
    });

    _fitZoomNode.onClick.listen((MouseEvent e) {
      fitZoomNode();
    });

    _sideNavButton.onClick.listen((MouseEvent e) {
        openSideNav();
    });

    _sideNavClose.onClick.listen((MouseEvent e) {
      closeSideNav();
    });
//    context["slide_zoom"] = (JsObject object){
//      var value = object["input"];
//      SCALE = 2.0 + value;
//      updateVis();
//    };

//    orderingList[4].status = true;

    /// sorting and style buttons
//    for (int i = 0; i < SortingNodes.values.length; i++) {
//      orderingList[i].element.onClick.listen((MouseEvent e) {
//        // only one button must be pressed
//        int buttonPressedIndex = -1;
//        for (int j = 0; j < orderingList.length; j++) {
//          if (j == i) {
//            // it is possible to have data in random order,
//            // all buttons can be in the status 'released'
//            if (orderingList[j].status) {
//              orderingList[j].status = false;
//              // add here behavior for buttonRelease
//            } else {
//              orderingList[j].status = true;
//              buttonPressedIndex = j;
//            }
//          } else {
//            orderingList[j].status = false;
//          }
//        }
//        // Checks the button pressed and calls the sort function
//        if (buttonPressedIndex >= 0) {
//          sortBy(buttonPressedIndex);
//        }
//        vm.render();
//        checkUIIntegrity();
//      });
//    }

    _selectOrder.onChange.listen((Event e) {
      int orderIndex = getSelectedOrder();
      if (orderIndex >= 0) {
        sortBy(orderIndex);
      }
      checkUIIntegrity();
      _selectOrder.blur();
    });

    _selectSymbol.onChange.listen((Event e) {
      SYMBOL_SELECTED = getSelectedSymbol();
      updateVis();
    });

    // at least one stlyle must be selected
    lineStylingList[LineStyle.solid.index].status = true;

    for (int i = 0; i < LineStyle.values.length; i++) {
      lineStylingList[i].element.onClick.listen((MouseEvent e) {
        lineStylingList[i].status = !lineStylingList[i].status;
        // only one button must be pressed
        for (int j = 0; j < lineStylingList.length; j++) {
          if (j == i) {
            lineStylingList[j].status = true;
          } else {
            lineStylingList[j].status = false;
          }
        }
        vm.render();
        checkUIIntegrity();
      });
    }

    _btnNodeLabels.onClick.listen((MouseEvent e) {
      NODE_LABELS = !NODE_LABELS;
      updateVis();
    });

    _checkHyperedgesStronger.onChange.listen((Event e) {
      HYPER_EDGES_STRONGER = _checkHyperedgesStronger.checked;
      vm.render();
    });

    _checkNodeColorAsEdge.onChange.listen((Event e) {
      NODE_COLOR_AS_EDGE = _checkNodeColorAsEdge.checked;
      vm.render();
    });

    _checkHyperedgesSplat.onChange.listen((Event e) {
      HYPER_EDGES_SPLAT = _checkHyperedgesSplat.checked;
      vm.render();
    });

    _checkShowDegree.onChange.listen((Event e) {
      SHOW_NODE_DEGREE = _checkShowDegree.checked;
      vm.render();
    });

    _checkAndSelection.onChange.listen((Event e) {
      AND_SELECTIONS_FILTERS = _checkAndSelection.checked;
    });

    _checkShowHighlightSelect.onChange.listen((Event e) {
      ENABLED_HIGHLIGHT_AND_SELECTIONS = _checkShowHighlightSelect.checked;
      checkUIIntegrity();
    });

    _checkHideNotImportant.onChange.listen((Event e) {
      ENABLED_NOT_IMPORTANT_BUTTONS = _checkHideNotImportant.checked;
      checkUIIntegrity();
    });

    document.onKeyDown.listen((KeyboardEvent ke) {
      if ((ke.ctrlKey || ke.metaKey)) {
        if (ke.keyCode == KeyCode.F) {
          ke.preventDefault();
          _searchName.focus();
        } else if (ke.keyCode == KeyCode.EQUALS ||
            ke.keyCode == KeyCode.NUM_PLUS) {
          ke.preventDefault();
          increaseZoomNode();
          increaseTsWidth();
        } else if (ke.keyCode == KeyCode.DASH ||
            ke.keyCode == KeyCode.NUM_MINUS) {
          ke.preventDefault();
          decreaseTsWidth();
        } else if (ke.keyCode == KeyCode.ZERO) {
          ke.preventDefault();
          resetZoom();
        }
      }
    });

    document.onClick.listen((MouseEvent e) {
      checkUIIntegrity();
    });

    // at the end of setup UI must updated
    checkUIIntegrity();
  }

  replaceMetaList(String before, String after, List<Element> toChange) {
    String capitalize(String s) =>
        s != '' ? s[0].toUpperCase() + s.substring(1) : '';
    String transform(String s) => '{' + s + '}';

    String after_u = capitalize(after);
    String before_u = capitalize(before);

    before = transform(before);
    before_u = transform(before_u);

    toChange.forEach((Element e) {
      e.innerHtml = e.innerHtml.replaceAll(before, after);
      e.innerHtml = e.innerHtml.replaceAll(before_u, after_u);

      e.title = e.title.replaceAll(before, after);
      e.title = e.title.replaceAll(before_u, after_u);

      if (e is InputElement) {
        e.placeholder = e.placeholder.replaceAll(before, after);
        e.placeholder = e.placeholder.replaceAll(before_u, after_u);
      }
//    e.title = 'test';
      if (DEBUG) print(e.title);
    });
  }

  replaceMeta() {
    if (DEBUG) print(mainGraph.hyperedge_meta);

    String hyperedge_name = 'hyperedge';
    List<Element> listToChange = document.querySelectorAll(".hyperedge-change");
    replaceMetaList(hyperedge_name, mainGraph.hyperedge_meta, listToChange);

    String node_name = 'node';
    List<Element> listToChangeNode = document.querySelectorAll(".node-change");
    replaceMetaList(node_name, mainGraph.node_meta, listToChangeNode);

    String group_name = 'group';
    List<Element> listToChangeGroup =
        document.querySelectorAll(".group-change");
    replaceMetaList(group_name, mainGraph.group_meta, listToChangeGroup);

    String ts_name = 'time slot';
    List<Element> listToChangeTs = document.querySelectorAll(".ts-change");
    replaceMetaList(ts_name, mainGraph.ts_meta, listToChangeTs);
  }

  String getEdgeOrder() {
    return _selectEdgesOrder.value;
  }

  /// check which button is pressed
  int getSelectedOrder() {
    var orderIndex = -1;
    var orderingValues = {
      'id': 0,
      'alpha': 1,
      'degree': 2,
      'leaf': 3,
      'barycentric': 4,
      'spectral': 5,
      'appearance': 6,
      'group': 7,
      'rcm': 8
    };
    if (orderingValues[_selectOrder.value] >= 0){
        orderIndex = orderingValues[_selectOrder.value];
    }
    return orderIndex;
  }


  /// check which symbol is selected
  int getSelectedSymbol() {
    var symbolIndex = -1;
    var symbolValues = {
      'star': 0,
      'rectangle': 1,
      'circle': 2,
      'cross': 3,
      'triangle': 4,
      'reverseTriangle': 5,
      'diamond': 6,
      'diamondSquare': 7
    };
    if (symbolValues[_selectSymbol.value] >= 0){
      symbolIndex = symbolValues[_selectSymbol.value];
    }
    return symbolIndex;
  }

  void openSideNav(){
    _sideNav.style.right = '0px' ;
  }

  void closeSideNav(){
    _sideNav.style.right = '-25%' ;
  }



  //TODO: desc mode => buttons instead just the index and the status of them
  /// Sort nodes according to the pressed button
  void sortBy(int buttonPressedIndex) {
    if (buttonPressedIndex == SortingNodes.nodeId.index) {
      if (DEBUG) print("sorting default (ids)");
      vm.sortNodesBy(SortingNodes.nodeId);
    }
    if (buttonPressedIndex == SortingNodes.alpha.index) {
      if (DEBUG) print("sorting alphabetically");
      vm.sortNodesBy(SortingNodes.alpha);
    }
    if (buttonPressedIndex == SortingNodes.degree.index) {
      if (DEBUG) print("sorting by degrees");
      vm.sortNodesBy(SortingNodes.degree, false);
    }
    if (buttonPressedIndex == SortingNodes.leafOrder.index) {
      if (DEBUG) print("sorting by leaf order");
      vm.sortNodesBy(SortingNodes.leafOrder);
    }
    if (buttonPressedIndex == SortingNodes.rcm.index) {
      if (DEBUG) print("sorting by rcm");
      vm.sortNodesBy(SortingNodes.rcm);
    }
    if (buttonPressedIndex == SortingNodes.barycentric.index) {
      if (DEBUG) print("sorting by barycentric distance");
      vm.sortNodesBy(SortingNodes.barycentric);
    }
    if (buttonPressedIndex == SortingNodes.spectralOrder.index) {
      if (DEBUG) print("sorting by spectral order");
      vm.sortNodesBy(SortingNodes.spectralOrder);
    }
    if (buttonPressedIndex == SortingNodes.timeslot.index) {
      if (DEBUG) print("sorting by appearance (ts)");
      vm.sortNodesBy(SortingNodes.timeslot, false);
    }
    if (buttonPressedIndex == SortingNodes.community.index) {
      if (DEBUG) print("sorting by appereance + community");
      vm.sortNodesBy(SortingNodes.community, true);
    }
  }

  updateIntensity() {
    BACKGROUND_INTENSITY = 1 - double.parse(_rngIntensity.value) / 200.0;
    updateVis();
  }

//  fitZoom(){
//    num sc = vm.getFitNodesScale();
//    SCALE = min(max(sc, double.parse(_sliderZoom.min)), double.parse(_sliderZoom.max));
//    _sliderZoom.value = SCALE.toString();
//
//    updateVis();
//
//    num ts_sc = vm.getFitTsScale();
//    TS_WIDTH = min(max(ts_sc, double.parse(_tsWidthSlider.min)), double.parse(_tsWidthSlider.max));
//    _tsWidthSlider.value = TS_WIDTH.toString();
//    scaleTimeSlotWidth(TS_WIDTH);
//
//    updateVis();
//
//  }

  fitZoomNode() {
    num nh = vm.getFitNodesScale();

    NODE_HEIGHT_DIM = min(max(nh, double.parse(_sliderZoomNode.min)),
        double.parse(_sliderZoomNode.max));
    _sliderZoomNode.value = NODE_HEIGHT_DIM.toString();

    updateVis();
  }

  fitZoomTsWidth() {
    num ts_sc = vm.getFitTsScale();
    TS_WIDTH = min(max(ts_sc, double.parse(_tsWidthSlider.min)),
        double.parse(_tsWidthSlider.max));
    _tsWidthSlider.value = TS_WIDTH.toString();
    scaleTimeSlotWidth(TS_WIDTH);

    updateVis();
  }

  resetZoom() {
//    _sliderZoom.value = "2.0";
//    updateZoom();
    _sliderZoomNode.value = "7.0";
    _tsWidthSlider.value = "1.5";
    updateZoomNode();
    updateTsWidth();
  }

//
//  decreaseZoom(){
//    _sliderZoom.stepDown(1);
//    updateZoom();
//  }
//
//  increaseZoom(){
//    _sliderZoom.stepUp(1);
//    updateZoom();
//  }
//
//  updateZoom() {
//    SCALE = double.parse(_sliderZoom.value);
//    updateVis();
//  }

  decreaseZoomNode() {
    _sliderZoomNode.stepDown(1);
    updateZoomNode();
  }

  increaseZoomNode() {
    _sliderZoomNode.stepUp(1);
    updateZoomNode();
  }

  updateZoomNode() {
    NODE_HEIGHT_DIM = double.parse(_sliderZoomNode.value);
    updateVis();
  }

  decreaseTsWidth() {
    _tsWidthSlider.stepDown(1);
    updateTsWidth();
  }

  increaseTsWidth() {
    _tsWidthSlider.stepUp(1);
    updateTsWidth();
  }

  updateTsWidth() {
    TS_WIDTH = double.parse(_tsWidthSlider.value);
    num scaleTs = TS_WIDTH;
    scaleTimeSlotWidth(scaleTs);
  }

  /**
   * Method that should manage errors
   */
  void handleError(var error, var s) {
    print('Error in paoh_tool.dart ...');
    print(error);
    if (s != null) print('Stack trace:\n $s');
  }

  /**********************************************
   *            Theme management
   **********************************************/
  /// listbox management
  void setSelectTheme(SelectElement se) {
    _selectTheme = se;
  }

  SelectElement get selectTheme => _selectTheme;

// loading the themes listbox
  loadThemeName(String path) async {
    try {
      var data = await HttpRequest.getString(path);
      _getThemeName(data);
    } catch (e, s) {
      handleError(e, s);
    }
  }

  void _getThemeName(String jsonS) {
    Map themeName = json.decode(jsonS);
    listTheme = themeName['themes'];

    for (int i = 0; i < listTheme.length; i++) {
      if (i >= _selectTheme.options.length)
        _selectTheme.append(new OptionElement());
      _selectTheme.options[i].innerHtml = listTheme[i]['themename'];
    }
    _opList = _selectTheme.options;

    _selectTheme.onChange.listen((e) {
      Theme.resetTheme;
      selectedTheme = listTheme[_selectTheme.selectedIndex]['themename'];
      if (DEBUG) print("theme: " + selectedTheme);
      loadTheme(selectedTheme);
      currentTheme = selectedTheme;
      savePreferences();
      updateVis();
      _selectTheme.blur();
    });

    if (storedOK) {
      loadTheme(currentTheme);
      storedOK = false;
    } else
      currentTheme = "day";

    // sets current theme as "selected" inside the related list
    int lengthOptions = _selectTheme.options.length;
    for (int i = 0; i < lengthOptions; i++) {
      if (_selectTheme.options[i].text.contains(currentTheme.toString()))
        _selectTheme.options[i].selected = true;
    }
  }

  void loadTheme(String choiche) {
    for (int i = 0; i < listTheme.length; i++) {
      var theme = listTheme[i];
      if (theme['themename'] == choiche) {
        //PAOVIS - Color of vertices and edges
        Theme.setVerticesPaovisDefault(theme['verticesPaovisDefault']);
        Theme.setVerticesPaovisHighlight(theme['verticesPaovisHighlight']);
        Theme.setVerticesPaovisNotHighlight(
            theme['verticesPaovisNotHighlight']);
        Theme.setVerticesPaovisSelect(theme['verticesPaovisSelect']);
        Theme.setVerticesSplatSelect(theme['verticesSplatSelect']);
        Theme.setVerticesSplatHighlight(theme['verticesSplatHighlight']);
        Theme.setAdjacentVerticesPaovisSelect(
            theme['adjacentVerticesPaovisSelect']);
        Theme.setAdjacentVerticesSplatSelect(
            theme['adjacentVerticesSplatSelect']);
        Theme.setAdjacentVerticesPaovisHighlight(
            theme['adjacentVerticesPaovisHighlight']);
        Theme.setAdjacentVerticesSplatHighlight(
            theme['adjacentVerticesSplatHighlight']);

        Theme.setEdgesPaovisDefault(theme['edgesPaovisDefault']);
        Theme.setEdgesPaovisHighlight(theme['edgesPaovisHighlight']);
        Theme.setEdgesPaovisNotHighlight(theme['edgesPaovisNotHighlight']);
        Theme.setEdgesPaovisSelect(theme['edgesPaovisSelect']);

        //CURVES - Color of edges
        Theme.setEdgesCurvesDefault(theme['edgesCurvesDefault']);
        Theme.setEdgesCurvesHighlight(theme['edgesCurvesHighlight']);
        Theme.setEdgesCurvesNotHighlight(theme['edgesCurvesNotHighlight']);
        Theme.setEdgesCurvesSelect(theme['edgesCurvesSelect']);

        //SPLAT - Color of Vertices and Edges
        Theme.setVerticesSplatDefault(theme['verticesSplatDefault']);

        Theme.setEdgesSplatDefault(theme['edgesSplatDefault']);
        Theme.setEdgesSplatCommunity(theme['edgesSplatCommunity']);
        Theme.setEdgesSplatHighlight(theme['edgesSplatHighlight']);
        Theme.setEdgesSplatNotHighlight(theme['edgesSplatNotHighlight']);
        Theme.setEdgesSplatSelect(theme['edgesSplatSelect']);

        //GENERAL - Color of others canvas element
        //Canvas
        Theme.setCanvasBackground(theme['canvasBackground']);
        Theme.setCanvasBorderBackground(theme['canvasBorderBackground']);

        //Vertices content
        Theme.setVerticesFillMainContent(theme['verticesFillMainContent']);
        Theme.setVerticesBorderMainContent(theme['verticesBorderMainContent']);

        //Vertices symbol and font for each state
        Theme.setFillVerticesSymbol(theme['fillVerticesSymbol']);
        Theme.setBorderVerticesSymbol(theme['borderVerticesSymbol']);

        Theme.setVerticesSymbolFillHighlight(
            theme['verticesSymbolFillHighlight']);
        Theme.setVerticesSymbolBorderHighlight(
            theme['verticesSymbolBorderHighlight']);

        Theme.setVerticesSymbolFillSelect(theme['verticesSymbolFillSelect']);
        Theme.setVerticesSymbolBorderSelect(
            theme['verticesSymbolBorderSelect']);

        Theme.setVerticesSymbolFontDefault(theme['verticesSymbolFontDefault']);

        Theme.setVerticesSymbolFontHighlight(
            theme['verticesSymbolFontHighlight']);

        Theme.setVerticesSymbolFontSelect(theme['verticesSymbolFontSelect']);

        //Adjacent vertices symbol and font for each state
        Theme.setAdjacentVerticesSymbolFillHighlight(
            theme['adjacentVerticesSymbolFillHighlight']);
        Theme.setAdjacentVerticesSymbolBorderHighlight(
            theme['adjacentVerticesSymbolBorderHighlight']);

        Theme.setAdjacentVerticesSymbolFillSelect(
            theme['adjacentVerticesSymbolFillSelect']);
        Theme.setAdjacentVerticesSymbolBorderSelect(
            theme['adjacentVerticesSymbolBorderSelect']);

        Theme.setAdjacentVerticesSymbolFontHighlight(
            theme['adjacentVerticesSymbolFontHighlight']);
        Theme.setAdjacentVerticesSymbolFontSelect(
            theme['adjacentVerticesSymbolFontSelect']);

        //Time slot and font
        Theme.setTsFillMainContent(theme['tsFillMainContent']);
        Theme.setTsBorderMainContent(theme['tsBorderMainContent']);

        Theme.setTsFontDefault(theme['tsFontDefault']);
        Theme.setTsFontHighlight(theme['tsFontHighlight']);

        //Sparkline
        Theme.setALT_COLORS(theme["ALT_COLORS"][0], theme["ALT_COLORS"][1]);
        Theme.setSparklineBackground(
            theme['sparklineBackground']); //if ALT_COLORS = null
        Theme.setSparklineHighlight(theme['sparklineHighlight']);
        Theme.setSparklineSelect(theme['sparklineSelect']);

        //Linegraph
        Theme.setHistogramBackgroundLinegraph(
            theme['histogramBackgroundLinegraph']);
        Theme.setHistogramBorderLinegraph(theme['histogramBorderLinegraph']);
      }
    }
  }

  savePreferences() {
    window.localStorage["preferencesBiofabric"] =
        '{"currentTheme": "$currentTheme"}';
  }

  getPreferencesFromStorage() {
    String storedPreferences = window.localStorage["preferencesBiofabric"];
    if (storedPreferences != null) {
      preferencesFromJSON(storedPreferences);
      storedOK = true;
    } else
      return null;
  }

  preferencesFromJSON(String jsonString) {
    Map storedPreferences = json.decode(jsonString);
    currentTheme = storedPreferences["currentTheme"];
    if (DEBUG) print("Theme: $currentTheme");
  }
}
