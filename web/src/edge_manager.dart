part of paohvis;

// list of edges to be displayed
//TODO: this should take care of only visual properties
class EdgeManager {
  CanvasElement _canvas;
  CanvasRenderingContext2D _context;
  //TODO: allow drawing any shape instead of just circles
  bool DRAWCIRCLE = true; // circles at the exges extreme
  int x = 0; // x position of the drawing area
  int y = 0; // y position of the drawing area
  TimeManager tm = new TimeManager();
  ColorManager cm = new ColorManager();
  SizeManager sm = new SizeManager();

  int _frameHeigth = 0;

  Graph _graph;

  Map<String, List<Edge>> _edges;
  Map<String, List<List<Edge>>> _edgesBins;
  Nodes nodes;

  double minWeight = .0;
  double maxWeight = .0;
  bool _binpacked = false;
  int numNodes = 0;
  int maxEdgeDistance = 0; // utility

  double lineWidth = LINE_WIDTH;
  num nodeRadius = max(SCALE * NODE_RADIUS, MIN_NODE_RADIUS);
  num edgeSep = EDGE_SEP;
  num i;
  num minWidth = 0;

  Map<String, num> coordXMinTS = new Map<String, num>();
  Map<String, num> coordXMaxTS = new Map<String, num>();
  num marginTS = 20;

  EdgeManager(Graph g, CanvasElement canvasElement, [num minTSWidth]) {
    _canvas = canvasElement;
    _context = canvasElement.getContext('2d');
    _frameHeigth = _canvas.height;
    _graph = g;
    numNodes = g.nodes.length;
    nodes = g.nodes;
    _edges = g.edges;
    _edgesBins = new Map<String, List<List<Edge>>>();
    minWidth = minTSWidth;
    setCoordMinMaxTS();
//    setNodesDegree();
//    biofabricBinpack();
//    edgeListSetup(0)
  }

  List<List<Edge>> edgesBins(ts) {
    return _edgesBins[ts];
  }

  Map<String, List<Edge>> get edges {
    return _edges;
  }

  void switchToSimple() {
    _edges = _graph.simple;
    setCoordMinMaxTS();
    setup();
  }

  void switchToHyper() {
    _edges = _graph.edges;
    setCoordMinMaxTS();
    setup();
  }

  void setCoordMinMaxTS() {
    _edges.forEach((ts, tsEdges) {
      tsEdges.forEach((e) {
        e.edge.forEach((n) {
          if ((coordXMinTS[ts] != null) || (coordXMaxTS[ts] != null)) {
            if (coordXMinTS[ts] > n.coordX) coordXMinTS[ts] = n.coordX;
            if (coordXMaxTS[ts] < n.coordX) coordXMaxTS[ts] = n.coordX;
          } else {
            coordXMinTS[ts] = n.coordX;
            coordXMaxTS[ts] = n.coordX;
          }
        });
      });
      //print("TS: " + ts + " " +coordXMinTS[ts].toString() + " " + coordXMaxTS[ts].toString());
    });
  }

  void setup() {
    lineWidth = LINE_WIDTH;
    nodeRadius = min(max(NODE_HEIGHT_RAT * NODE_HEIGHT_DIM, MIN_NODE_RADIUS),
        MAX_NODE_RADIUS);
    edgeSep = getEdgeSep();

    minWeight =
        _edges[tm.timeList[0]][0].weight; // TODO: should be already computed
    maxWeight = _edges[tm.timeList[0]][0].weight; // should be already computed
    _edges.forEach((ts, tsEdges) {
      tsEdges.forEach((tsEdge) {
        if (tsEdge.isValid) {
          if (minWeight > tsEdge.weight) minWeight = tsEdge.weight;
          if (maxWeight < tsEdge.weight) maxWeight = tsEdge.weight;
          tsEdge.computeMinMax();
        }
      });
    });
    maxEdgeDistance = maxDistance();
    orderEdges(false, HYPENET_SORT_LENGTH);

    if (HYPENET_PACK_EDGES) {
      binpackAll();
//    binpackUnion();
    }
    else if (REPEAT) {
    //binpackUnion();

     aggregateRepeat();
    }
    else {
      falsePack();
    }

    computeDimensions();
  }

  updateValid([bool toBeValid = false]) {
    _edges.forEach((ts, tsEdges) {
      tsEdges.forEach((e) {
        e.updateValid(toBeValid);
      });
    });
  }

  resetValid() {
    _edges.forEach((ts, tsEdges) {
      tsEdges.forEach((e) {
        e.resetValid();
      });
    });
  }

  num getEdgeSep() {
    return EDGE_SEP * SCALE;
  }

  computeDimensions() {
    // TODO: this should be in vis manager
//    if(_binpacked){

    if (PAOVIS) {
//      edgeSep = SCALE*EDGE_SEP;
      edgeSep = getEdgeSep();

      _edgesBins.forEach((ts, tsEdges) {
//        num currentEdgeSep = SCALE*edgeSep;
        num tsWidth =
            max(MIN_TS_WIDTH, tsEdges.length * SCALE * (lineWidth + edgeSep));
        if (!tm.isMinimized(ts))
          tm.setWidth(ts, tsWidth); // shouldn't be here... event manager?!!
        else {
          if (!tm.isFirst(ts) && tm.isPrevMinimized(ts)) {
            tm.setWidth(ts, 0);
          } else {
            tm.setWidth(ts, minWidth);
          }
        }
//         edgeSep = min(edgeSep, currentEdgeSep);
      });
    } else if (NODELINK || SPLAT || CURVES) {
      _edges.forEach((ts, tsEdgesBins) {
        num tsWidth = min(CANVAS_WIDTH / tm.length, MIN_TS_WIDTH);
        tm.setWidth(ts, tsWidth); // shouldn't be here... event manager?!!
      });
    } else {
      edgeSep = getEdgeSep();

      _edges.forEach((ts, tsEdges) {
//        num currentEdgeSep = SCALE*edgeSep;
        num tsWidth =
            max(MIN_TS_WIDTH, tsEdges.length * SCALE * (lineWidth + edgeSep));
        tm.setWidth(ts, tsWidth); // shouldn't be here... event manager?!!
//        edgeSep = min(edgeSep, currentEdgeSep);
      });
    }
    tm.computePosXAll();
  }

  /** Render links among nodes */
  void render() {
    if (PAOVIS) {
      if (_binpacked)
        drawBiofabric();
      else
        drawBiofabricUnpacked();
    }
    if (CURVES) {
      drawCurves();
    }
    if (SPLAT) {
      drawSplat();
    }
    if (NODELINK) {
      drawNodelink();
    }
  }

  void drawCurves() {
//    int counter = 0;
    _edges.forEach((ts, tsEdges) {
      if (tm.isVisible(ts)) {
        // first, renders unselected/unhighlighted edges
        if (!tm.isMinimized(ts)) {
          tsEdges.forEach((e) {
            if (e.length > 1) {
              // only draw curves connecting at least than 2 nodes
              // compute displacement, can be set externally
              num curveDisplacement = 0.0;
              num distance = (e._lenght.abs()).toInt();
              curveDisplacement =
                  (tm.getWidth(ts) / maxEdgeDistance * distance);
              if (!(e.isHighlighted || e.isSelected)) {
                connectNodes(e, tm.getPosX1(ts), curveDisplacement, 0.0,
                    numNodes.toDouble(), distance.toDouble());
              }
            }
          });
          // then, renders selected/highlighted edges
          tsEdges.forEach((e) {
            if (e.length > 1) {
              // only draw curves connecting at least than 2 nodes
              // compute displacement, can be set externally
              num curveDisplacement = 0.0;
              num distance = (e._lenght.abs()).toInt();
              curveDisplacement =
                  (tm.getWidth(ts) / maxEdgeDistance * distance);
              if (e.isHighlighted || e.isSelected) {
                connectNodes(e, tm.getPosX1(ts), curveDisplacement, 0.0,
                    numNodes.toDouble(), distance.toDouble());
              }
            }
          });
//      counter ++;
        }
      }
    });
  }

  /**
   * Draws curves between two nodes
   *  param curveDisplacement: increment in the height of the
   *  curve to better separate and differentiate curves. Since the function draws just one
   *  curve it should be set manually.
   *  Still to be refined displacement is the start of the curve, should be the same of the
   *  beginning of the time interval min and max are needed if color coding have to be
   *  considered.
   *  */
  void connectNodes(Edge e, num displacement, double curveDisplacement,
      [double min, double max, double value]) {
    String strokeStyle = cm.getEdgeColor(e, 'CURVES');
    //alpha value management
    num _previousGlobalAlpha = _context.globalAlpha;
    num _globalAlpha = 1;
    bool isGlobalAlphaChanged = (_globalAlpha != _previousGlobalAlpha);

    Node a = e.first;
    Node b = e.last;

    _context
      ..strokeStyle = strokeStyle
      ..fillStyle = strokeStyle
      ..lineWidth = 1;
    if (isGlobalAlphaChanged) _context.globalAlpha = _globalAlpha;

    num halfNodeHeight = a.height / 2;

    if (ENABLED_COLORGROUP) {
      // TODO: include in ColorManager

      Point startPoint = new Point(displacement, a.y + halfNodeHeight);
      Point endPoint = new Point(displacement, b.y + halfNodeHeight);

      String colorNodeA = Theme.edgesCurvesDefault;
      String colorNodeB = Theme.edgesCurvesDefault;
      String _strokeStyle = Theme.edgesCurvesDefault;
      double communityNodeA = -1.0;
      double communityNodeB = -1.0;

      if (a.hasCommunity) {
        if (a._community != "theComId") {
          communityNodeA = double.parse(a._community);
          colorNodeA = getColorCode(
              1.0,
              1.0,
              communityNodeA,
              ColorCodings.Communities,
              false,
              new HexColor(_strokeStyle).toHslColor().h.toInt(),
              new HexColor("#ffffff"));
        }
      }

      if (b.hasCommunity) {
        if (b._community != "theComId") {
          communityNodeB = double.parse(b._community);
          colorNodeB = getColorCode(
              1.0,
              1.0,
              communityNodeB,
              ColorCodings.Communities,
              false,
              new HexColor(_strokeStyle).toHslColor().h.toInt(),
              new HexColor("#ffffff"));
        }
      }

      if (communityNodeA == communityNodeB) {
        _context..strokeStyle = colorNodeA;
      } else {
        CanvasGradient gradient = _context.createLinearGradient(
            startPoint.x, startPoint.y, endPoint.x, endPoint.y);
        gradient.addColorStop(0, colorNodeB);
        gradient.addColorStop(1, colorNodeA);

        _context..strokeStyle = gradient;
      }
    }

    _context
      ..beginPath()
      ..moveTo(displacement, a.y + halfNodeHeight)
      ..bezierCurveTo(
          displacement + curveDisplacement,
          a.y + a.height,
          displacement + curveDisplacement,
          b.y,
          displacement,
          b.y + halfNodeHeight)
      ..stroke();
    if (isGlobalAlphaChanged) _context.globalAlpha = _previousGlobalAlpha;
  }

  /// draws edge splat
  void drawSplat() {
    String t;
    i = 0;
    _edges.forEach((ts, tsEdges) {
      if (tm.isVisible(ts)) {
        if (!tm.isMinimized(ts)) {
          var sorteKeys = _edges.keys.toList()..sort();
          t = sorteKeys.elementAt(0);
          num displacement;
          // first, render unselected/not highlighted edges
          tsEdges.forEach((e) {
            if (e.length > 1) {
              // only draw curves connecting at least 2 nodes
              if (!(e.isHighlighted || e.isSelected)) {
                if (SPLAT_INTERLEAVING)
                  displacement = tm.getPosX1(t);
                else
                  displacement = tm.getPosX1(ts);
                splatNodes(e, ts, displacement);
              }
            }
          });
          // then, render selected/highlighted edges
          tsEdges.forEach((e) {
            if (e.length > 1) {
              // only draw curves connecting at least than 2 nodes
              if (e.isHighlighted || e.isSelected) {
                if (SPLAT_INTERLEAVING)
                  displacement = tm.getPosX1(t);
                else
                  displacement = tm.getPosX1(ts);
                splatNodes(e, ts, displacement);
              }
            }
          });
          if (SPLAT_INTERLEAVING) i++;
        }
      }
    });
  }

  /**
   * Draws curves between two nodes
   *  param curveDisplacement: increment in the height of the
   *  curve to better separate and differentiate curves. Since the function draws just one
   *  curve it should be set manually.
   *  Still to be refined displacement is the start of the curve, should be the same of the
   *  beginning of the time interval min and max are needed if color coding have to be
   *  considered.
   *  */
  void splatNodes(Edge e, String ts, num displacement,
      [double min, double max, double value]) {
    num lw = 1;

    String _strokeStyle = cm.getEdgeColor(e, 'SPLAT');
    _context
      ..strokeStyle = _strokeStyle
      ..lineWidth = lw;

//    String _strokeLines = Theme.strokeLines;
    //alpha value management
    num _previousGlobalAlpha = _context.globalAlpha;
    num _globalAlpha = 1;
    bool isGlobalAlphaChanged = (_globalAlpha != _previousGlobalAlpha);

    Node a = e.first;
    Node b = e.last;

    //num displacement = tm.getPosX1(ts);
//    if (min!=null && max!= null && value!= null){
//      color = getColorCode(min, max, value, ColorCodings.fixed, true,
//          new HexColor(_strokeStyle).toHslColor().h.toInt(), new HexColor("#ffffff"));
//    }

    if (isGlobalAlphaChanged) _context.globalAlpha = _globalAlpha;
//    _context
//      ..strokeStyle = color
//      ..lineWidth = 2
//      ..globalAlpha = 0.5;
//
//    if(e.isHighlighted) {
//      _context
//        ..strokeStyle = Theme.strokeHighlight
//        ..globalAlpha = 1;
//    }
//
//    if(e.isSelected) {
//      _context
//        ..strokeStyle = Theme.strokeSelect
//        ..globalAlpha = 1;
//    }
//
    num halfNodeHeight = a.height / 2;
//    var previousGlobalAlpha = _context.globalAlpha;
    displacement = displacement + i;

    Point startPoint = new Point(displacement, a.y + halfNodeHeight);
    Point endPoint =
        new Point(displacement + tm.getWidth(ts), b.y + halfNodeHeight);

    String colorNodeA = Theme.edgesSplatDefault;
    String colorNodeB = Theme.edgesSplatDefault;
    String colorIntersect = Theme.edgesSplatDefault;

    if (ENABLED_COLORGROUP) {
      double communityNodeA = -1.0;
      double communityNodeB = -1.0;

      if (a.hasCommunity) {
        if (a._community != "theComId") {
          communityNodeA = double.parse(a._community);
          colorNodeA = getColorCode(
              1.0,
              1.0,
              communityNodeA,
              ColorCodings.Communities,
              false,
              new HexColor(_strokeStyle).toHslColor().h.toInt(),
              new HexColor("#ffffff"));
        }
      }

      if (b.hasCommunity) {
        if (b._community != "theComId") {
          communityNodeB = double.parse(b._community);
          colorNodeB = getColorCode(
              1.0,
              1.0,
              communityNodeB,
              ColorCodings.Communities,
              false,
              new HexColor(_strokeStyle).toHslColor().h.toInt(),
              new HexColor("#ffffff"));
        }
      }

      if (communityNodeA == communityNodeB) {
        _context..strokeStyle = colorNodeA;
      } else {
        CanvasGradient gradient = _context.createLinearGradient(
            startPoint.x, startPoint.y, endPoint.x, endPoint.y);
        gradient.addColorStop(0, colorNodeB);
        gradient.addColorStop(1, colorNodeA);

        _context..strokeStyle = gradient;
      }
    }

    nodeRadius = SCALE * NODE_RADIUS;

    if (e.isSelected) {
      nodeRadius = 1.5 * nodeRadius;

      if (a.isSelected || b.isSelected) {
        colorNodeA = Theme.verticesSplatSelect;
        colorNodeB = Theme.verticesSplatSelect;
      }

      if (a.isAdjacentToSelected) {
        colorNodeA = Theme.adjacentVerticesSplatSelect;
      }
      if (b.isAdjacentToSelected) {
        colorNodeB = Theme.adjacentVerticesSplatSelect;
      }

      _context..lineWidth = lw + LINE_WIDTH_STRONG;
    } else if (e.isHighlighted) {
//      _strokeStyle = Theme.edgesSplatHighlight;
      nodeRadius = 1.5 * nodeRadius;
//      _context
//        ..lineWidth = lw+LINE_WIDTH_STRONG;

    } else {
      if (ENABLED_HIGHLIGHT &&
          (NODE_HIGHLIGHTED || EDGE_HIGHLIGHTED || TS_HIGHLIGHTED)) {
        colorNodeA = Theme.verticesPaovisNotHighlight;
        colorNodeB = Theme.verticesPaovisNotHighlight;
        colorIntersect = Theme.verticesPaovisNotHighlight;
      } else if (ENABLED_SELECTION &&
          (NODE_SELECTED || EDGE_SELECTED || TS_SELECTED)) {
        colorNodeA = Theme.verticesPaovisNotHighlight;
        colorNodeB = Theme.verticesPaovisNotHighlight;
        colorIntersect = Theme.verticesPaovisNotHighlight;
      }
    }

    _context
      ..beginPath()
      ..moveTo(startPoint.x, startPoint.y)
      ..lineTo(endPoint.x, endPoint.y)
      ..moveTo(startPoint.x, endPoint.y)
      ..lineTo(endPoint.x, startPoint.y)
      ..stroke();

    drawCircle(_context, startPoint, nodeRadius, colorNodeA);
    drawCircle(
        _context, new Point(startPoint.x, endPoint.y), nodeRadius, colorNodeB);
    drawCircle(
        _context, new Point(endPoint.x, startPoint.y), nodeRadius, colorNodeA);

    if (e.edge.length > 2) {
      for (int i = 1; i < e.edge.length - 1; i++) {
        Line L1 = new Line(
            new Point(displacement, e.edge[i].y + halfNodeHeight),
            new Point(
                displacement + tm.getWidth(ts), e.edge[i].y + halfNodeHeight));
        Line L2 = new Line(startPoint, endPoint);
        Point intersectPoint = getLinesIntersect(L1, L2);

        nodeRadius = SCALE * NODE_RADIUS;

        if (e.isSelected) {
          colorIntersect = Theme.adjacentVerticesSplatSelect;
          nodeRadius = 1.5 * nodeRadius;
        }
        if (e.isHighlighted) {
          colorIntersect = Theme.adjacentVerticesSplatHighlight;
          nodeRadius = 1.5 * nodeRadius;
        }
        if (HYPER_EDGES_SPLAT)
          drawCircle(_context, intersectPoint, nodeRadius, colorIntersect);
      }
    }
    drawCircle(_context, endPoint, nodeRadius, colorNodeB);

    if (isGlobalAlphaChanged) _context.globalAlpha = _previousGlobalAlpha;
  }

  /// draws edge splat
  void drawNodelink() {
    nodes.coordMinMaxNodes = nodes.nodes;

    /// If the dataset has negative coordinates (x<0 or y<0),
    /// the graph is displayed in the negative axis. To solve
    /// this problem, the minimum negative coordinate is used
    /// (in absolute value) to move the graph in the TS space.

    _edges.forEach((ts, tsEdges) {
      if (tm.isVisible(ts)) {
        if (!tm.isMinimized(ts)) {
          tsEdges.forEach((e) {
            if (!(e.isHighlighted || e.isSelected)) nodelinkNodes(e, ts);
          });
        }
      }
    });
    _edges.forEach((ts, tsEdges) {
      if (tm.isVisible(ts)) {
        if (!tm.isMinimized(ts)) {
          tsEdges.forEach((e) {
            if (e.isHighlighted || e.isSelected) nodelinkNodes(e, ts);
          });
        }
      }
    });
  }

  //    var nlPosX = (n) => displacement + (n.coordX + nodes._displacementCoordX) * len / (nodes.coordXMax - nodes.coordXMin);
  num nlPosX(Node n, String ts) {
    num margin = 16;
    num displacement = tm.getPosX1(ts) + margin;
    num len = tm.getWidth(ts) - 2 * margin;

    return scaleCoord(n.coordX, nodes.coordXMin, nodes.coordXMax, displacement,
        displacement + len);
  }

  num nlPosY(Node n) {
    if (ENABLED_BIND_NODELINK) {
      return n.y + n.height / 2;
    }
    num displacement = 30;
    return scaleCoord(n.coordY, nodes.coordYMin, nodes.coordYMax, displacement,
        min(nodes.getHeightAll(), CANVAS_HEIGHT));
  }

  void nodelinkNodes(Edge e, String ts) {
    if (e.length == 0) return;

    String _strokeStyle = cm.getEdgeColor(e, 'NODELINK');

//    String _strokeLines = Theme.strokeLines;
    //alpha value management
    num _previousGlobalAlpha = _context.globalAlpha;
    num _globalAlpha = 1;
    bool isGlobalAlphaChanged = (_globalAlpha != _previousGlobalAlpha);

    Node a = e.first;
    Node b = e.last;

    String colorNodeA = cm.getNodeColor(a, e, 'NODELINK');
    String colorNodeB = cm.getNodeColor(b, e, 'NODELINK');
    num lw;
    num radiusA = nodeRadius * 2;
    num radiusB = nodeRadius * 2;

    if (e.isSelected) {
      lw = LINE_WIDTH_STRONG;
    }

    if (a.isHighlighted) {
      radiusA *= 1.6;
    } else if (a.isAdjacentToHighlighted) {
      radiusA *= 1.3;
    } else if (a.isSelected) {
      radiusA *= 1.6;
    } else if (a.isAdjacentToSelected) {
      radiusA *= 1.3;
    }

    if (b.isHighlighted) {
      radiusB *= 1.6;
    } else if (b.isAdjacentToHighlighted) {
      radiusB *= 1.3;
    } else if (b.isSelected) {
      radiusB *= 1.6;
    } else if (b.isAdjacentToSelected) {
      radiusB *= 1.3;
    }

    _context
      ..strokeStyle = _strokeStyle
      ..lineWidth = lw;
    if (isGlobalAlphaChanged) _context.globalAlpha = _globalAlpha;
//

    Point nodeA;
    Point nodeB;

    nodeA = new Point(nlPosX(a, ts), nlPosY(a));
    nodeB = new Point(nlPosX(b, ts), nlPosY(b));

    _context
      ..beginPath()
      ..moveTo(nodeA.x, nodeA.y)
      ..lineTo(nodeB.x, nodeB.y)
      ..stroke();

    drawCircle(_context, nodeA, radiusA, colorNodeA);
    drawCircle(_context, nodeB, radiusB, colorNodeB);

    if (isGlobalAlphaChanged) _context.globalAlpha = _previousGlobalAlpha;
  }

  void orderEdges([bool desc, bool byLength]) {
    // bin packing
    if (byLength != null && byLength) {
      if (desc != null && desc) {
        _edges.forEach((ts, tsEdges) {
          tsEdges.sort((b, a) {
            if (a.isValid && b.isValid) {
              int c = a.lenght.compareTo(b.lenght);
              if (c == 0) c = a.first.position.compareTo(b.first.position);
              if (c == 0) c = a.last.position.compareTo(b.last.position);
              return c;
            }
            return 0;
          });
        });
      } else {
        _edges.forEach((ts, tsEdges) {
          tsEdges.sort((a, b) {
            if (a.isValid && b.isValid) {
              int c = a.lenght.compareTo(b.lenght);
              if (c == 0) c = a.first.position.compareTo(b.first.position);
              if (c == 0) c = a.last.position.compareTo(b.last.position);
              return c;
            }
            return 0;
          });
        });
      }
    } else {
      if (desc) {
        _edges.forEach((ts, tsEdges) {
          tsEdges.sort((b, a) {
            if (a.isValid && b.isValid) {
              int c = a.first.position.compareTo(b.first.position);
              if (c == 0) c = a.last.position.compareTo(b.last.position);
              return c;
            }
            return 1;
          });
        });
      } else {
        _edges.forEach((ts, tsEdges) {
          tsEdges.sort((a, b) {
            if (a.isValid && b.isValid) {
              int c = a.first.position.compareTo(b.first.position);
              if (c == 0) c = a.last.position.compareTo(b.last.position);
              return c;
            }
            if (a.isValid && !b.isValid) return 1;
            if (!a.isValid && b.isValid) return -1;
            return 0;
          });
        });
      }
    }
  }

  List<List<Edge>> binpack(List<Edge> edges) {
    List<Edge> tsEdgeCopy = new List.from(edges);
    List<List<Edge>> tsEdgesBins = new List<List<Edge>>();

    while (tsEdgeCopy.length > 0) {
      Edge toPlace = tsEdgeCopy.first;
      if (toPlace.isValid) {
        // binpacks only valid edges
        bool placed = false;
        // check if there is space in the existing bins

        tsEdgesBins.takeWhile((bin) => !placed).forEach((bin) {
         if (fitsInBin(toPlace, bin)) {
            bin.add(toPlace);
            placed = true;
          }
        });
        if (!placed) {
          tsEdgesBins.add(new List<Edge>());
          tsEdgesBins.last.add(toPlace);
        }
      }

      tsEdgeCopy.removeAt(0);
    }
    return tsEdgesBins;
  }

  List<List<Edge>> packUnion(List<Edge> edges) {
    List<Edge> tsEdgeCopy = new List.from(edges);
    List<List<Edge>> tsEdgesBins = new List<List<Edge>>();

    while (tsEdgeCopy.length > 0) {
      Edge toPlace = tsEdgeCopy.first;

      tsEdgesBins.add(new List<Edge>());
      tsEdgesBins.last.add(toPlace);


      tsEdgeCopy.removeAt(0);
    }
    return tsEdgesBins;
  }

  void falsePack() {
    // false packing
    _edgesBins = new Map<String, List<List<Edge>>>();

    _edges.forEach((ts, tsEdges) {
      List<List<Edge>> tsEdgesBins = new List<List<Edge>>();

      tsEdges.forEach((e) {
        List<Edge> bin = new List<Edge>();
        if (e.isValid) bin.add(e);
        if (bin.length > 0) tsEdgesBins.add(bin);
      });

//      tsEdgesBins.sort((List<Edge> la, List<Edge> lb) => la[0].lenght -  lb[0].lenght );

      _edgesBins.putIfAbsent(ts, () => tsEdgesBins);
    });

    _binpacked = true;

    int maxLength = 0;
    _edgesBins.forEach((ts, tsEdgesBins) {
      maxLength =
          (tsEdgesBins.length > maxLength) ? tsEdgesBins.length : maxLength;
    });
  }

  void binpackAll() {
    // bin packing
    _edgesBins = new Map<String, List<List<Edge>>>();

    _edges.forEach((ts, tsEdges) {
      List<List<Edge>> tsEdgesBins = binpack(tsEdges);
      _edgesBins.putIfAbsent(ts, () => tsEdgesBins);
    });

    _binpacked = true;

    int maxLength = 0;
    _edgesBins.forEach((ts, tsEdgesBins) {
      maxLength =
          (tsEdgesBins.length > maxLength) ? tsEdgesBins.length : maxLength;
    });
  }

  int occurrence(List<Edge> test, Edge e){
    int occurrence = 0;
    test.forEach((k){
      if(k.equal(e)){
        occurrence++;
      };
    });
    return occurrence;
  }


  void aggregateRepeat(){
    //print ("aggregateRepeat called");
    List<Edge> unionEdges = new List<Edge>();

    _edges.forEach((ts, tsEdges) {
      tsEdges.forEach((e) {
        if (e.isValid) if (!unionEdges.any((ue) => ue.equal(e))) {
          unionEdges.add(e);
        }
        if(occurrence(tsEdges, e)>1){
          e.repeat = (occurrence(tsEdges, e)-1);
        }
      });


     /* print ("there are " + tsEdges.length.toString() + " edges in TS " + ts);
      tsEdges.forEach((e) {
        if (e.isValid) if (!unionEdges.any((ue) => ue.equal(e))) {
          unionEdges.add(e);
        } else {
          unionEdges.forEach((ue){
            if (ue.equal(e)) {
              ue.addRepeat();
              print("found existing edge: " + ue.repeat.toString());
            } else {
              print("existing edge with weight " + e.repeat.toString() + " but not found yet");
            }
          });
        }
      });*/
    });
    List<List<Edge>> unionPacked = packUnion(unionEdges);

//    // bin packing
    _edgesBins = new Map<String, List<List<Edge>>>();

    _edges.forEach((ts, tsEdges) {
      List<List<Edge>> tsEdgesBins = new List<List<Edge>>();
      unionPacked.forEach((unionBin) {
        List<Edge> bin = new List<Edge>();
        bool placed = false;
        tsEdges.takeWhile((_) => !placed).forEach((e) {
          if (e.isValid) if (unionBin.any((ue) => e.equal(ue))) {
            bin.add(e);
            placed = bin.length == unionBin.length;
          }
        });
        if (bin.length > 0) tsEdgesBins.add(bin);
      });
      _edgesBins.putIfAbsent(ts, () => tsEdgesBins);
    });
   //falsePack();

    _binpacked = true;

    int maxLength = 0;
    _edgesBins.forEach((ts, tsEdgesBins) {
      maxLength =
      (tsEdgesBins.length > maxLength) ? tsEdgesBins.length : maxLength;
    });

   /* _edgesBins = new Map<String, List<List<Edge>>>();

    _edges.forEach((ts, tsEdges) {
      List<List<Edge>> tsEdgesBins = binpack(tsEdges);
      _edgesBins.putIfAbsent(ts, () => tsEdgesBins);
    });

    _binpacked = true;

    int maxLength = 0;
    _edgesBins.forEach((ts, tsEdgesBins) {
      maxLength =
      (tsEdgesBins.length > maxLength) ? tsEdgesBins.length : maxLength;
    });*/

  }

  void binpackUnion() {
    // brute force union: if this works TODO: do it efficiently

    List<Edge> unionEdges = new List<Edge>();

    _edges.forEach((s, tsEdges) {
      tsEdges.forEach((e) {
        if (e.isValid) if (!unionEdges.any((ue) => ue.equal(e))) {
          unionEdges.add(e);
        }
      });
    });
    List<List<Edge>> unionPacked = binpack(unionEdges);

//    // bin packing
    _edgesBins = new Map<String, List<List<Edge>>>();

    _edges.forEach((ts, tsEdges) {
      List<List<Edge>> tsEdgesBins = new List<List<Edge>>();
      unionPacked.forEach((unionBin) {
        List<Edge> bin = new List<Edge>();
        bool placed = false;
        tsEdges.takeWhile((_) => !placed).forEach((e) {
          if (e.isValid) if (unionBin.any((ue) => e.equal(ue))) {
            bin.add(e);
            placed = bin.length == unionBin.length;
          }
        });
        if (bin.length > 0) tsEdgesBins.add(bin);
      });
      _edgesBins.putIfAbsent(ts, () => tsEdgesBins);
    });
//    falsePack();

    _binpacked = true;

    int maxLength = 0;
    _edgesBins.forEach((ts, tsEdgesBins) {
      maxLength =
          (tsEdgesBins.length > maxLength) ? tsEdgesBins.length : maxLength;
    });
  }

  void drawBiofabricUnpacked() {
    // TODO: implement this, check how to combine this with packed biofabric

//    drawBiofabric();
  }

  void drawVerticalEdges(LineStyle ls) {
    num counterBin;
    LineStyle currLs = ls;

//      _edgesBins.forEach((ts, tsEdgesBins) {

    tm.timeList.forEach((String ts) {
      var tsEdgesBins = _edgesBins[ts];

      counterBin = 0;
      tsEdgesBins.forEach((bin) {
        bin.forEach((e) {
//            if (e.isValid){
          e.xPos = edgeDisplacement(ts, counterBin);
//            }
        });
        counterBin++;
      });

      if (tm.isVisible(ts)) {
        if (!tm.isMinimized(ts)) {
          counterBin = 0;
          tsEdgesBins.forEach((bin) {
            String color1, color2 = "#cccccc";
            num lw = lineWidth;

            bin.forEach((e) {
              if(e.repeat>1 && REPEAT ){
                lw = LINE_WIDTH*e.repeat;
              }
              if (e.isValid) {
//                if (DEBUG) print("e.isValid - e.weight: " + e.weight.toString() +
//                " - e.last.y: " + e.last.y.toString() + " - e.first.y: " + e.first.y.toString());

                if (HYPER_EDGES_STRONGER) lw = e.weight > 2 ? lw + 1.5 : lw;
                if (!((e.last.y < 0) ||
                    (e.first.y > _canvas.height / window.devicePixelRatio))) {
                  //                  color1 = colors[counterBin % colors.length];
//                    if (DEBUG) print("counterBin = " + counterBin.toString());
                  color1 = cm.getEdgeColor(e, 'PAOVIS',
                      counterBin.toInt().isEven || !ALTERNATE_COLORS);

                  if (e.isSelected) {
                    lw += LINE_WIDTH_STRONG;
                  } else if (e.isHighlighted) {
                    //|| e.isSideHighlighted) {
                    lw += LINE_WIDTH_HIGHLIGHT;
                  } else if (e.isSideHighlighted) {
                    //|| e.isSideHighlighted) {
                    lw += LINE_WIDTH_HIGHLIGHT / 2;
                  }

                  //                  if (e.inIntersection)
                  //                  currLs = LineStyle.dashed;//For security: in case something went wrong

                  if (DEBUG) print("about to drawEdgeTwoColors");
                  drawEdgeTwoColors(e, e.xPos, color1, color2, currLs, lw);
                  if (DEBUG) print("about to set lineWidth to lw");
                  lw = lineWidth;
                }
              }
            });
            counterBin++;
          });
        }
      }
    });
  }

  void drawDotsNodes() {
    int counterBin;
    String vertexColor;
    num radius = nodeRadius;

    _context
//        ..strokeStyle = vertexColor
//        ..fillStyle = vertexColor
      ..lineWidth = 1.3
      ..globalAlpha = 1;

    _edgesBins.forEach((ts, tsEdgesBins) {
      if (tm.isVisible(ts) && !tm.isMinimized(ts)) {
//        if (!tm.isMinimized(ts)) {
        counterBin = 0;
        tsEdgesBins.forEach((bin) {
          bin.forEach((e) {
            //bool drawE = true;
            if (e.isValid &&
                !((e.last.y < 0) ||
                    (e.first.y > _canvas.height / window.devicePixelRatio))) {
//                bool DRAWCIRCLE = true;
              //TODO: change magic numbers
              num inc = nodeRadius * 2.0;
              num shift = inc + nodeRadius * 2.0;

              if (EDGE_HIGHLIGHTED && e.isHighlighted) {
                inc = nodeRadius * 5.0;
                shift = inc + nodeRadius * 2.5;
              }
              int nodePosInEdge = 0;
              e.edgeUnfiltered.forEach((Node n) {
                nodePosInEdge++;
                num idCom = null;
                if (DRAWCIRCLE) {
                  if (NODE_COLOR_AS_EDGE) {
                    vertexColor = cm.getEdgeColor(
                        e, 'PAOVIS', counterBin.isEven || !ALTERNATE_COLORS);
                  } else {
                    if (n.getTsCommunity(ts) != null) {
                      idCom = n.getTsCommunity(ts).toInt();
//                        nodeCom = _graph.communities.getComName(idCom);
                    }
                    vertexColor = cm.getNodeColor(n, e, 'PAOVIS', idCom, false);
                  }
                  radius = nodeRadius;
                  if (e.isSelected || e.isSideSelected) {
                    if (n.isSelected) {
                      radius = 1.5 * nodeRadius;
                    }
//                      else if (n.isAdjacentToSelected) {
//                        radius = 1.3 * nodeRadius;
//                      }
                  }
                  if (ENABLED_HIGHLIGHT &&
                      (NODE_HIGHLIGHTED ||
                          EDGE_HIGHLIGHTED ||
                          TS_HIGHLIGHTED)) {
                    radius = nodeRadius;

                    if (n.isSelected) {
                      radius = 1.5 * nodeRadius;
                    } else if (e.isHighlighted) {
                      radius = 1.1 * nodeRadius;
                      if (n.isHighlighted) {
                        radius = 1.3 * nodeRadius;
                      }
                      if (n.isAdjacentToHighlighted) {
                        radius = 1.1 * nodeRadius;
                      }
                    }
                  }

                  if (ENABLED_COLORGROUP) {
                    _context
                      ..strokeStyle = vertexColor
                      ..fillStyle = vertexColor;
                    if(vertexColor != Theme.verticesNotHighlight){
                      _context
                        ..strokeStyle = '#666666';
                    }

                } else if (e.inIntersection && n.isSelected) {
                    _context
                      ..strokeStyle = vertexColor
                      ..fillStyle = vertexColor;
                  } else {
                    _context
                      ..strokeStyle = vertexColor
                      ..fillStyle = '#ffffff';
                  }
                  num gvx = e.xPos;
                  num gvy = n.y + n.height / 2;

                  var role;
                  switch (SYMBOL_SELECTED){
                    case 0:
                    role = (ENABLED_NODEROLE && nodePosInEdge == 1)
                    ? shapes.star  : shapes.fillcircle; // 1st node as a star
                  break;
                    case 1:
                      role = (ENABLED_NODEROLE && nodePosInEdge == 1)
                      ? shapes.rect  : shapes.fillcircle; // 1st node as a rect
                      break;
                    case 2:
                      role = (ENABLED_NODEROLE && nodePosInEdge == 1)
                      ? shapes.circle  : shapes.fillcircle; // 1st node as a circle
                      break;
                    case 3:
                      role = (ENABLED_NODEROLE && nodePosInEdge == 1)
                      ? shapes.cross  : shapes.fillcircle; // 1st node as a cross
                      break;
                    case 4:
                      role = (ENABLED_NODEROLE && nodePosInEdge == 1)
                      ? shapes.triangle  : shapes.fillcircle; // 1st node as a triangle
                      break;
                    case 5:
                      role = (ENABLED_NODEROLE && nodePosInEdge == 1)
                          ? shapes.reverseTriangle  : shapes.fillcircle; // 1st node as a reverseTriangle
                      break;
                    case 6:
                      role = (ENABLED_NODEROLE && nodePosInEdge == 1)
                          ? shapes.diamond  : shapes.fillcircle; // 1st node as a diamond
                      break;
                    case 7:
                      role = (ENABLED_NODEROLE && nodePosInEdge == 1)
                          ? shapes.diamondSquare  : shapes.fillcircle; // 1st node as a diamondSquare
                      break;
                  }
                  if (!n.isValid) {
                    // draw drips

                    gvy = e.last.y + shift; // drips below hyperedge
                    radius *= 0.7; // drips smaller than vertices
                    shift += inc; // drips displacement
                  }
                  drawVertex(_context, new Point(gvx, gvy), radius, role);
                }
              });
            }
          });
          counterBin++;
        });
        //      counter++;
//        }
      }
    });
  }

  num edgeDisplacement(String ts, num c) {
    num step = tm.getWidth(ts) / (_edgesBins[ts].length + 1);
//      num step = edgeSep;
    num margin = step;

    return tm.getPosX1(ts) + margin + c * step;
  }

  void drawLabelsAndDrips() {
    int counterBin;
    String vertexColor;
    num radius = nodeRadius;

    _edgesBins.forEach((ts, tsEdgesBins) {
      if (tm.isVisible(ts)) {
//          num step = tm.getWidth(ts) / (tsEdgesBins.length + 1);
        if (!tm.isMinimized(ts)) {
          counterBin = 0;
          tsEdgesBins.forEach((bin) {
            bin.forEach((e) {
              if (e.isHighlighted) {
                // || e.isSelected) {
                if (e.isValid &&
                    !((e.last.y < 0) ||
                        (e.first.y >
                            _canvas.height / window.devicePixelRatio))) {
//              bool DRAWCIRCLE = true;
                  //TODO: change magic numbers

                  num inc = nodeRadius * 5.0;
                  num shift = inc + nodeRadius * 2.5;

                  e.edgeUnfiltered.forEach((Node n) {
                    if (DRAWCIRCLE) {
                      vertexColor = cm.getNodeColor(n, e, 'PAOVIS');

                      radius = nodeRadius;

                      if (e.isSelected) {
                        if (n.isSelected) {
                          radius = 1.6 * nodeRadius;
                        }
                        if (n.isAdjacentToSelected) {
                          radius = 1.3 * nodeRadius;
                        }
                      }
                      if (ENABLED_HIGHLIGHT &&
                          (NODE_HIGHLIGHTED ||
                              EDGE_HIGHLIGHTED ||
                              TS_HIGHLIGHTED)) {
                        radius = nodeRadius;

                        if (e.isHighlighted) {
                          radius = 1.5 * nodeRadius;
                          if (n.isHighlighted) {
                            radius = 1.6 * nodeRadius;
                          }
                          if (n.isAdjacentToHighlighted) {
                            radius = 1.3 * nodeRadius;
                          }
                        }
                      }

                      _context
                        ..strokeStyle = '#000000'
                        ..fillStyle = '#000000' //vertexColor
                        ..textBaseline = "middle";

                      if (e.isHighlighted &&
                          GHOST_TOOLTIPS &&
                          EDGE_HIGHLIGHTED) {
                        num gvx = e.xPos + radius + 3;
                        num fontSize =
                            min(max(nodeRadius * 6.5, n.height * 1.95), 18);

                        if (n.isValid) {
                          drawGVLabel(n, gvx, n.y + n.height / 2, fontSize);
                        } else {
                          fontSize = min(fontSize * 0.9, inc);
                          num gvy = e.last.y + shift;
                          drawGVLabel(n, gvx, gvy, fontSize);

                          shift += inc;
                        }
                      }
                    }
                  });
                }
              }
            });
            counterBin++;
          });
          //      counter++;
        }
      }
    });
  }

  void drawBiofabric() {
    /**
       * Two colors are needed in Biofabric technique, we use here the same color
       * and the second one is created with a displacement of 50 from the chosen
       * color
       * */

    int counterBin;

    // sets the line style
    int i = 0;
    bool found = false;
    LineStyle ls = LineStyle.solid;
    while ((i < lineStylingList.length) && (!found)) {
      if (lineStylingList[i].status) {
        found = true;
        ls = LineStyle.values[i];
      }
      i++;
    }

    drawVerticalEdges(ls);

    drawDotsNodes();

    drawLabelsAndDrips();
  }

  void drawGVLabel(Node n, num x, num y, num height) {
    _context
      ..font = "lighter " + (height).toString() + "px " + CANVAS_FONT
      ..textBaseline = "middle";

    String nodeText = n.name;
    _context..fillText(nodeText, x, y);
  }

  int binDistancePrev(Edge edge, List<List<Edge>> binList, int binIndex) {
    int MAX_SEPARATION = 10;
    int distPrev = MAX_SEPARATION;

    for (int currBin = binIndex - 1;
        currBin >= 0 && (binIndex - currBin) < MAX_SEPARATION;
        currBin--) {
      for (Edge e in binList[currBin]) {
        if (e.minPos == edge.minPos || e.maxPos == edge.maxPos) {
          distPrev = binIndex - currBin;
          break;
        }
      }
      if (distPrev != MAX_SEPARATION) break;
    }
    return distPrev;
  }

  int binDistancePos(Edge edge, List<List<Edge>> binList, int binIndex) {
    int MAX_SEPARATION = 10;
    int distPos = MAX_SEPARATION;

    for (int currBin = binIndex + 1;
        currBin < binList.length && (currBin - binIndex) < MAX_SEPARATION;
        currBin++) {
      for (Edge e in binList[currBin]) {
        if (e.minPos == edge.minPos || e.maxPos == edge.maxPos) {
          distPos = currBin - binIndex;
          break;
        }
      }
      if (distPos != MAX_SEPARATION) break;
    }
    return distPos;
  }

  bool fitsInBin(Edge edge, List<Edge> bin) {
    if (bin.length == 0) return true;
    int count = 0;
    bool flag = true;
    bin.takeWhile((e) => flag).forEach((e) {
      if (edge.maxPos < e.minPos || edge.minPos > e.maxPos)
        count++;
      else
        flag = false;
    });
    if (flag && count == bin.length) return true;
    return false;
  }

  /** returns the max distance between edges*/
  int maxDistance() {
    int maxDist = 0;
    _edges.forEach((ts, edgesTS) {
      edgesTS.forEach((e) {
        if (e.isValid) {
          int curDist = e.lenght;
          if (curDist > maxDist) maxDist = curDist;
        }
      });
    });
    return maxDist;
  }

  void drawEdgeColor(Edge e, int xDisplacement, String color,
      [num lineWidth = 1]) {
    num radius = nodeRadius;

    num halfNodeHeight = e.edge[0].height / 2;
    var previousGlobalAlpha = _context.globalAlpha;

    _context
      ..globalAlpha = 1
      ..strokeStyle = color
      ..lineWidth = lineWidth;

    int i = 0;
    bool found = false;
    LineStyle ls;
    while ((i < lineStylingList.length) && (!found)) {
      if (lineStylingList[i].status) {
        found = true;
        ls = LineStyle.values[i];
      }
      i++;
    }

    if (!found)
      ls = LineStyle.solid; //For security: in case something went wrong
    String prevStrokeStyle = canvasContext.strokeStyle;
    customLineTo(_context, new Point(xDisplacement, e.first.y + halfNodeHeight),
        new Point(xDisplacement, e.last.y + halfNodeHeight), ls, lineWidth);
    canvasContext.strokeStyle = prevStrokeStyle;

    for (Node n in e._edge) {
      if (DRAWCIRCLE)
        drawCircle(_context, new Point(xDisplacement, n.y + halfNodeHeight),
            radius, Theme.edgesSplatDefault);
    }
    _context.globalAlpha = previousGlobalAlpha;
  }

  void drawEdgeTwoColors(Edge e, num xDisplacement, String color1,
      String color2, LineStyle ls, num lineWidth) {
//      int i = 0;
    for (num i = 0; i < e.length - 1; i++) {
      if (DEBUG) print("about to call customLineWithNegativeTo...");
      customLineWithNegativeTo(
          _context,
          new Point(xDisplacement, e.edge[i].y + e.edge[i].height / 2.0),
          new Point(
              xDisplacement, e.edge[i + 1].y + e.edge[i + 1].height / 2.0),
          ls,
          color1,
          color2,
          lineWidth);
      if (DEBUG) print("customLineWithNegativeTo called successfully");
    }
    if (DEBUG) print("drawEdgeTwoColors ended correctly");
  }

  void drawEdge(Edge e, num xDisplacement, [double min, double max]) {}
}
