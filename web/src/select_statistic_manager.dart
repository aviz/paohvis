part of paohvis;

abstract class SelectStatisticManager {
  num _top = 0;
  num _left = 0;
  num _maxWidth = 50;

  DivElement _element;
  SelectElement _selectElement;

  SelectStatisticManager(this._element, this._selectElement){
    if(DEMO_VIS4DH){
      _element.style.display = 'none';
    }
  }

  updateDims(num top, num left, num width){
    _top = top;
    _left = left + 5;
    _maxWidth = width;

    _element.style.width = _maxWidth.toString() + 'px';
    _maxWidth -= 10;

    _element.style.top = _top.toString() + 'px';
    _element.style.left = _left.toString() + 'px';
    _selectElement.style.width = _maxWidth.toString() + 'px';
  }
}

class SelectStatisticManagerTop extends SelectStatisticManager{
  SelectStatisticManagerTop(DivElement _element, SelectElement _selectElement)
    :super(_element, _selectElement){

    if(_selectElement.length == 0) {
      _selectElement.append(new OptionElement(
          data: '# of {node}s', value: 'number of nodes', selected: false));
      _selectElement.append(new OptionElement(
          data: '# of {hyperedge}s', value: 'number of edges', selected: false));
      _selectElement.append(new OptionElement(
          data: 'average # of {node}s per {hyperedge}', value: 'average nodes', selected: true));
      _selectElement.options[0].className = 'node-change';
      _selectElement.options[1].className = 'hyperedge-change';
      _selectElement.options[2].className = 'node-change hyperedge-change';
      _selectElement.onChange.listen(( e){
        eventBus.fire(new TimeSlotsStatisticsChanged(_selectElement.value));
        _selectElement.blur();
      });
    }

    eventBus.fire(new TimeSlotsStatisticsChanged(_selectElement.value));

//    _selectElement.options.add("number of edges");
  }

}

class SelectStatisticManagerLeft extends SelectStatisticManager{

  SelectStatisticManagerLeft(DivElement _element, SelectElement _selectElement)
      :super(_element, _selectElement){

    if(_selectElement.length == 0) {
      _selectElement.append(new OptionElement(
          data: '# of {hyperedge}s', value: 'number of edges', selected: true));
      _selectElement.append(new OptionElement(
          data: '# of {time slot}s with {hyperedge}s', value: 'number of appereances', selected: false));

      _selectElement.options[0].className = 'hyperedge-change';
      _selectElement.options[1].className = 'hyperedge-change ts-change';

      _selectElement.onChange.listen(( e){
        eventBus.fire(new NodeStatisticsChanged(_selectElement.value));
        _selectElement.blur();

      });
    }

    eventBus.fire(new NodeStatisticsChanged(_selectElement.value));
//    _selectElement.options.add("number of edges");
  }
}