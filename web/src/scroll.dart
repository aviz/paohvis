part of paohvis;

abstract class Scroll{

  DivElement _element;
  CanvasElement _canvas;
  CanvasRenderingContext2D _context;

  num _width;
  num _height;

  num _x0 = 0;

  num _pos0 = 0;


  num _space0 = 0;
  num _space1 = 0;

  num _visLims0 = 0;
  num _visLims1 = 0;

  List<num> _highlights = new List<num>();
  List<num> _selects  = new List<num>();


  bool _mouseDown = false;

  var onMouseDownEvent, onMouseUpEvent, onMouseMoveEvent,onMouseLeaveEvent,
      onInputSlider, onClickSlider,
      onScrollSpaceChanged, onScrollLimitsChanged,
      onUpdateHighlights, onUpdateSelects;

  Scroll(DivElement element, CanvasElement canv){

    _element = element;

    _canvas = canv;
    _context = _canvas.getContext('2d');

    setupDims();

    onInputSlider = _element.onMouseWheel.listen(onMouseWheel);
    onClickSlider = _element.onClick.listen(onClick);
    onMouseDownEvent = _element.onMouseDown.listen(onMouseDown);
    onMouseUpEvent = _element.onMouseUp.listen(onMouseUp);
    onMouseMoveEvent = _element.onMouseMove.listen(onMouseMove);
    onMouseLeaveEvent = _element.onMouseLeave.listen(onMouseLeave);

  }


  setSpace(num ns0, num ns1){
    _space0 = ns0;
    _space1 = ns1;

    update();
  }

  setVisLimits(num vs0, num vs1, num x0){
    _visLims0 = vs0;
    _visLims1 = vs1;

    _x0 = 0; //x0

    setupDims();

    update();
  }

  updateHighlights(List<num> highlights){
    _highlights = highlights;
    update();
  }

  updateSelects(List<num> selects){
    _selects = selects;
    update();
  }

  setupDims(){
    _width = _element.clientWidth;
    _height = _element.clientHeight;

    num dpr = window.devicePixelRatio;

    _canvas.width = (_width * dpr).toInt();
    _canvas.height = (_height * dpr).toInt();

    _canvas.style.width = _width.toString() + "px";
    _canvas.style.height = _height.toString() + "px";

    _context.scale(dpr, dpr);
  }

  onMouseDown(MouseEvent e){

  }

  onMouseMove(MouseEvent e){
  }

  onMouseUp(MouseEvent e){
    _pos0 = 0;
    _element.releasePointerCapture(1);
    _mouseDown = false;
    //      eventBus.fire(new TimeSliderChanged(_element.value));
    e.preventDefault();
//    eventBus.fire(new TimeSlotsScrolled(e.client.x - _x1Bar));
  }

  onMouseLeave(MouseEvent e){
//    _mouseDown = false;
    //      eventBus.fire(new TimeSliderChanged(_element.value));
//    e.preventDefault();
//    eventBus.fire(new TimeSlotsScrolled(e.client.x - _x1Bar));
  }

  onMouseWheel(WheelEvent e){

  }

  onClick(MouseEvent e){

  }

  renderBack(){
    _context
      ..fillStyle = "#ffffff"
      ..strokeStyle = "#ffffff";

    _context
      ..fillRect(0, 0, _width, _height)
      ..strokeRect(0, 0, _width, _height);

    _context
      ..fillStyle = "#dddddd"
      ..strokeStyle = "#dddddd";

    _context
      ..fillRect(_x0, 0, _width, _height)
      ..strokeRect(_x0, 0, _width, _height);
  }

  renderBar(){
  }

  renderHints(List<num> posList, String color){
  }

  renderHighlights(){
    renderHints(_highlights, "#00e673");
  }

  renderSelects(){
    renderHints(_selects, "#FFA500");
  }

  render(){

    setupDims(); //TODO!!!!! add event!

    renderBack();

    if(_space0 == _space1) return;
    if(_visLims0 == _visLims1) return;

    renderBar();
    renderSelects();
    renderHighlights();

  }

  update(){
    render();
  }
}



class TimeSlotScroll extends Scroll{

  TimeSlotScroll(DivElement element, CanvasElement canv)
  :super(element, canv){
    onScrollSpaceChanged = eventBus.on<TimeScrollTsSpaceChanged>().listen((TimeScrollTsSpaceChanged event) {
      setSpace(event.x0, event.x1);
    });

    onScrollLimitsChanged = eventBus.on<TimeScrollVisLimitsChanged>().listen((TimeScrollVisLimitsChanged event) {
      setVisLimits(event.v0, event.v1, event.x0);
    });

    onUpdateHighlights =  eventBus.on<UpdateEdgeHighlights>().listen((UpdateEdgeHighlights event) {
      updateHighlights(event.list);
    });
    onUpdateSelects =  eventBus.on<UpdateEdgeSelects>().listen((UpdateEdgeSelects event) {
      updateSelects(event.list);
    });

    render();
  }

  onMouseWheel(WheelEvent e){
    //      eventBus.fire(new TimeSliderChanged(_element.value));
    e.preventDefault();
    eventBus.fire(new TimeSlotsScrolled(e.deltaY));
  }

  onMouseDown( e){
    e.preventDefault();

    _element.setPointerCapture(1);


    _pos0 = 0;

    _mouseDown = false;

    num xFirst = _space0;
    num xLast = _space1;

    num posX = scaleCoord(e.client.x, _x0, _width, xFirst, xLast);

    num x1 = _visLims0; //tm.getPosX1(firstTs);
    num x2 = _visLims1; // tm.getPosX2(lastTs);

    if(posX<x1 || posX>x2) {
      return false;
    }

    _pos0 = posX - x1;

    _mouseDown = true;

  }

  onClick(MouseEvent e){

//    print("click");
//    print(e.client.x);
//    print(_width);
//    print(scaleCoord(e.client.x, _x0, _width, _space0, _space1));

//    if(!_mouseDown) {
//      print('click nmd');
////      eventBus.fire(new TimeSliderChanged(_element.value));
//
////    num xFirst = tm.getPosX1(tm.timeList.first);
////    num xLast = tm.getPosX2(tm.timeList.last);
//
//      num xFirst = _space0;
//      num xLast = _space1;
//
//      num posX = scaleCoord(e.client.x, _x0, _width, xFirst, xLast);
//
//      eventBus.fire(new TimeSlotsMoved(xFirst-posX));
//    }
    _mouseDown = false;


  }

  onMouseMove(MouseEvent e){

    if(_mouseDown){

      e.preventDefault();

      num xFirst = _space0;
      num xLast = _space1;

      num posX = scaleCoord(e.client.x, _x0, _width, xFirst, xLast);

      eventBus.fire(new TimeSlotsMoved(xFirst - posX+ _pos0));
    }
  }

  renderBar(){
    num x1 = _visLims0; //tm.getPosX1(firstTs);
    num x2 = _visLims1; // tm.getPosX2(lastTs);

    num x1Bar = scaleCoord(x1, _space0, _space1, _x0, _width);
    num barWidth = scaleCoord(x2,  _space0, _space1,  _x0, _width)-x1Bar;

      _context
        ..fillStyle = "#555555"
        ..strokeStyle = "#555555";

      _context
        ..fillRect(x1Bar, 4, barWidth, _height-8)
        ..strokeRect(x1Bar, 4, barWidth, _height-8);


  }

  renderHints(List<num> posList, String color){

    num w = 2;

    posList.forEach((xh){
      num xh_pos = scaleCoord(xh, _space0, _space1, _x0, _width);

      _context
        ..fillStyle = color
        ..strokeStyle = color;

      _context
        ..fillRect(xh_pos, 0, w, _height)
        ..strokeRect(xh_pos, 0, w, _height);
    });
  }


}

class VertScroll extends Scroll{

  VertScroll(DivElement element, CanvasElement canv)
  :super(element, canv){

    onScrollSpaceChanged = eventBus.on<VertScrollNodeSpaceChanged>().listen((VertScrollNodeSpaceChanged event) {
      setSpace(event.n0, event.n1);
    });

    onScrollLimitsChanged = eventBus.on<VertScrollVisLimitsChanged>().listen((VertScrollVisLimitsChanged event) {
      setVisLimits(event.v0, event.v1, 0);
    });

    onUpdateHighlights =  eventBus.on<UpdateNodeHighlights>().listen((UpdateNodeHighlights event) {
      updateHighlights(event.list);
    });
    onUpdateSelects =  eventBus.on<UpdateNodeSelects>().listen((UpdateNodeSelects event) {
      updateSelects(event.list);
    });

    render();
  }

  onMouseWheel(WheelEvent e){
    //      eventBus.fire(new TimeSliderChanged(_element.value));
    e.preventDefault();
    eventBus.fire(new NodesScrolled(e.deltaY));
  }

  onClick(MouseEvent e){

//    if(!_mouseDown) {
////      eventBus.fire(new TimeSliderChanged(_element.value));
//
//      num x1 = _visLims0; //tm.getPosX1(firstTs);
//
//      num xFirst = _space0;
//      num xLast = _space1;
//
//      num y1Bar = scaleCoord(x1, xFirst, xLast, 0, _height);
////
//      num posY = scaleCoord(e.client.y-_x0, 0, _height, _space0, _space1);
////
//      eventBus.fire(new NodesScrolled(posY-y1Bar));
//    }
    _mouseDown = false;
  }

  onMouseMove(MouseEvent e){
    if(_mouseDown){
      e.preventDefault();
//
//      num xFirst = _space0;
//      num xLast = _space1;
//
//      num posX = scaleCoord(e.client.x, _x0, _width, xFirst, xLast);
//
//      eventBus.fire(new VertScrolled(xFirst - posX+ _pos0) );

      num posY = scaleCoord(e.client.y-_x0, 0, _height, _space0, _space1);
//
      eventBus.fire(new NodesScrolled(posY-_pos0));
    }
  }


  onMouseDown( e){
    e.preventDefault();


    _element.setPointerCapture(1);

    _pos0 = 0;

    _mouseDown = false;

    num posY = scaleCoord(e.client.y-_x0, 0, _height, _space0, _space1);

    num x1 = _visLims0; //tm.getPosX1(firstTs);
    num x2 = _visLims1; // tm.getPosX2(lastTs);

    if(posY<x1 || posY>x2) {
      return false;
    }


    _pos0 = posY - x1;

    _mouseDown = true;


  }

  renderBar(){

    num x1 = _visLims0; //tm.getPosX1(firstTs);
    num x2 = _visLims1; // tm.getPosX2(lastTs);

    num xFirst = _space0;
    num xLast = _space1;

    num y1Bar = scaleCoord(x1, xFirst, xLast, 0, _height);
    num barHeight = scaleCoord(x2,  xFirst, xLast,  0, _height) - y1Bar;

    _context
      ..fillStyle = "#555555"
      ..strokeStyle = "#555555"
      ..globalAlpha = 1;

    _context
      ..fillRect(4, y1Bar, _width-8, barHeight)
      ..strokeRect(4, y1Bar, _width-8, barHeight);
  }

  renderHints(List<num> posList, String color) {
    num h = 2;

    num xFirst = _space0;
    num xLast = _space1;

    posList.forEach((yh){
      num yh_pos = scaleCoord(yh, xFirst, xLast, 4, _height-4);

      _context
        ..fillStyle = color
        ..strokeStyle = color;

      _context
        ..fillRect(0, yh_pos, _width, h)
        ..strokeRect(0, yh_pos, _width, h);
    });
  }

}