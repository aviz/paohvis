part of paohvis;

class ColorManager{

  String getEdgeColor(Edge e, String technique, [bool primary=true]){

    String edgeColor = Theme.edgesPaovis;

    if(e.isHighlighted || e.isSideHighlighted){
      edgeColor = getDefaultEdgeColor(technique, primary);
    }else if((NODE_HIGHLIGHTED || EDGE_HIGHLIGHTED || TS_HIGHLIGHTED)){// && ENABLED_HIGHLIGHT &&  (NODE_HIGHLIGHTED || EDGE_HIGHLIGHTED || TS_HIGHLIGHTED)){
      edgeColor = Theme.edgesNotHighlight;
    }else if(e.isSelected || e.isSideSelected){
        edgeColor = getDefaultEdgeColor(technique, primary, e.isSelected, e.inIntersection);
    }else if((NODE_SELECTED || EDGE_SELECTED || TS_SELECTED)){// && ENABLED_HIGHLIGHT &&  (NODE_HIGHLIGHTED || EDGE_HIGHLIGHTED || TS_HIGHLIGHTED)){
      edgeColor = Theme.edgesNotHighlight;
    }else{
      edgeColor = getDefaultEdgeColor(technique, primary);
    }

    return edgeColor;
  }

  String getTsColor(TimeManager tm, String ts){
    String tsColor = Theme.tsFontDefault;
    if(tm.isHighlighted(ts)){
      tsColor = Theme.tsFontDefault;
    }else if((NODE_HIGHLIGHTED || EDGE_HIGHLIGHTED || TS_HIGHLIGHTED)){// && ENABLED_HIGHLIGHT &&  (NODE_HIGHLIGHTED || EDGE_HIGHLIGHTED || TS_HIGHLIGHTED)){
      tsColor = Theme.tsFontNotHighlight;
    }else if(tm.isSelected(ts) || tm.isSideSelected(ts)){
      tsColor = Theme.tsFontDefault;
    }else if((NODE_SELECTED || EDGE_SELECTED || TS_SELECTED)){
      tsColor = Theme.tsFontNotHighlight;
    }
    return tsColor;
  }

  String getNodeColor(Node n, [Edge e=null, String technique='PAOVIS', num community = null, bool lighter=true]){

    String nodeColor = getDefaultNodeColor(n, technique, community, lighter);

    if (e != null){
      if(e.isHighlighted || e.isSideHighlighted){
        nodeColor = getDefaultNodeColor(n, technique, community);
      }else if(NODE_HIGHLIGHTED || EDGE_HIGHLIGHTED || TS_HIGHLIGHTED){
        nodeColor = Theme.verticesNotHighlight;
      }else if(e.isSelected || e.isSideSelected){
        nodeColor = getDefaultNodeColor(n, technique, community);
      }else if((NODE_SELECTED || EDGE_SELECTED || TS_SELECTED)){
        nodeColor = Theme.verticesNotHighlight;
      }else if(!n.isValid){
        nodeColor = '#888888';
        if( ENABLED_COLORGROUP && community != null){ // global!
          nodeColor = getCommunityColor(community, true);
        }
      }
    }else if (n.isHighlighted || n.isAdjacentToHighlighted) {
      nodeColor = getDefaultNodeColor(n, technique, community);
    } else if (NODE_HIGHLIGHTED || EDGE_HIGHLIGHTED || TS_HIGHLIGHTED) {
      nodeColor = Theme.verticesNotHighlight;
    } else if (n.isSelected || n.isAdjacentToSelected) {
      nodeColor = getDefaultNodeColor(n, technique, community);
    } else if ((NODE_SELECTED || EDGE_SELECTED || TS_SELECTED)) {
      nodeColor = Theme.verticesNotHighlight;
    }

    return nodeColor;
  }


  String getDefaultEdgeColor(String technique,[bool primary=true, bool selected=false, bool intersection=false]){

    String edgeColor = Theme.edgesPaovis;

    if(intersection)
      return Theme.edgesPaovisIntersect;

    if(!ALTERNATE_COLORS) {
      if(selected)
        return Theme.edgesPaovisSelect;
      return Theme.edgesPaovis;
    }


    switch (technique) {
      case 'PAOVIS':
        edgeColor = getPaovisEdgeColor(primary);
        break;
      case 'CURVES':
        edgeColor = Theme.edgesCurvesDefault;
        break;
      case 'SPLAT':
        edgeColor = Theme.edgesSplatDefault;
        break;
      case 'NODELINK':
        edgeColor = Theme.edgesNodelinkDefault;
        break;
    }
    return edgeColor;
  }

  String getDefaultNodeColor(Node n, String technique, [num community=null, bool lighter=true]){

    if( ENABLED_COLORGROUP && community != null){
        return getCommunityColor(community, false);
    }

    String nodeColor = Theme.verticesDefault;
    switch (technique) {
      case 'PAOVIS':
        nodeColor = Theme.verticesPaovisDefault;
        break;
      case 'SPLAT':
        nodeColor = Theme.verticesSplatDefault;
        break;
      case 'NODELINK':
        nodeColor = Theme.verticesNodelinkDefault;
        break;
    }
    return nodeColor;
  }

  String  getPaovisEdgeColor(bool primary){

    String edgeColor = "#1b9e77";
    if(!primary){
      edgeColor = "#7570b3";
    }

    return edgeColor;
//    String edgeColor;
//    HslColor lineHexBase = new HexColor(Theme.edgesPaovisDefault).toHslColor();
//    HslColor hslcol = lineHexBase;
//    if((lineHexBase.l*1.5)>80) {
//      hslcol = new HslColor(lineHexBase.h, lineHexBase.s, 80);
//    } else {
//      hslcol = new HslColor(lineHexBase.h, lineHexBase.s, lineHexBase.l *1.5);
//    }
//
//    if(primary) {
//      edgeColor = "#" + hslcol.toHexColor().toString();
//    }
//    else {
//      edgeColor =  "#" + (new HslColor(hslcol.h + 50, hslcol.s, hslcol.l))
//          .toHexColor()
//          .toString();
//    }
//
//    return edgeColor;
  }

  String getCommunityColor(int comId, [bool lighter = true]){
    String nodeColor = getColorCode(
        1.0,
        1.0,
        comId.toDouble(),
        ColorCodings.Communities,
        lighter,
        120,
        new HexColor("#ffffff"), 0 );
    return nodeColor;
  }

}